---
title: Internationale Tagung Universität Frankfurt 2008
category: rezeption
language: de
page-nr: 410
---
![Casino Gebäude Frankfurt]({{ 'assets/images/Campus-Westend-Frankfurt.jpg' | relative_url }}){:.img-right.img-large}
***„Kreuz der Wirklichkeit” und „Stern der Erlösung”***\
*Die Glaubens-Metaphysik*\
*von Eugen Rosenstock-Huessy und Franz Rosenzweig*



[Programm der Tagung](https://www.uni-frankfurt.de/40697877/RosenzweigRosenstock_Tagung_Frankfurt_08_Programm___screen_.pdf)
