---
title: "Peter Sloterdijk: Der Kontinent ohne Eigenschaften: Lesezeichen im Buch Europa"
category: rezeption
language: de
page-nr: 970
published: 2024-12-14
---
**Peter Sloterdijk**\
[**Der Kontinent ohne Eigenschaften: Lesezeichen im Buch Europa**](https://www.suhrkamp.de/buch/peter-sloterdijk-der-kontinent-ohne-eigenschaften-t-9783518432143)
[![Sloterdijk: Kontinent ohne Eigenschaften]({{ 'assets/images/PS_Kontinent_ohne_Eigenschaften.jpg' | relative_url }}){:.img-right.img-small}](https://media.suhrkamp.de/mediadelivery/asset/dcf4695a4ede4ae49c7b01f82d9c7499/der-kontinent-ohne-eigenschaften_9783518432143_leseprobe.pdf?contentdisposition=inline)

Über Europa sind viele Bonmots und Untergangsdiagnosen im Umlauf. Man wisse nicht, unter welcher Nummer man Europa erreichen könne, seine Bewohner seien dekadent, der Halbkontinent, der einst den »Rest der Welt« kolonisierte, sei nun seinerseits in den Rest geraten etc.

Doch wie im Fall Mark Twains erweisen sich Nachrichten vom Ableben der »Alten Welt« regelmäßig als stark übertrieben. Gleichwohl sind sich die Europäer ihrer Eigenschaften nicht mehr sicher: »Sie wissen nicht, woher sie kommen, erst recht nicht, wohin die Reise geht.« Um Orientierung zu stiften, blättert Peter Sloterdijk im Buch Europa einige Lesezeichen auf, etwa das des Kulturphilosophen Eugen Rosenstock-Huessy, der die »Autobiografie des westlichen Menschen« als Sequenz politischer Revolutionen erzählte. Sloterdijk öffnet auch das »Buch der Geständnisse«, aus dem sich ein bezeichnender Geist der Selbstkritik erklärt. Und er zitiert aus dem »Buch der Ausdehnungen«, das Europas Missionen im Zeitalter der nautischen Globalisierung illustriert.

Was ist Europa also? Jedes Gemeinwesen, das sich in der Tradition Roms sieht? Ein sich selbst verstärkender Lernzusammenhang? Das wahre Europa, so Sloterdijk, findet sich überall dort, wo die schöpferischen Leidenschaften denen des Ressentiments den Rang abgelaufen haben.

**Inhalt**
- Erste Eröffnungsrede: Ausreden, Nekrologe, Aprèsludes
- Zweite Eröffnungsrede: Lateineuropa Der Kontinent, das *imperium* und seine Übertragungen
- Lektion eins: Die Grande École der Welt Europa als Lernzusammenhang: Aus dem Buch der Steigerungen
- Lektion zwei: Out of Revolution:  Wie ein deutscher Historiker den Europäern ihre Autobiographie schreibt
- Lektion drei: Geschichte a priori: Das Buch der Endspiele - Spenglers Prophezeiung und wie sie sich erfüllte
- Lektion vier: *Dire vrai sur soi-même* Das europäische Buch der Geständnisse
- Lektion fünf: Geht, setzt die Welt in Brand! Aus dem Buch der Ausdehnungen
- Lektion sechs: Lose Fische Von Schiffen, Globen und überzähligen
Söhnen
- Lektion sieben: Get a-way, you old peoples! Aus dem Buch der Gegenstimmen: Europa im Akkusativ
- Dank

### Lektion zwei: Out of Revolution - Wie ein deutscher Historiker den Europäern ihre Autobiographie schreibt

Das Buch Europa - wie können wir uns seiner Lesbarkeit vergewissern? Auf welche Weise überzeugen wir uns davon, daß es dem »Kontinent ohne Eigenschaften«, ungeachtet seines ausweichenden Charakters, an Erkennbarkeit, vielleicht sogar an Erzählbarkeit nicht ganz fehlt? Wo müßte man ansetzen, um den amorphen Quasi-Gegenstand Europa unter dem Licht der Darstellbarkeit aufzufassen - sei es dank einer Erhellungsmethode, die Licht von außen herbeibringt, sei es durch eine Fluoreszenz, die dem Objekt selbst innewohnt?

Ich möchte auf den folgenden Seiten die Rolle des seltsamen Fremden aus der Erzählung »Das Sandbuch« von Jorge Luis Borges übernehmen, der an der Tür des Bibliophilen von Buenos Aires geläutet und sich als Verkäufer seltener Bibeln präsentiert hat.

Allerdings: Dessen Sandbuch war die Verneinung der Möglichkeit, eine zusammenhängende Geschichte zu bilden. Es übermittelte eo ipso die Botschaft, daß du dich als Leser des diabolischen Werks an keiner Stelle darin lokalisieren kannst; du würdest eine Stelle, die dich angeht, vorausgesetzt, es gäbe sie, nie wiederfinden - daher könntest du auch nicht überprüfen, ob sie dir wirklich etwas zu sagen hatte. Das Lesen im Unendlichen ist die Unmöglichkeit, von einer Geschichte betroffen zu sein. Der Verkäufer von Buenos Aires hatte das Dilemma präzise bezeichnet:

      Wenn der Raum unendlich ist, sind wir an einer beliebigen Stelle im Raum.
      Wenn die Zeit unendlich ist, dann sind wir an einer beliebigen Stelle in der Zeit.

Das Buch, das ich in meinem Koffer mit mir führe und dem Publikum jetzt zum Erwerb anbiete, wenn auch zunächst nur zu einer ersten Kenntnisnahme, ist das perfekte Anti-Sandbuch. Es stellt den heftigsten Protest dar, der in jüngerer Zeit gegen das Verschwimmen des menschlichen Daseins in der Unendlichkeit - und deren kleiner Schwester, der Gleichgültigkeit - formuliert wurde. Ja, es entfaltet das Äußerste an Eindringlichkeit, um es nicht Zudringlichkeit zu nennen, mit dem sich ein Buch einem Leser mitteilen kann. Dieses Buch nimmt sich das Recht, die Rezeptivität, genauer: die Lern- und Leidensbereitschaft des Aufnehmenden bis ins Extrem zu beanspruchen - weit hinaus über das, was Wagner- und Stockhausen-Opern von ihren Hörern verlangen.

Die Rede ist von dem voluminösen Werk Out of Revolution aus der Feder des Historikers und Sprachtheoretikers Eugen Rosenstock-Huessy, im Jahr 1938 bei Wipp & Stock zu Eugene, Oregon, USA, veröffentlicht und versehen mit dem etwas bizarren, doch lautstarken Untertitel: Autobiography of Western Man. Schlägt man es auf, begrüßt den Leser das horazische Motto: De te fabula narratur - »von dir handelt die Erzählung«. Merken wir beiläufig an, daß auch Karl Marx sich dieser Formel bediente, um deutsche Leser seines Werks Das Kapital 1, vor dem Trugschluß zu bewahren, all das, was dort abgehandelt wird, beträfe nur Engländer - doch nein, es geht auch Leser auf dem Kontinent an, und die deutschen besonders.

»Autobiographie des westlichen Menschen«- die Formulierung ist geeignet, in mehrfacher Hinsicht Befremden hervorzurufen. sZum einen ist die Wendung »westlicher Mensch« für heutige Gehöre problematisch geworden; kaum ein Europäer unserer Tage würde sich - obgleich »identitäre«, kommunitarische und regionalistische Bewegungen eine hohe Konjunktur erleben - ohne Vorbehalte einem Kollektiv »westlicher Menschen« zuordnen. Nicht nur weil zur Zeit die großen Mehrheiten von antikollektivistischen Motiven beherrscht werden; man zögert auch, dem Ausdruck Kredit zu geben, seit zahlreiche Angehörige der euro-amerikanischen Zivilisation begonnen haben, sich selbst so lange in den Spiegeln des globalen Ostens, des globalen Südens und der globalen Indigenität zu be-trachten, bis ihnen jede Lust an der Selbstaffirmation verging.

...

All diesen umwälzenden Vorgängen ist in Hinsicht auf die Einzelnen ein Zug gemeinsam, den man als eine radikale Promotion des Menschenwesens bezeichnen darf. Sie affiziert das In-der-Welt-Sein von Angehörigen der Gattung homo sapiens von Grund auf.

Ihre Wirkung ist eine Tiefenaufhebung der Sklaverei, die sämtlichen Abolitionismen zuvorkommt. Sie wurde bis heute weder in Asien noch in der arabo-muslimischen Welt und in den Nationen des Globalen Südens nachvollzogen, weswegen man dort gern - um altehrwürdige Repressionen auf der Linie patriarchalischer Strukturen zu legitimieren - gern betont, es gebe doch eine Mehrzahl von eigenständigen »Zivilisationen«. Daher sei das Pathos allgemeiner Menschenwürde und weiblicher Gleichberechtigung nicht mehr als ein eurozentrischer Spleen, dem Kredit zu geben man sich hüten werde.

Die revolutionären Promotionen der conditio humana, wie der Autor sie resümiert, sind bis in die heutigen Tage kaum zu Ende gedacht. Sie hoben alle Arten von altem Adel auf, indem sie den Menschen per se als das Wesen bestimmten, das nicht nichtadlig sein kann. Die Reformation machte durch die Schaffung dessen, was Rosenstock-Huessy *laity sanctified*, geheiligte Laizität, nennt, aus jedem Menschen ein Wesen mit priesterlicher Kompetenz - jeder Einzelne ist nun, seit der Glaube nicht mehr unter ein Kirchendach gebannt werden kann, unmittelbar zum Unendlichen. Die englische Revolution machte aus jedem Gentleman einen Pair des Königs, die französische aus jedem Menschen von Talent und gutem Willen ein Mitglied des neuen meritokratischen Adels mit Zügen adamitischer Popularität. Die verallgemeinerten Anreden: Herr, Sir, monsieur, madam und madame sollten verdeutlichen, daß der Sinn revolutionärer Bewegungen letztlich nicht in verbesserten Sozialleistungen besteht, sondern in allgemeiner Nobilitierung.

Die modernen Massenkulturen, vor diesem Hintergrund wahrgenommen, folgen hingegen fast durchwegs regressiven Motiven, da sie die Menschen in ihrer vorreformatorischen, unrevolutionierten Gewöhnlichkeit ansprechen; sie appellieren an eine erbliche Vulgarität, die keine Einschwörung auf den Dienst am Gemeinwohl kennen will und von der kulturtragenden Sorge um die Lebenschancen der Enkelkinder nichts weiß. Wo auch immer man Autokratien, Diktaturen und Populismen am Werk sieht, wird die Annahme gemacht und bestätigt, in politischen Dingen sei nur auf die niedrigen Regungen:
Angst, Gier und Rachsucht, Verlaß. Rußland hat sich durch sein Bekenntnis zur Niedrigkeit von Europa losgesagt, es ist ins Stadium der vorreformatorischen Unterwerfung zurückgekehrt. Das Erbe der Russischen Revolution für die übrige Welt besteht in der Demonstration, daß eine Politik der halben Wahrheit, auch wenn sie pravda heißt, in Lüge, Zynismus, Staatsterror und Massenalkoholismus endet.

Als Anthropologe der europäischen Revolutionen bekennt sich Rosenstock-Huessy zu der Ansicht, daß der Mensch ein Wesen sei, das zu seiner Entfaltung der Entwurzelung aus dem bloßen Herkommen bedürfe. Der Mensch, so glaubt er, muß gleichsam umgetopft und aus der Gegenwart in die künftigen Zustände umgepflanzt werden, weil er im festgetretenen heimischen Boden leicht verkommt. Er erhebt damit einen elementaren Einwand gegen jede Form von Nativismus. Er hat für den Kult um indigene Kulturen nicht mehr als schweigende Skepsis übrig - obschon er bereit ist, anzuhören, was fremde Stimmen zu sagen haben, sobald sie für sich das Wort nehmen. Nicht umsonst hatte Rosenstock-Huessy seine (von der Akademia bis heute ignorierte) Sprachphilosophie in der Sentenz zusammengefaßt: respondeo etsi mutabor: »Ich antworte, sollte ich auch verwandelt werden.«) Wer immer nur bleibt, wo er zufällig zur Welt kam, und wer sich in allem bloß als Kind seiner Zeit verhält, ohne auch einen Ruf von vorne, von einem wie auch immer vagen Omega der Geschichte her, zu hören, verfällt der Beschränktheit dessen, was man vormals lokale Dämonen genannt hätte. Der Entwurzelung aus verbrauchtem Herkommen müsse aber immer eine Angelobung an eine legitime neue Quelle der Begeisterung entsprechen, damit die Einzelnen im nachrevolutionären Raum nicht der Desorientierung verfallen; in der Flaute nach den Kampfzeiten erliegen sie allzu leicht einem dumpfen Nicht-weiter-Wissen. Das könnte - entre nous - bereits über heutige Europäer gesagt sein, die oft ratlos nach Brüssel schauen und nicht wissen, ob es wirklich die besseren Engel unserer Natur sind, die dort für das Weitere sorgen.

Mit Hinweisen wie diesen hat unser Autor das Recht erworben, sich nicht allein an die amerikanischen Leser von 1938, sondern auch an die Bürger des heutigen Europa zu wenden. Für die unter ihnen, die Genaueres wissen wollen, haben wir hier ein etwas breiteres Lesezeichen eingefügt, sei es auch an einer für das Publikum so gut wie unbekannten Stelle. Nicht wenige Europäer unserer Tage bedürfen, wie es scheint, einer Art von Innerer Mission, wenngleich weniger im Sinn der Rechristianisierung als dem einer Besinnung auf die geschichtlichen Quellen ihrer Zivilität. Wie ihre Vorfahren in den 20er Jahren des 20. Jahrhunderts wissen sie kaum, woher sie kommen, erst recht nicht, wohin ihre weitere Reise geht.

#### Referenzen
- Homer: *Odyssee*, IX, Wende vom 8. zum 7. Jahrhundert v. Chr.
- Harro von Senger: *Stratageme I und II. Lebens- und Überlebenslisten aus drei Jahrtausenden: Die berühmten 36 Strategeme der Chinesen – lange als Geheimwissen gehütet, erstmals im Westen vorgestellt*, 2 Bände, Frankfurt a. ‌M. 2003.
- Giorgio Agamben: *Jenseits der Menschenrechte*, Jungle World 27/2001, S. 27, unter: https://jungle.world/artikel/2001/27/jenseits-der-menschenrechte.
- Susan Sontag: *What's Happening to America (A Symposium)*, in: Partisan Review 34/1 (1967), S. 57f.
- Michel Winock: *Décadence. Fin de siècle* Paris 2017
- Mario Praz: *Liebe, Tod und Teufel. Die schwarze Romantik*, München 1981 [Orig. 1930]
- Friedrich Nietzsche: *Die fröhliche Wissenschaft*
- Friedrich Nietzsche: *Die Götzen-Dämmerung, Sprüche und Pfeile*
- Friedrich Nietzsche: *Jenseits von Gut und Böse. Zur Genealogie der Moral*
- Friedrich Nietzsche: *Morgenröte*
- Tzvetan Todorov: *Die verhinderte Weltmacht. Reflexionen eines Europäers*, München 2003
- Fjodor Dostojewskij: *Aufzeichnungen aus dem Kellerloch*, Frankfurt a. ‌M. 2024,
- James Joyce: *Ulysses*, Berlin 2022
- Jorge Luis Borges: *Das Sandbuch*, in: ders., Spiegel und Maske. Erzählungen, Frankfurt a. ‌M. 2022,
- Friedrich Nietzsche: *Die Geburt der Tragödie aus dem Geiste der Musik*
- Augustinus: *De civitate Dei contra paganos*
- Howard W. French: *Afrika und die Entstehung der modernen Welt. Eine Globalgeschichte*, Stuttgart 2023. [Orginal: *Born in Blackness: Africa, Africans, and the Making of the Modern World, 1471 to the Second World War* 20242]
- Robert Bartlett: *The Making of Europe. Conquest, Colonization and Cultural Change 950-1350*, London 1993 [deutsch: *Die Geburt Europas aus dem Geist der Gewalt 950-1350*, München 1996].
- Ernst Robert Curtius: *Europäische Literatur und lateinisches Mittelalter*, Bern/München 1967 [1948]
- Bernhard Jussen: *Das Geschenk des Orest. Eine Geschichte des nachrömischen Europa 526-1535*, München 2023.
- Franz Overbeck: *Kirchenlexicon, Materialien. »Christentum und Kultur«, Werke und Nachlaß, Band 6.1*, hg. v. Barbara von Reibniz, S. 246, Stuttgart 1996.
- Eugen Rosenstock-Huessy: *Out of Revolution. Autobiography of Western Man*, Eugene 1969 [1938]
- Ernst H. Kantorowicz: *Die zwei Körper des Königs. Eine Studie zur politischen Theologie des Mittelalters*, München 1990,
- Claudio Magris: *Donau, Biographie eines Flusses*, München 2007 [1988]
- Fernand Braudel: *Das Mittelmeer und die mediterrane Welt in der Epoche Philipps II.*, Frankfurt a. ‌M. 1998
- Alexander Randa: *Das Weltreich. Wagnis und Auftrag Europas im 16. und 17. Jahrhundert*, Freiburg 1962.
- António Vieria (1608-1697)
