---
title: Blogs
category: rezeption
language: de
page-nr: 340
---


* ein Blog der des öfteren über ERH schreibt:
    Blog von Peter Leithart,  
    es sind meist kleine Happen, die Gedanken von ERH darstellen.  
    Er schrieb auch einige empfehelenswerte Essays:  
   *  [The Relevance of Eugen Rosenstock-Huessy](https://www.firstthings.com/web-exclusives/2007/06/the-relevance-of-eugen-rosenst)
   *  Why study Rosenstock-Huessy
   *  Tribalism
   *  Jews and Gentiles
   *  Metabolism of Science and its addendum World, Nature, Physis
   *  The Cross of Eugen Rosenstock-Huessy
   *  Fathers and Sons
* Dieter Schlesak: Ist unsere Zukunft offen?
* Erik Ritter von Kuehnelt-Leddihn: Eugen Rosenstock-Huessy und die Deutsche Revolution
* Eberhard le Coutre: Planet – Sprache – Dienst
