---
title: "Wim Leenman & Bob O'Brien: Planetarische Standorte gründen"
category: rezeption
language: de
page-nr: 320
---
Die Idee einer neuen, sozialen Institution, *„Planetarische Standorte”*, als Weiterentwicklung von Freiwilligendiensten ist entstanden aus 55-jähriger Erfahrung mit Workcamps für Freiwillige und 25 Jahren Gemeinschaftsleben. Solch eine neue Institution ist historisch vergleichbar mit der monastischen Geschichte, die mit einem Kloster im sechsten Jahrhundert begann und im Mittelalter zu Tausenden in ganz Europa anwuchs.  
Heute ist die soziale Stabilität durch eine fortschreitende Zersplitterung der urbanisierten, industrialisierten, an der Technologie orientierten Gesellschaft bedroht, und dies führt zu weitverbreiteter Einsamkeit, zu Entfremdung, zum Rückzug aus Bürgerverantwortung, zu einem Gefühl der Ohnmacht in den fortgeschrittenen Wirtschaftssystemen. Und dies beginnt bereits, die sich entwickelnden Nationen zu betreffen, - mit möglicherweise katastrophalen Folgen. Die Gründung einer neuen sozialen Institution von freiwilligen Diensten neben solch anderen großen Institutionen, wie Universitäten, Gewerkschaften, Kirchen, Rechtswesen und anderen sozialen Errungenschaften, böte eine konstruktive Antwort auf unsere mißliche Lage.

Pierre Cérésole, der allgemein als der Gründer der Freiwilligendienste im Jahre 1920 gilt, war inspiriert durch den Aufsatz *„A Moral Equivalent of War”* (Ein sittlicher Ausgleich des Krieges), - von William James 1910 veröffentlicht, nachdem dieser viele Jahre lang die Psychologie des Krieges erforscht hatte. James schrieb: *„... wenn ganze Nationen die Armee bilden und die Wissenschaft der Destruktion mit der Wissenschaft der Produktion im Blick auf intellektuelle Verfeinerung wetteifert, dann sehe ich voraus, daß Kriege absurd und infolge ihrer eigenen Ungeheuerlichkeit unmöglich werden”* (William James: The Moral Equivalent of War. Mössingen-Talheim 1995, S. 67).

Es gibt zwei wichtige Faktoren für unsere Not und unser Leiden: Krieg und Zersplitterung.  
Ist der Freiwilligendienst eine Antwort darauf?

Da der Freiwilligendienst gewachsen und heute Bestandteil von privaten und staatlichen Programmen auf dem ganzen Globus ist, können wir sehen, daß er sogar noch mehr darstellt als ,,einen sittlichen Ausgleich des Krieges". Er kann geistiges Leben neu erwecken und zu einer Aufwertung der Werte in der heutigen Gesellschaft führen, nicht nur indem er als Ausweg für die Energie, Phantasie und den Idealismus der Jugend, konstruktive Arbeit bereitstellt, sondern auch indem er dies in einem sozialen Rahmen tut, der alle gesellschaftlichen Klassen, alle Generationen und die besonderen Talente jedes einzelnen Individuums in einem Prozeß zusammenbringt, der Möglichkeiten der Freundschaft und des Verstehens zwischen allen Mitgliedern der menschlichen Familie bietet.  
Wenn sich diese Freiwilligen begegnen, indem sie die Leiden der Menschheit erleichtern, werden sie bald einen gemeinsamen geistigen Boden jenseits ihrer konfliktreichen Lebensumstände benötigen. Im Umgang mit diesem Vakuum sollten die Biographien von Männern und Frauen, die aus heutiger Sicht als Samenkörner der Vergangenheit für eine Weltzukunft erkennbar sind, Teil der Orientierung dieser politisch und spirituell heterogenen Gruppe von Freiwilligen sein. Nur jene menschlichen Samenkörner, die im Feld der ganzen Menschheit gesät wurden, können die Kraft haben, uns mit einem Geist auszustatten, der die Vielheit der Geister versöhnen kann. Unser Vorschlag unternimmt es, die jüngere Generation in die Geschichte der Menschheit herein zu rufen und Glauben und Hoffnung in eine gemeinsame Bestimmung neu zu entfachen.
Wir schlagen vor, ein Experiment in kleinerem Maßstab zu versuchen, und zwar in einer schon bestehenden Gemeinschaft, deren Mitglieder vorbereitet worden sind und die bereit sind, eine „Garnison” von Freiwilligen anzunehmen, die sich für einen Dienst von mindestens einem Jahr verpflichten sollen.
Die Gemeinschaft wäre eine dauerhafte Basis für aufeinanderfolgende Gruppen von Freiwilligen und würde dadurch Kontinuität schaffen, Kontakte mit dem Umfeld knüpfen, und eine Quelle von sozialem, politischem und ökonomischem Sachverstand sein, aus der die unerfahrenen Freiwilligen schöpfen könnten.  
Es wird eine praktische und eine spirituelle Erfahrung sein.

Wir können nur die Hauptelemente vorschlagen, die einen ,,Planetarischen Standort" ausmachen. Jeder „Planetarische Standort” wir seine eigene Identität entwickeln, so wie jede Universität ihre eigene Identität hat, obwohl ihre Grundbestandteile, -werte und -formen einen gemeinsamen Sinn ausdrücken und zu diesem beitragen.  
Die Gesellschaft ist wie ein Zuhause mit vielen Zimmern. Immer in der Geschichte wurde, wenn eine große Not entstand, ein neues Zimmer angebaut, welches das gesamte bis dahin bestehende Lebensmuster veränderte.  
Wir glauben, daß eine kritische Notlage eine neue Institution in unserer sozialen Struktur erfordert, welche die Einheit stärkt, ohne die Verschiedenartigkeit zu zerstören.
Die bestehenden Programme von Freiwilligendiensten selbst benötigen es, daß ,,Planetarische" Standorte geschaffen werden, wenn sie die Verdienste, die sie in der Geschichte erbracht haben, konsolidieren sollen. Wir wissen, daß Errungenschaften immer fragil sind und niemals als dauerhaft angesehen werden können und daß, wenn die Zeit für einen neuen Schritt reif ist, der Augenblick genutzt werden muß.  
Wir schlagen vor, das Experiment eines „Planetarischen Standortes” als Flaggschiff im Programm *„European Voluntary Service for Young People”* zu initiieren.

Haarlem, im September 1996

Verfasser:  
Wim Ph. Leenman und Bob O'Brien  
Rosenstock-Huessy-Huis  
Hagestraat 10  
NL 2011 CV Haarlem

Übersetzung: Anna Möckel
