---
title: Dissertationen
category: rezeption
language: de
page-nr: 350
---
* Wilfried Rohrbach: *Das Sprachdenken Eugen Rosenstock-Huessys*, Stuttgart 1973
* Ingrid Ritzkowsky: *Rosenstock-Huessys Konzeption einer Grammatik der Gesellschaft*, Dissertation FU Berlin, 1973
* Hans-Peter Veraguth: *Erwachsenenbildung in der Zusammenarbeit von Gesellschaft und 'ökumenischer Kirche'. Die religiösen Sozialisten und Eugen Rosenstock-Huessy als Mitarbeiter der Weimarer freien Volksbildung (1919-1933)*, Dissertation (Theol. Fak.). Zürich 1973.
* Manfred Schmid: *Eugen Rosenstock-Huessys Herausforderung der Philosophie: Grammatik statt Ontologie*, Dissertation Uni Wien 1976, Alber 2011
* Viktor Beyfuß: *Die soziologische Interpretation der europäischen Revolutionen im Werk Eugen Rosenstck-Huessys* (Diss. phil. Würzburg 1990), München 1991
* Ruth Mautner: *Im zeugenden Gespräch, Vergegenwärtigung der Sprachkunde Eugen Rosenstock-Huessys als Lehre vom Gestaltenwandel; Philosophische Ansätze einer sammelnd-weitersagenden Beschreibung*, Dissertation Uni Wien, 1994
* Werner Justus Manz: *Arbeit und Persönlichkeit. Betriebliche Erwachsenenbildung als wesentlicher Aspekt der Betriebspolitik, im Sinne von Eugen Rosenstock-Huessy*. Universität Oldenburg, 1997 (München, Mering 1998)
* Dominik Klenk: Metanomik. *Quellenlehren jenseits der Denkgesetze. Eugen Rosenstock-Huessys Wegbereitung vom ich-einsamen Denken der neuzeitlichen Philosophie zur gelebten Sprachvernunft*, Münster 2003
* Andreas Leutzsch: *Geschichte der Globalisierung als globalisierte Geschichte. Die historische Konstruktion der Weltgesellschaft bei Rosenstock-Huessy und Braudel*, Bielefeld, 2008 (Campus, 2010)

#### Diplomarbeit
* Ulrike Vittur: *"La grande e nobile impresa della parola" Eugen Friedrich Moritz Rosenstock-Huessy, filosofo del linguaggio*, Università degli Studi di Trento, 2001
