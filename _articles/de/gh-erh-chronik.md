---
title: "Gottfried Hofmann: Eugen Rosenstock-Huessy – Versuch einer Chronik seines Lebens"
category: lebensbild
order: 120
language: de
page-nr: 180
---

Gottfried Hofmann, der langjährige Betreuer des Eugen Rosenstock-Huessy Archives in Bethel, legt hier eine Arbeit vor, die im Laufe vieler Jahre entstanden ist, eine detaillierte Auflistung aller bisher bekannten Begebenheiten im Leben Rosenstock-Huessys. Diese Chronik ermöglicht erstmalig einen genauen Einblick in das lange, ereignisreiche Leben dieses großen Universalgelehrten zwischen 1888 und 1973. Das erscheint um so nötiger, als nicht nur keine befriedigende Biographie des wichtigen Sprachdenkers und Freundes Franz Rosenzweigs vorliegt, sondern leider auch viele falsche Daten im Umlauf sind. Eine Chronik kann zwar eine Biographie nicht ersetzen, aber diese Arbeit bildet dafür sicherlich eine notwendige Voraussetzung.

Gottfried Hofmann, geboren 1938, Schüler der Friedrich von Bodelschwingh-Schule in Bethel von 1956 bis 1959, Studium in Germanistik, Philosophie, Geschichte und Leibeserziehung in Münster und Berlin, Lehrer an Realschulen und der Martin-Niemöller Gesamtschule in Bielefeld. Er hatte in den Jahren 1974 bis 1978 engen Kontakt zu Dr. Georg Müller, dem Begründer des Archives in Bethel, und betreute das Archiv nach dessen Tode bis zum Jahre 2012. Er konnte in den Jahren 1999 und 2007 das Rosenstock-Huessy Archiv in Four Wells, Norwich, Vt. ordnen und inventarisieren. Seit 2012 stand ihm für seine Arbeit das Digital Archive zur Verfügung.


Gottfried Hofmann: Eugen Rosenstock-Huessy – Versuch einer Chronik seines Lebens, 2018, Agenda
