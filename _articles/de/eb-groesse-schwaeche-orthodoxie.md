---
title: "Ernst Benz: Größe und Schwäche der Orthodoxie"
category: rezeption
language: de
page-nr: 920
---
aus: Ernst Benz: Geist und Leben der Ostkirche, 1971

### INHALT
#### I. Die orthodoxe Ikone
1. Die Stellung der Ikone in der orthodoxen Frömmigkeit (6)
2. Die »Fremdheit« der Ikone (7) -
3. Der kirchlich-dogmatische Charakter der Ikone (8) -
4. Die Theologie der Ikone (9) -
5. Die »nicht von Händen gemachte« Ikone (9) -
6. Ikone und Liturgie (11) -
7. Die Grundgedanken der Liturgie der Ikonenweihe (12) -
8. Haupttypen der Ikonen und ihre dogmatische Begründung (14): Christus-Ikonen (14), Ikonen der HI. Dreifaltigkeit (15), Mutter-Gottes-Ikonen (16), Heiligen-Ikonen (17), Engel-Ikonen (17), Szenische Ikonen (18), Die kirchliche Fixierung der Ikonenmalerei (19), Ikone und Dogma (20)

#### II. Liturgie und Sakramente
1. Die Liturgie (21): Das himmlische Hochzeitsmahl in der Predigt Jesu (21), Das letzte Mahl Jesu (22), Das Brotbrechen der Urgemeinde und die Erscheinungen des Auferstandenen (22), Das Freuden-Pascha (23), Die liturgische Produktivität (24), Die Vereinheitlichung der Liturgie in der byzantinischen Reichskirche (25), Mannigfaltigkeit innerhalb der Liturgie (26), Die liturgischen Gesten (27), Die liturgischen Gewänder (28), Das liturgische Bild-Denken. »Verheißung« und »Erfüllung« (29) -
2. Die Sakramente (30): Wesen und Anzahl der Sakramente (30), Die Eucharistie als Mysterien-Drama (32), Vergegenwärtigung (34), Eucharistie und Gemeinde (36)

#### III. Dogma.
1. Allgemeine Bemerkungen (37): Orthodoxes Dogma und griechischer Geist (37), Dogma als Ausdruck des kirchlichen Bewußtseins (38), Dogma und Liturgie (38) -
2. Orthodoxes und römisch-katholisches Verständnis des Dogmas (39): Das abendländische Christentum (40), a) Die Religion als Rechtsverhältnis (40), b) Das rechtliche Verständnis der Kirche (40), c) Das rechtliche Denken in der Theologie (42); Das orthodoxe Christentum (43), a) Die mystische Grundhaltung (43), b) Das mystische Verständnis der Kirche (45), c) Vergottung (46), d) Der Primat der Liebe (47) -
3. Einige Haupt-Dogmen (49): Die Trinitäts-Lehre (49), Die Christologie (52), Die dogmatische Stellung der Gottesmutter (54)

#### IV. Verfassung und Recht der orthodoxen Kirche
1. Die Entstehung des Kirchenrechts (58) -
2. Die drei Dämme gegen die Häresie (59): Der apostolische Kanon (59), Die apostolische Glaubensregel (60), Die apostolische Sukzession der Bischöfe (60) -
3. Die Rechtsquellen der Kirche (61) -
4. Das Verfassungs-prinzip der Kirche (62)

#### V. Nationalkirchen, schismatische Kirchen, Emigrationskirchen
1. Vielgliedrigkeit der orthodoxen Kirche (66) -
2. Prinzipien der Gliederung (67): Die sprachliche und nationale Gliederung (67), Politische Faktoren der kirchlichen Gliederung (69) -
3. Die alten »schismatischen« Kirchen (70): Die nestorianische Kirche (71), Die monophysitischen Kirchen (71), Zentralismus und Partikularismus in der orthodoxen Kirche (72), Die Stellung des Ökumenischen Patriarchen im türkischen Reich (73), Die orthodoxen Emigrationskirchen (74)

#### VI. Das Mönchtum
1. Anfänge des altkirchlichen Mönchtums (76)
2. Das Eremitentum (77) -
3. Klöster (78) -
4. Unterschied des östlich-orthodoxen und des römisch-katholischen Mönchtums (79) -
5. Das Mönchtum in der byzantinischen Reichskirche (80) -
6. Russisches Mönchtum (81): Eremitenklöster und Höhlenklöster (81), Stifter-klöster (82), Kampf des koinobitischen und des Skiten-Mönchtums (83) -
7. Das orthodoxe Mönchtum heute (85): Haupttypen (85), Askese (86), Stellung zur Wissenschaft (87), Das russische Starzentum (87) -
9. Mönchtum und Mystik (88)

#### VII. Mission und Ausbreitung der orthodoxen Kirche .
1. Missionarisches Versagen der Orthodoxie? (91) -
2. Die Missionsmethode der byzantinischen Kirche (92) -
3. Mission und Volkstum (93) -
4. Die Mission unter den Germanen (94): Anfänge der Germanenmission (94), Der germanische Arianismus (95), Wulfila (96) -
5. Die Slawenmission. Kyrill und Method (97) -
6. Die Mission unter den Ostslawen (99): Die Mission im Nordosten Rußlands (99), Mission und Kolonisation im Nordosten Ruß-lands (100) -
7. Mission und Ausbreitung der russisch-orthodoxen Kirche (101): Mission vor dem Einfall der Tataren (101), Nach dem Einfall der Tataren (101), Die Einsiedlerbewegung (101) -
8. Die Stellung der russisch-orthodoxen Kirche in der Mongolenzeit (102): Rußland zwischen Rom und den Mongolen (102), Abwendung von Rom. Fürst Alexander Newskij (103), Die Lage der russischen Kirche in der Mongolenzeit (104), Scheitern der Union mit Rom (105), Orthodoxe Mission unter den Tataren (105), Die Missionierung Sibiriens und Alaskas (106), Die sprachschöpferische Leistung der russisch-orthodoxen Mission (109) -
9. Die Mission der Nestorianer auf asiatischem Boden (110): Die persische Kirche und ihre missionarische Ausbreitung (110), Die orthodoxe und die nestorianische Mission im chinesischen Reich (113), Die nestorianische Mission im Mongolenreich (115), Verhandlungen der Mongolen mit Rom (115), Christliche Einflüsse am Hof der Mongolen-Khane (117)

#### VIII. Die orthodoxe Kultur
1. Gibt es eine »orthodoxe Kultur«? (119) -
2. Ausfallerscheinungen und ihre Kompensation (120): Das Fehlen der Plastik im Bereich der kirchlichen Kunst (120), Ikonenmalerei und Mosaik-Kunst (120), Wandmalerei (121), Metallverzierung der Ikonen (122), Buchmalerei (123), Kunstgewerbe (123) -
3. Das Fehlen des Theaters. Blüte der Liturgie (124) -
4. Verbot der kirchlichen Instrumentalmusik. Hochblüte des Chorgesangs (125): Die dogmatische Begründung (125), Die Hochblüte des Chorgesangs (126)

#### IX. Die ethischen Ideen de Orthodoxie
1. Mangelhafte Erforschung der orthodoxen Sozialethik (128)
2. Die Liebesethik (128) -
3. Versagen der orthodoxen Ethik bei der Lösung der sozialen Frage? (130) -
4. Sozialethische Reformideen im orthodoxen Rußland (131): Der niedere Klerus (132), Grigorij Petrow (132), Priester Gapon (133), Sozialethische Reformideen im Rußland der Revolution von 1917 (134) -
5. Orthodoxe Wurzeln des russischen Bolschewismus? (135): Reich-Gottes-Erwartung und Sozialismus (135), »Sobornost« und bolschewistische Gemeinschaftsidee (136), »Sobornost« und »Mir« (137) -
6. Die sozialethische Aktivität der orthodoxen Kirche Griechenlands (138)

#### X. Die politischen Ideen der Orthodoxie
1. Eusebios von Caesarea als Begründer der orthodoxen staatskirchlichen Idee (140): Kaiser Konstantin als Urbild des »christlichen Kaisers« (140), Christliche Umbildung des antiken Gottkaisertums (141), Kaiser und Patriarch (142) -
2. Augustin als Begründer der römischen Kirchenidee und Staatsmetaphysik (143): Die Schuldfrage beim Zusammenbruch des heidnischen Imperiums (143), Augustin ignoriert den Mythos von Byzanz als dem »neuen Rom« (144), Die römische Kirche als Nachfolgerin des Imperium Romanum (145) -
3. Die Weiterbildung des byzantinischen Systems bei den Ostslawen (146) -
4. Unterschied zwischen dem byzantinischen und dem Moskauer System (148) -
5. Peter der Große (149)

#### XI. Rom, Byzanz, Moskau
1. Byzanz und Rom (150): Byzanz, das »neue Rom« (150), »Renovatio« (151), Abfall Roms von Byzanz (151), Streit um den Primat (152), Entfremdung (153) -
2. Moskau und Rom (153): Moskau, das »dritte Rom« (153), Die Wanderung des Kaiser-Adlers von Byzanz nach Moskau (154), Der russische Zar - der »neue Konstantin« (155), Die Briefe Filofejs (155), Das Patriarchat Moskau (156)

#### XII. Rußland und Europa
Gehört Rußland zu Asien oder zu Europa? (158) -
1. Rußland als Bestandteil Europas (158): Das Kiewer Reich (158), Das Moskauer Reich als »Vormauer der Christenheit« (159), Das Rußlandbild Leibnizens (160), Rußland als Retter Europas in den napoleonischen Kriegen. Die »Heilige Allianz« (161), Der Brand von Moskau (161), Die Slawophilen (162), Kritik am »faulen Westen« keine Absage an Europa (162), Der Krimkrieg als »Verrat Europas an Rußland« (163) -
2. Trennende Momente (165): Roms Kampf gegen die slawische Liturgie (165), Die Kreuzzüge (165), Die Auswirkung der Tatarenzeit (167), Die Zeit der »Großen Wirren« (168)

#### XIII. Die Orthodoxie innerhalb der Ökumene heute

#### XIV. Größe und Schwäche der Orthodoxie
1. Die Stärke der Orthodoxie (180) -
2. Die Schwächen der Orthodoxie (183): Das Staatskirchentum (183), Der Nationalismus (Phyletismus) (184), Die Verselbständigung der Liturgie (185), Preisgabe der »Welt« (186) -
3. Sind diese Schwächen unüberwindlich? (187)

#### Bibliographie
#### Register

---

### XIV. GRÖSSE UND SCHWÄCHE DER ORTHODOXIE

#### 1. Die Stärke der Orthodoxie

Innerhalb der heutigen Christenheit leuchtet die Orthodoxie durch eine einzigartige Größe hervor. Ihre Größe besteht einmal darin, daß sie die Fülle der altkirchlichen Katholizität getreulich bewahrt hat. Dies gilt für alle ihre Lebensgebiete.

Vor allem in ihrer Liturgie leben das altkirchliche Verständnis und die altkirchliche Praxis des Gottesdienstes unmittelbar weiter. Was immer die alte und die byzantinische Kirche auf dem Gebiet der liturgischen Dramatik, der Meditation und Kontemplation an Schönheit der Gebete und Hymnen geschaffen haben, ist in ihrer Liturgie in einer einzigartigen Weise vereinigt. Ebenso ist auch der Inhalt der Schrift des Alten und Neuen Testaments in Form von reichen, nach festen Regeln variierenden Lesungen immer gegenwärtig. Aber auch die gesamte geschichtliche Tradition der Kirche wird in Gestalt der Lesungen aus dem Leben der großen Heiligen und Mystiker der Kirche ständig vor der Gemeinde ausgebreitet (s. S. 28 ff).

Innerhalb des Sakramentsgottesdienstes hat die Predigt ihren festen Ort, ursprünglich in der Katechumenen-Messe im Anschluß an die Lesung des Evangelientextes, heute vielfach auch an anderer Stelle, so nach der Kommunion.
Wortgottesdienst und Sakramentgottesdienst sind sinngemäß ineinander-gefügt, so daß ihre totale Trennung, wie sie in dem reformatorischen Christentum des Westens erfolgte, niemals eintreten konnte. In der Liturgie ist auch die gesamte Fülle der altkirchlichen Lehre, wie sie in den sieben alt-kirchlichen ökumenischen Synoden definiert wurde, unmittelbar lebendig, und zwar in der Form des anbetenden Hymnus, in dem die Grundgedanken der orthodoxen Wahrheit betend vor Gott und der mitbetenden Gemeinde ausgebreitet werden (s. S. 54 ff). Die Trennung von Liturgie und Theologie, Anbetung und Dogma ist hier noch nicht vollzogen.

Diese Kirche hat sich ein ursprüngliches Bewußtsein der Ökumenizität und Katholizität bewahrt. Ihr Selbstverständnis als die eine heilige ökumenische und apostolische Kirche ist nicht auf einer Rechtsidee begründet, sondern auf dem Bewußtsein, den mystischen Leib Christi zu repräsentieren.

In diesem Kirchenbewußtsein gehören die himmlische und die irdische Kirche wie auch die Kirche der Entschlafenen unauflöslich zusammen. In der Liturgie wird sich die irdische Kirche ihrer Zugehörigkeit zur oberen Kirche bewußt; hier erfährt die irdische Gemeinde die Gegenwart der Engel, der Patriarchen, der Propheten, der Apostel, der Märtyrer, der Heiligen und Erlösten; hier erfährt sie in dem eucharistischen Sakrament die Ankunft ihres gegenwärtigen Herrn. Innerhalb dieses mystischen Leibes findet eine eigentümliche Kommunikation und Korrelation statt: In dieser Gemeinschaft sind die Gaben des Heiligen Geistes, die Kräfte der Vergebung, die Kräfte der Mitteilung des Heils, die Kräfte des stellvertretenden Leidens füreinander und die Kräfte der Fürbitte wirksam, die bis in den Bereich des Todes hinein wirken, denn Gott ist „ein Herr der Lebenden, nicht der Toten".

Diese Einheit und Katholizität der Kirche hat die urchristliche Verbindung eines echten Personalismus, einer Hochschätzung der Einzigartigkeit und Einmaligkeit der menschlichen Einzelpersönlichkeit mit dem urchristlichen Gemeinschaftsbewußtsein bewahrt. Die Katholizität der orthodoxen Kirche ist aber in keiner Weise uniform. Durch das Prinzip, jedem Volk das Evangelium, die Liturgie und die orthodoxe Lehre in seiner eigenen Sprache zu geben, hat sich die Orthodoxie an die natürliche volksmäßige Differenzierung der Menschheit angepaßt und eine große Fülle nationaler Kirchentümer hervorgebracht. Diese nationale Differenzierung erstreckt sich aber nicht nur auf die Sprache, sondern auch auf die kirchliche Tradition. Gerade dadurch hat sich die orthodoxe Kirche ungemein schöpferisch erwiesen, indem sie auf die einzelnen nationalen Kulturen der orthodoxen Völker einen starken religiösen, sozialen und sittlichen Einfluß ausgeübt und an der geistigen und politischen Entwicklung dieser Völker den stärksten Anteil genommen hat. Dies hat sich gerade in den Zeiten besonders segensreich ausgewirkt, in denen die politische Existenz der orthodoxen Völker bedroht oder vernichtet war.

Die Einheit der Orthodoxie inmitten dieser Mannigfaltigkeit wird garantiert durch die Einheit des neutestamentlichen und alttestamentlichen Kanons, durch die Einheit der bischöflichen Verfassung, die auf der apostolischen Sukzession beruht, durch die Einheit der Liturgie, die in allen Sprachen dieselbe ist, und durch die Einheit der Lehre und der Lehrtradition.

Die Kraft der Bewahrung innerhalb der orthodoxen Kirche ist um so bewundernswerter, als diese Kirche den größten geschichtlichen Katastrophen in Form von Verfolgungen aller Art vor allem vonseiten des Islams ausgesetzt war, die in weiten Gebieten zur Ausrottung der Orthodoxie geführt haben. Trotzdem hat die Orthodoxie das überlieferte altkirchliche liturgische und dogmatische Erbe mit größter Treue bis zum heutigen Tage bewahrt.

Dieses überkommene Erbe ist nun in keiner Weise, wie oft behauptet, ein historisches museales Requisit, sondern stellt eine lebendige, entwicklungsfähige Macht dar. In einem gewissen Sinn beruht gerade auf der Tatsache, daß innerhalb der Orthodoxie die Lehre nicht so genau bis in alle Einzelheiten hinein definiert und kirchenrechtlich fixiert ist, die Größe der Orthodoxie. Sie ist bei aller inneren Festigkeit durchaus entwicklungsfähig. Ihr System ist noch in keiner Weise abgeschlossen. Das charismatische Leben der Orthodoxie ist nicht in rechtlichen und institutionellen Formen definitiv eingeengt; auch nach der Seite der Theologie hin zeigt sie eine bedeutsame geistige Beweglichkeit, die schon darin zum Ausdruck kommt, daß die theologischen Lehrer häufig nicht ordinierte Priester, sondern Laien sind. Das charismatische Amt des Lehrers - didaskalos -, das von Anfang an als besonderes Amt neben dem Amt des Diakons, des Priesters und Bischofs bestand, hat sich in der Orthodoxie auf diese Weise weiter bewahrt.

Ein weiterer wesentlicher Zug ist ihr christlicher Universalismus. Dieser macht sich sowohl in der Betrachtung des Kosmos wie in der Betrachtung der Geschichte bemerkbar. Während innerhalb des westlichen Christentums die christliche Naturphilosophie aufs ganze gesehen weithin zurückgetreten ist, hat die Ostkirche ihr christliches Verständnis der Schöpfung in immer neuen Entwürfen einer christlichen Kosmologie und Naturphilosophie ausgedrückt. Das christliche Verständnis des Kosmos kommt vor allem darin zum Ausdruck, daß das Heilsgeschehen nicht nur als ein für den Menschen bestimmtes und innerhalb der menschlichen Geschichte und der menschlichen Gemeinschaft sich vollziehendes Ereignis gedacht wird, sondern als ein kosmisches Ereignis, in das die Entwicklung des gesamten Universums einbezogen ist. Anthropologie, Kosmologie und Heilslehre stehen in einem unauflöslichen Zusammenhang. Wie der Fall des Menschen nach orthodoxem Verständnis das gesamte Universum in die Empörung gegen Gott hineinriß und den Mächten der Sünde und des Todes auslieferte, so hat auch die Menschwerdung Gottes in Jesus Christus und seine Auferstehung kosmische Wirkungen. An dem durch Christus geschaffenen Heil hat auch die „gesamte Kreauts" teil, die sich mit dem Menschen „nach dem Tag der Erlösung sehnt". So werden am Ende der Zeiten bei der Erfüllung des Heils zusammen mit dem Menschen die alte Erde und der alte Himmel in die neue Erde und den neuen Himmel verwandelt. Die schöpferische Umgestaltung der Kreatur umfaßt mit dem Menschen das gesamte Universum.

Ebenso deutlich kommt die Größe dieses Universalismus im Verständnis der Heilsgeschichte selbst zum Ausdruck. Die heilsgeschichtliche Wirkung Gottes beschränkt sich nicht nur auf die schmale Tradition der alttestamentlichen Heilsgeschichte: Auch die übrigen Völker nehmen nach der Auffassung der orthodoxen Kirche an der heilsgeschichtlichen Entwicklung teil.

So findet der weltgeschichtliche Universalismus des orthodoxen Denkens seine Begründung in der Logos-Lehre: der göttliche Logos hat zwar vor seiner Menschwerdung vor allem in den alttestamentlichen Propheten gesprochen, aber er war auch im Bereich der Völker nicht unbezeugt, und schon Clemens von Alexandrien und Origenes haben darauf hingewiesen, daß sich die Spuren des göttlichen Logos auch in der griechischen, indischen, ägyptischen und persischen Philosophie finden. So ist die gesamte Menschheit von Anfang an in die Heilsgeschichte einbezogen, die in Jesus Christus ihr Ziel und ihre Erfüllung findet.

Zu der Größe der Orthodoxie gehört auch die Tatsache, daß sie allein den Gedanken der Schönheit Gottes bewahrt hat und nicht aufhört, sie in ihren Gebeten und Hymnen zu preisen. Dieser Gedanke der Schönheit Gottes ist nicht anwendbar auf den zornigen Gott der Gerechtigkeit und der Prädestination, wie ihn die abendländische Theologie beschreibt. Er ist als christliche Idee nur aufrechtzuerhalten in Verbindung mit einem Universalismus, der letztlich in Christus den Vollender des Kosmos und der Heilsgeschichte, den Triumphator nicht nur über die Sünde, sondern auch über die physische Form der Vernichtung, über den Tod, und den Überwinder der dämonischen Mächte des gesamten Kosmos sieht.

Damit hängt die Tatsache zusammen, daß die Orthodoxie auch in ihrer religiösen Grundhaltung den Urcharakter der Stimmung der christlichen Gemeinde bewahrt hat, nämlich die Chara, die Freude und den Jubel, die in den neutestamentlichen Schriften als das charakteristische Zeichen der Stimmung und Haltung der ältesten Gemeinde beschrieben wird. Diese Freude ist innerhalb der Orthodoxie die Grundstimmung des Gottesdienstes, vor allem des eucharistischen Gottesdienstes geblieben: die Freude der Vereinigung mit ihrem lebendigen Herrn. In dieser Freude äußert sich der Jubel darüber, daß die Mächte der Sünde und des Todes überwunden, daß die Dämonen besiegt sind, daß das Reich des Satans im Himmel schon gebrochen ist, daß das Böse im Grunde bereits überwunden ist, daß für den Wiedergeboren der neue Äon des Gotteslebens, der Gottesherrlichkeit und der Schönheit Gottes, das Leben der neuen Kreatur und des neuen Kosmos schon begonnen haben.

#### 2. Die Schwächen der Orthodoxie
Auf Grund ihrer inneren Struktur ist die orthodoxe Kirche vier Gefahren ausgesetzt, die alle auf einer Verschiebung des von der orthodoxen Kirchenlehre geforderten Gleichgewichtes zwischen ihren einzelnen Lebensbereichen bestehen.

##### Das Staatskirchentum

Die erste Gefahrenquelle ist das Staatskirchentum; sie besteht in einer Verschiebung des Gleichgewichtes zwischen Staat und Kirche zugunsten des Staates. So sehr von der orthodoxen Lehre eine „Harmonie" und „Symphonie" zwischen Staat und Kirche als Idealforderung erhoben wird, so sehr hat sich doch das Gleichgewicht innerhalb der Geschichte der Orthodoxie mit einer gewissen Regelmäßigkeit zugunsten eines Übergewichtes des Staates über die Kirche verschoben. Innerhalb der Orthodoxie ist die Kirche stets in Gefahr, auch ihre innere Freiheit an den Staat zu verlieren. Besonders charakteristisch hierfür ist die Entwicklung in Rußland. Die Tradition des dortigen Staatskirchentums wirkt sogar noch in der heutigen Patriarchatskirche weiter. Obwohl sie in einem atheistischen Staate lebt, der in seiner Verfassung die völlige Trennung von Kirche und Staat proklamiert hat, schlägt die alte staatskirchliche Tradition auch bei ihr wieder in der Form durch, daß sich ihre Hierarchie zum Instrument der Innen- und vor allem der Außenpolitik der Sowjetunion machen läßt.

Das orthodoxe Staatskirchentum hat aufs Ganze gesehen verhängnisvolle Folgen gehabt und dabei der Kirche mehr geschadet als dem Staat. Es hat den Staat verführt, mit seinen dem Wesen der Kirche durchaus unangemessenen Machtmitteln das innere Leben und die Gestaltung der Kirche zu beeinflussen mit der Grundtendenz, das Christentum seinen staatlichen Zwecken und Vorteilen anzupassen. Es hat die Kirche selbst verführt, statt der ihr eigentümlichen und wesengemäßen Mittel der Wirksamkeit die Mittel der politischen und polizeilichen Macht des Staates zu gebrauchen, die ihr bereits Kaiser Konstantin mit der nötigen Gewaltanwendung zur Verfügung stellte. Es hat damit auf alles, was Kirche heißt, den schwer wieder zu tilgenden Verdacht gewälzt, als bezwecke die Kirche etwas wesentlich anderes, als den Menschen durch das Evangelium den Glauben und die Freiheit der Kinder Gottes zu bringen. Das eigentliche Verhängnis aber ist: das Staatskirchentum hat das Evangelium zu einem Gesetz gemacht für die, die nicht daran glauben. Das Christentum aber wird seinem Wesen entfremdet, wenn es zum Gesetz für die Geborenen statt für die Wieder-geborenen gemacht wird.

##### Der Nationalismus (Phyletismus)

Das zweite Gefahrenmoment besteht in einer Verschiebung des Gleichgewichtes zwischen dem ökumenischen und dem nationalkirchlichen Bewußtsein der orthodoxen Kirche zugunsten des Nationalismus. Die Gefahr des Nationalismus ist bereits durch die eigentümliche Grundstruktur der Orthodoxie gegeben, die jedem Volk seine eigene Kultsprache und seine eigene Verfassung und kirchliche Autonomie zuzubilligen bereit ist. So hat sich sowohl in der Geschichte Rußlands als vor allem auch in der Geschichte der Balkanvölker die kirchliche Entwicklung aufs stärkste mit der staatlichen und nationalen Entwicklung verflochten.

Es wurde bereits dargestellt, wie bei dem Versuch der bulgarischen Kirche, sich im Zusammenhang mit der nationalen Befreiung von dem ökumenischen Patriarchat von Konstantinopel zu lösen und eine eigene autokephale Nationalkirche zu bilden, dieser kirchliche Nationalismus von seiten des ökumenischen Patriarchen als eine Häresie mit dem Namen des „Phyletismus" bezeichnet wurde. In Wirklichkeit ist der Phyletismus die eigentliche latente Häresie, von der die gesamte Orthodoxie ständig bedroht ist. Immer wieder macht sich im Bereich der orthodoxen Kirche gerade bei den modernen Versuchen einer engeren Verbindung ihrer Gliedkirchen bemerkbar, daß die nationalen Egoismen stärker sind als das ökumenische Bewußtsein der Zusammengehörigkeit. Selbst unter den Emigrationskirchen und deren ökumenisch gesinnten Weltorganisationen wie etwa dem orthodoxen Weltstudentenbund machen sich immer wieder nationale Spannungen bemerkbar, die die Zusammenarbeit zwischen Griechen, Arabern und Slawen aufs stärkste belasten. Aber auch in Syrien, in Palästina und Ägypten wird die Zusammenarbeit zwischen dem griechischen und arabischen bzw. koptischen Teil der Kirche durch Rivalitätskämpfe um die geistige und kirchenpolitische Führung innerhalb der Kirche erschwert. Es gehört zu der größten Schuld und Tragik dieser nationalen Aufsplitterung der Orthodoxie, daß z. B. gerade in den nationalen Streitigkeiten der Balkanvölker die orthodoxe Kirche sich selten als ein geistiges Einheitsband erwiesen hat, das stark genug gewesen wäre, die furchtbare, häufig sich wiederholende blutige Austragung dieser Konflikte zu verhindern. Aber auch die Emigrationskirchen sind durch die völkischen Gegensätze aufs stärkste zerrissen und bieten den Außenstehenden ein peinliches Bild der nationalen und politischen Zerklüftung, das in einem auffälligen Gegensatz zu dem ideellen ökumenischen Anspruch der Orthodoxie steht.

##### Die Verselbständigung der Liturgie

Die dritte Gefahr entsteht durch die Verschiebung des Gleichgewichts zwischen Verkündigung, Sakrament und sozialer Verwirklichung der Kirche zugunsten des Sakraments. Man kann sie als Gefahr des liturgischen Isolationismus bezeichnen. Das Übergewicht des Liturgischen und Sakramentalen führt leicht dazu, daß die Liturgie zu einer Schale wird, in die sich die Kirche wie eine Schildkröte zurückzieht und nur selten den Kopf heraussteckt. Diese Gefahr des liturgischen Isolationismus ist im Wesen der Orthodoxie selbst vorgegeben. Sie ist aber noch verstärkt worden durch die politischen Verhältnisse. So hat es die politische Entwicklung immer wieder mit sich gebracht, daß es die Inhaber der staatlichen Gewalt sehr gern sahen, wenn sich die Kirche auf ihre liturgisch-sakramentale Tätigkeit beschränkte und davon Abstand nahm, ihrer Pflicht der Verkündigung durch Pflege der Predigt nachzukommen oder gar auf die Verwirklichung der christlichen Ethik innerhalb des sozialen Lebens, innerhalb der Wirtschaft, der Politik und der Kultur zu drängen. Die orthodoxen Zaren, Kaiser und Könige wie auch ihre heutigen atheistischen Nachfolger haben in der Regel die Politik verfolgt, die liturgische Selbstisolierung der Kirche zu fördern, um die Kirche auf diese Weise zu verharmlosen und ihre Ansprüche auf eine christliche Formung der Welt zu lähmen.

Verstärkt wurde diese Entwicklung durch die Tatsache, daß die meisten orthodoxen Kirchen jahrhundertelang gezwungen waren, unter fremdgläubigen Herrschern und Regierungen ein nur geduldetes Leben zu führen.

Unter der Mongolen-, Araber- und Türkenherrschaft haben sich die einzelnen orthodoxen Kirchen Rußlands, des vorderen und mitteleren Orients in der Tat wie Schildkröten in ihre liturgische Schale zurückgezogen und zwangsläufig auf eine Aktivität nach außen verzichtet. Es ist deshalb nicht zu verwundern, wenn heute die orthodoxe Kirche in Rußland, die bereits auf eine vielhundertjährige Geschichte einer solchen Selbstisolierung unter der Herrschaft nicht-christlicher Regierungen zurückblicken kann, unter der Herrschaft des Bolschewismus eine ähnliche Haltung des liturgischen Isolationismus einnimmt. Diese Haltung hat zwar den Vorteil, daß sie die Orthodoxie in einem gewissen Sinn in einer einzigartigen Weise gegen nicht-christliche Einflüsse in ihrem innersten liturgischen Bereich immunisiert, sie birgt aber auch die Gefahr, daß diese aufgezwungene Dauerhaltung der Selbstisolierung leicht den Zustand einer Dauerlähmung annehmen kann.

##### Preisgabe der „Welt"

Damit ist bereits die vierte Gefahr genannt, die Verschiebung des Gleichgewichts zwischen dem Transzendentalismus, d. h. der Hinneigung der Kirche auf ihre Verbindung mit der himmlischen Welt, und der Aufgabe einer christlichen Erneuerung der Welt zugunsten des Transzendentalismus.

Der orthodoxe Christ erlebt im Gottesdienst die Begegnung mit der himmlischen Kirche, mit dem Himmelreich. Aus dieser Begegnung strömen ihm Gnade, Vergebung, Heil und Hoffnung zu. Wohl mit unter dem Druck der politischen Entwicklung hat die Orthodoxie einen Überschwang zum Jenseitigen hin aufzuweisen, der sie an der spezifischen Aufgabe des Christen hindert, als „Mitarbeiter Gottes", wie der Apostel Paulus sagt, die Welt im Geist Christi zu gestalten. Auch von hier aus wird die Gefahr eines Verzichts auf soziale Aktivität und kulturelle Verwirklichung der Kirche verstärkt. Bezeichnenderweise hat die christliche Mystik im Bereich der östlichen Orthodoxie Formen angenommen, in der die Welt und sogar der Nächste gänzlich aus dem Blick des Gläubigen heraustritt. Es entschwindet das Du des Nebenmenschen, und es bleibt nur noch die innere Begegnung zwischen dem Ich und dem transzendenten Es, in dem schließlich das Ich selbst verschwindet und untergeht. So hat sich die Orthodoxie oft nur zu bereitwillig von der Aufgabe dispensiert, die Welt christlich zu gestalten, und hat sich gern damit getröstet, daß diese Welt sowieso bis zum Jüngsten Tag „im Argen liegt". Die meisten sozialpolitischen Impulse auf russischem Boden sind nicht von orthodoxen Gläubigen, sondern von Gegnern der Kirche und der Religion ausgegangen - ganz im Gegensatz zu den angelsächsischen Ländern, wo die soziale Erneuerung im wesentlichen von bewußten Christen vor allem der freikirchlichen Kreise getragen wurde.

#### 3. Sind diese Schwächen unüberwindlich?

Diese Schwächen sind jedoch nicht unüberwindlich. Die Orthodoxie besitzt in sich durchaus die geistige Kraft, das teilweise verlorengegangene Gleichgewicht zwischen den einzelnen Sphären wiederherzustellen. Dies liegt allerdings nicht ausschließlich in ihrer Hand. So hängt vor allem die Wiederherstellung des Gleichgewichts zwischen Staat und Kirche in den allermeisten Fällen - vor allem im Bereich der Sowjetunion fast ausschließlich von der Haltung des Staates ab, d. h. von seiner Bereitschaft, der Kirche eine mehr oder minder große Aktivität im Bereich des öffentlichen Lebens zuzubilligen. Die Geschichte der Orthodoxie zeigt, daß es einer verantwortungsvollen Kirchenleitung durchaus möglich ist, auch die zeitweilig unterdrückte Dynamik der Kirche, die auf eine christliche Umgestaltung der Welt hindrängt, trotz langer Lähmung zur Geltung zu bringen.

Ebenso ist die Gefahr des Nationalismus innerhalb der Orthodoxie nicht unüberwindlich. Auch hier zeigt die Geschichte der orthodoxen Kirche, daß die ökumenische Idee die tragende Grundlage einer echten Zusammenarbeit verschiedenartiger und verschiedensprachiger Nationalkirchen sein kann. Gerade im Zeichen der modernen ökumenischen Bewegung, die auf der Welt-kirchenkonferenz von Stockholm 1925 vor die Weltöffentlichkeit trat, hat sich eine Belebung des Zusammengehörigkeitsbewußtseins innerhalb der einzelnen Nationalkirchen der Orthodoxie vollzogen. Die moderne ökumenische Bewegung hat zu einer Erneuerung des ursprünglichen ökumenischen Bewußtseins der Orthodoxie, zu einer Wiederentdeckung der „inneren Ökumene" der Orthodoxie geführt.

Auch die Gefahr des liturgischen Isolationismus ist durchaus überwindbar. Die Liturgie braucht nicht als ein Element der Selbstisolierung der Kirche zu wirken. Sie kann umgekehrt als Fundament einer Erneuerung des Geistes und des Lebens wirken. Die Liturgie enthält in sich sowohl die Kräfte einer Erneuerung der Theologie wie der Mystik wie der Heiligung und christlichen Durchformung des ganzen Lebens. Sie erträgt auch durchaus eine Erneuerung der Predigt. So ist es charakteristisch, daß die orthodoxen Erneuerungsbewegungen, die überall dort auf der Welt auftraten, wo die äußere, gewaltsame Beschränkung und Unterdrückung der Orthodoxie in Wegfall kam, gerade von der Liturgie ausgingen und in dieser ihr Lebenszentrum erblickten.

In dem biblischen Bericht von der Berufung Mose ist überliefert, daß Mose in der Wüste einen Busch sah, der „mit Feuer brannte und war doch nicht verzehrt" (2. Mose 3, 2), und „der Engel des Herrn erschien ihm in einer feurigen Flamme aus dem Busch", und als er hinging, „rief ihm Gott aus dem Busch und sprach: Tritt nicht herzu, zieh deine Schuhe aus von deinen Füßen, denn der Ort, darauf du stehst, ist ein heilig Land!".

Die orthodoxe Liturgie hat die verschiedenen mystisch-typologischen Deutungen bewahrt, die ihre großen Beter und Asketen dieser Szene gegeben haben. Sie erblickt darin drei Mysterien:

Das Mysterium der göttlichen Dreifaltigkeit:
- „Wie du dem Moses im Dornbusch in Gestalt des Feuers erschienest, wurdest, Wort des Vaters, du Engel genannt, der dein Kommen zu uns offenbarte, durch das du deutlich allen verkündetest der einen Urgottheit dreipersönliche Kraft.«

Das Mysterium der Menschwerdung:
- „Vertraut Geheimnissen, hat in heiliger Schau vorausgeschaut Moses dein Bild: den im Feuer nicht verbrennenden Dornbusch, o Jungfrau, über allen Tadel Erhabene. Denn der Bildner hat, wohnend in dir, dich über alle Gebilde Erhobene nicht verbrannt, Gottesbraut."

Das Mysterium der Gottesmutter:
- „Dich bildete der Dornbusch auf Sinai vor einst, der nicht verbrannte, o Jungfrau, in der Berührung des Feuers. Denn als Jungfrau gebarst du. Und überragend den Sinn bist du, Mutter-Jungfrau, Jungfrau geblieben."

Im tiefsten Sinne jedoch ist die orthodoxe Kirche selbst - hervorgegangen aus dem Mysterium der Menschwerdung und es in sich bewahrend, aufgesproßt in der Wüste als die Kirche der Asketen und Faster, zerzaust von den Sandstürmen der Verfolgung durch Glaubensfeinde und feindliche Glaubensgenossen, ausgedörrt durch unermeßliches Leid und innere wie äußere Anfechtung, aber doch unverbrannt, brennend vom Feuer des Heiligen Geistes, durchglüht von der Gottesliebe, durchstrahlt von der Hochzeitsfreude des himmlischen Gastmahls, durchleuchtet von der All-verklärenden Kraft des Auferstandenen -

DER BRENNENDE DORNBUSCH.
