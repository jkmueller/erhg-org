---
title: "Hartmut Rosa: Resonanztheorie"
category: rezeption
language: de
page-nr: 960
published: 2024-09-28
---
### Hartmut Rosa: *Eine Soziologie der Weltbeziehung*

Wenn Beschleunigung das Problem ist, dann ist Resonanz vielleicht die Lösung. So lautet die Kernthese dieses gefeierten Buches von Hartmut Rosa, das als Gründungsdokument einer Soziologie des guten Lebens gelesen werden kann. Anstatt Lebensqualität in der Währung von Ressourcen, Optionen und Glücksmomenten zu messen, müssen wir unseren Blick auf die Beziehung zur Welt richten, die dieses Leben prägt. Dass diese Beziehung immer häufiger gestört ist, hat viel mit der Steigerungslogik der Moderne zu tun, und zwar auf individueller wie kollektiver Ebene. Rosa nimmt die großen Krisen der Gegenwart in den Blick und weist einer resonanztheoretischen Erneuerung der Kritischen Theorie den Weg.

Hartmut Rosa: *Resonanz: Eine Soziologie der Weltbeziehung*, Berlin, 10 Mar. 2019

>*Leseprobe von [Hartmut Rosa: Resonanz]({{ 'assets/downloads/Rosa_Hartmut_Resonanz_intro.pdf' | relative_url }})*

### Teil 1: Die Grundelemente menschlicher Weltbeziehungen

### V. Resonanz und Entfremdung als Basiskategorien einer Weltbeziehungstheorie

#### 2. Intrinsische Interessen und Selbstwirksamkeitserwartungen
*p. 278*

Der Irrtum der Moderne bestünde dann nicht in der Hoffnung, durch die Erhöhung von Selbstwirksamkeitserwartungen eine Verbesserung der Weltbeziehung und damit der Lebensqualität zu suchen, sondern in der Verwechslung einer stummen, auf Beherrschung und Verfügbarmachung ausgerichteten und ergebnisorientierten Selbstwirksamkeit mit der Erfahrung resonanter, einwirkender, prozessorientierter und antwortorientierter Selbstwirksamkeit, welche nicht nur mit dem stets Unverfügbaren, Nichtbeherrschbaren, Widerständigen rechnet, sondern auf dieses sogar konstitutiv angewiesen bleibt. Das bedeutet aber auch, dass die Selbstwirksamkeitsstrategie der Moderne nicht per se falsch, sondern nur einseitig ist; dass sie zwar gewaltiges Entfremdungspotential birgt, aber zugleich die Ressourcen für resonante Weltbeziehungen bereitstellt.

Resonanz können wir nun aber mittels der Spiegelneuronen- und Empathietheorien einerseits und der Selbstwirksamkeitsforschung andererseits genauer bestimmen als ein spezifisch kognitives, affektives und leibliches Weltverhältnis, bei dem Subjekte auf der einen Seite durch einen bestimmten Weltausschnitt berührt und bisweilen bis in ihre neuronale Basis ›erschüttert‹ werden, bei dem sie aber auf der anderen Seite auch selbst ›antwortend‹, handelnd und einwirkend auf Welt bezogen sind und sich als wirksam erfahren - dies ist die Natur des Antwortverhältnisses oder des ›vibrierenden Drahtes‹ zwischen Subjekt und Welt. Die *leuchtenden Augen* eines Menschen können dann als sicht- und tendenziell messbares Indiz dafür gelesen werden, dass der ›Resonanzdraht‹ in beide Richtungen in Bewegung ist: Das Subjekt entwickeltein intrinsisches, tendenziell handlungsorientierendes und öffnendes Interesse *nach außen*, während es zugleich *von außen* in Schwingung versetzt oder affiziert wird.

Tatsächlich lässt sich auf diese Weise die Idee des *vibrierenden Drahtes* zwischen Subjekt und Welt emotionssoziologisch formalisieren als eine Erfahrung beziehungsweise als ein Zustand, in dem das Subjekt einerseits von einem Weltausschnitt affiziert, also berührt und bewegt wird, während es andererseits mit einer entgegenkommenden, nach außen gerichteten emotionalen Bewegung, mit intrinsischem Interesse (*Libido*) und entsprechenden Wirksamkeitserwartungen reagiert. *Affekt* (von lat. ad*facere* bzw. *afficere* - antun) und *Emotion* (von lat. *emovere* - hinausbewegen) bilden dann also den Drahts, dessen bidirektionale Schwingung sich in spielerischer Form vielleicht als Af‹-fekt und E→ motion darstellen ließe.

Diese Doppelbewegung hat in der Tat fast immer (auch) eine unmittelbare leibliche Seite. Gefühle im Sinne der Af‹-fekte gehen einher mit Empfindungen des Durchströmtwerdens, beispielsweise von Hitze oder Kälte, von angenehmen oder beunruhigenden Schauern, von Wallungen etc., während unsere E→motionen dann Ausdruck finden in Momenten des Sich-Aufrichtens oder Zusammensinkens, Lachens oder Weinens usw. Wie wir indessen im nächsten Kapitel sehen werden, bedeutet dies nicht, dass Resonanz einen Gefühlszustand bezeichnet: Sie ist vielmehr ein Beziehungsmodus, der gegenüber dem emotionalen Inhalt offenbleibt.

Wie eng die beiden Seiten miteinander verknüpft sind, zeigt sich nicht zuletzt an der unmittelbar leiblichen Dimension menschlicher Weltbezogenheit: Angst und Stress, so haben wir bereits gesehen, setzen nach den Befunden der Neuropsychologie die Resonanz- und Empathiefähigkeit signifikant herab und schränken sie systematisch ein - sie sind umgekehrt aber auch das unmittelbare Ergebnis niedriger Selbstwirksamkeitserwartungen unter herausfordernden Bedingungen: „Wenn Menschen versuchen, auf Herausforderungen oder Bedrohungen zu reagieren, denen gegenüber sie befürchten, nicht viel ausrichten zu können, erfahren sie Stress; ihr Herzschlag beschleunigt sich, ihr Blutdruck steigt, sie schütten Stresshormone aus und ihr Immunsystem baut ab.” Die Natur der Verbindung zwischen den beiden ›Drahtenden‹ zeigt sich darüber hinaus aber auch darin, wie sich die (generellen ebenso wie die spezifischen, die individuellen ebenso wie die kollektiven) Selbstwirksamkeitserwartungen von Subjekten erhöhen lassen. Abgesehen davon, dass sie mit jeder erfolgreich gemeisterten Aufgabe, mithin also durch eigene positive Vorerfahrungen, zu steigen tendieren, wachsen sie (ganz im Sinne der Spiegelneuronentheorie!) auch, wenn ein Subjekt andere (ähnliche Personen) bei der erfolgreichen Bewältigung einer Aufgabe beobachtet oder, wenn auch in deutlich geringerem Maße, wenn es verbal (durch ›gutes Zureden‹ und entsprechende Anerkennungssignale) bestärkt wird. Auch eine Verbesserung der leiblich-emotionalen Befindlichkeit, das heißt eine Reduktion von Stress- und Angst-symptomen, wirkt sich konsequenterweise bereits positiv auf Art und Qualität der Selbstwirksamkeitserwartungen aus - was noch einmal deutlich werden lässt, wie eng die kognitiv-evaluativen und die leiblich-affektiven Dimensionen der Weltbeziehung miteinander verzahnt sind.

Damit sind nun aber alle theoretischen Vorüberlegungen abgeschlossen und alle thematischen Bausteine zusammengetragen, die wir für eine präzise konzeptuelle Bestimmung von Resonanz und Entfremdung als den kategorialen Grundbegriffen einer systematischen Soziologie der Weltbeziehung benötigen. Sie zu leisten ist die Aufgabe der nun folgenden drei Abschnitte.

#### 3. RESONANZ

Es kann kein Zweifel daran bestehen, dass sich der Resonanzbegriff als Metapher zur Beschreibung von Beziehungsqualitäten in hohem Maße eignet und dass er ein enormes Anregungspotential für die Untersuchung von Weltverhältnissen auf nahezu allen Feldern des menschlichen Lebens entfaltet. Allein, eine solche metaphorische Verwendung des Begriffs genügt nicht den konzeptuellen und systematischen Anforderungen, um Resonanz als einen sozialphilosophischen Grundbegriff und eine sozialwissenschaftliche Analysekategorie zu etablieren, auf die sich eine umfassende Soziologie der Weltbeziehung aufbauen ließe. Daher gilt es nun, die bisher diskutierten Bausteine so zu definieren und in ihrem Verhältnis zueinander zu bestimmen, dass zumindest eine Arbeitsdefinition von Resonanz als sozialtheoretischer Kategorie entsteht.

*p.298*

***Resonanz***\
Resonanz ist eine durch Af‹-fizierung und E→motion, intrinsisches Interesse und Selbstwirksamkeitserwartung gebildete Form der Weltbeziehung, in der sich Subjekt und Welt gegenseitig berühren und zugleich transformieren. Resonanz ist keine Echo-, sondern eine Antwortbeziehung; sie setzt voraus, dass beide Seiten mit eigener Stimme sprechen, und dies ist nur dort möglich, wo starke Wertungen berührt werden. Resonanz impliziert ein Moment konstitutiver Unverfügbarkeit. Resonanzbeziehungen setzen voraus, dass Subjekt und Welt hinreichend
›geschlossen‹ bzw. konsistent sind, um mit je eigener Stimme zu sprechen, und offen genug, um sich affizieren oder erreichen zu lassen. Resonanz ist kein emotionaler Zustand, sondern ein Beziehungsmodus. Dieser ist gegenüber dem emotionalen Inhalt neutral. Daher können wir traurige Geschichten lieben.\
*Definition 1: Was ist Resonanz?*

#### 4. Entfremdung
*p.316*

***Entfremdung***\
Entfremdung bezeichnet eine spezifische Form der Weltbeziehung, in der Subjekt und Welt einander indifferent oder feindlich (repulsiv) und mithin innerlich unverbunden gegenüberstehen. Daher kann Entfremdung auch als *Beziehung der Beziehungslosigkeit* (Rahel Jaeggi) bestimmt werden.

Entfremdung definiert damit einen Zustand, in dem die, ›Weltanverwandlung‹ misslingt, so dass die Welt stets kalt, starr, abweisend und nichtresponsiv erscheint. Resonanz bildet daher ›das Andere‹ der Entfremdung - ihren Gegenbegriff.

Depression/Burnout heißt der Zustand, in dem alle Resonanzachsen stumm und taub geworden sind. Man ›hat‹ beispielsweise Familie, Arbeit, Verein, Religion etc., aber sie ›sagen‹ einem nichts: Es findet keine Berührung mehr statt, das Subjekt wird nicht mehr affiziert und erfährt keine Selbstwirksamkeit. Welt und Subjekt erscheinen deshalb gleichermaßen als bleich, tot und leer.\
*Definition 2: Was ist Entfremdung?*

#### 5. Die Dialektik von Resonanz und Entfremdung
*p.321*

Resonanz entsteht also niemals dort, wo alles ›reine Harmonie‹ ist, und auch nicht aus der Abwesenheit von Entfremdung, sondern sie ist vielmehr gerade umgekehrt *das Aufblitzen der Hoffnung auf Anverwandlung und Antwort in einer schweigenden Welt*.

*p.326*

Dass Philosophie und Sozialtheorie der Spätmoderne seit Lyotard und Derrida, seit Foucault und Deleuze, seit Rorty und Lévinas so sehr, so anhaltend und so stark auf Differenz setzen und auf die irreduzible Andersheit des Fremden pochen, während sie allen Identitäts-, Authentizitäts- und Integrationstheorien skeptisch oder rundheraus ablehnend gegenüberstehen, lässt sich dabei als eine Reaktion auf die übermächtige Tendenz moderner Gesellschaften zur nostrifizierenden Aneignung verstehen, welche versucht, sich das jeweils Fremde oder Andere ›einzuverleiben‹, anstatt zu ihm in eine Antwortbeziehung zu treten, welche die eigene Stimme dieses Anderen und damit dessen Unverfügbarkeit konstitutiv anerkennt. Postmoderne und poststrukturalistische Ansätze begegnen deshalb der Vorstellung einer Überwindung von Entfremdung und einer Etablierung von Resonanzverhältnissen tendenziell mit Skepsis, weil sie in ihr nur eine neue oder verschleierte Variante solcher Aneignung sehen.

Diese Skepsis lässt sich aber zerstreuen, wenn die Differenz zwischen Anverwandlung und Aneignung ernst genommen wird: Anverwandlung im Modus der Resonanz meint die Verflüssigung von Weltverhältnissen und Beziehungen, nicht deren Fixierung; sie meint die Möglichkeit des Neu- und Anderswerdens von Subjekt und Welt und damit auch: die Möglichkeit genuiner Begegnung mit dem Anderen und Fremden. Erst ein anverwandelter Weltausschnitt kann auf eine bedeutungsvolle Weise widersprechen - was etwas kategorial anderes ist als ein bloß dingliches oder feindliches Sich-Widersetzen.

Ebendeshalb besteht der Unterschied zwischen genuiner Freundschaft (als einer paradigmatischen Form eines Resonanzverhältnisses) und bloßer Bekanntschaft darin, dass Freunde sich trauen, einander Schmerzhaftes und Unangenehmes zu sagen, und sich damit zu erreichen vermögen, während Bekannte solche Themen vermeiden und sich an den neuralgischen Punkten zurückziehen - oder sich nur gegenseitig verletzen.

Daran wird deutlich, dass irritierender und erschütternder Widerspruch Element einer Resonanzbeziehung sein kann und nicht mit verletzender oder feindlicher Repulsion verwechselt werden darf. Widerspruchsfähigkeit und -bereitschaft, nicht die blinde Übereinstimmung, sind geradezu eine Voraussetzung für Resonanzbeziehungen, erst sie ermöglichen es dem Subjekt, einen Widerhall in der Welt zu finden, der mehr ist als ein Echo. Möglicherweise ist die Furcht davor, nichts zu hören, nur der indifferent schweigenden Welt zu begegnen, auf der Seite der Subjekte eine versteckte, aber wirkmächtige Antriebskraft für die soziale Beschleunigungsspirale der Spätmoderne. Wenn immer wieder beobachtet wird, dass spätmoderne Subjekte von sich aus dazu tendieren, alle (spontan) entstehenden zeitlichen Freiräume mit neuen Verpflichtungen und Aktivitäten zuzustellen und beispielsweise notorisch nach Fernbedienungen und Kommunikationsgeräten greifen, sobald sie einen Moment mit sich alleine und entpflichtet sind, so könnten sie damit unbewusst einer Logik folgen, welche die Resonanzerwartungen panisch auf die Zukunft verschiebt: Solange wir damit befasst sind, Aufgaben zu erledigen, To-do-Listen abzuarbeiten und Termine zu erfüllen, sind stumme, verdinglichte Weltbeziehungen unvermeidlich und gleichsam legitimiert - wir akzeptieren sie, um Ressourcen zu gewinnen und zu sichern, die uns ›später‹, wenn wir zu dem kommen, was wir ›eigentlich‹ wollen und sind, helfen sollen, ein gutes Leben zu haben und mithin Resonanz zu erfahren.

In diesem dominanten Alltagsmodus vertrauen wir darauf, dass unsere eigene Stimme und die Stimme der Welt schon noch hörbar werden, wenn wir nur erst die Zeit-Räume für diesen anderen Modus der Weltbeziehung finden. Werden wir dann aber plötzlich und unerwartet in einen Zustand der Muße versetzt, in dem wir uns selbst und der Welt ohne den Schutz und den Puffer der Aufgaben und Optionen begegnen, ergreift uns die Panik davor, dass das Schweigen anhalten könnte. Dann erst wird das ganze Ausmaß der spätmodernen Weltverhältnissen innewohnenden existentiellen Entfremdung spürbar.

Diese Weltverhältnisse in ihren entfremdenden wie resonanzverheißenden Qualitäten empirisch und analytisch näher zu bestimmen, ist die Aufgabe, der ich mich in Teil 2 dieses Buches widmen werde.

### Teil 2: Resonanzsphären und Resonanzachsen

#### VI . Einleitung: Resonanzsphären, Anerkennung und die Achsen der Weltbeziehung
#### VII . Horizontale Resonanzachsen
##### 1. Die Familie als Resonanzhafen in stürmischer See
##### 2. Freundschaft: Das menschliche Rühren und die Kraft der Verzeihung
##### 3. Politik: Die vier Stimmen der Demokratie
#### VIII . Diagonale Resonanzachsen
##### 1. Objektbeziehungen: »Die Dinge singen hör ich so gern«
##### 2. Arbeit: Wenn das Material zu antworten beginnt
##### 3. Schule als Resonanzraum
##### 4. Sport und Konsum als Versuche, sich zu spüren
#### IX: Vertikale Resonanzachsen
##### 1. Die Verheißung der Religion
##### 2. Die Stimme der Natur
##### 3. Die Kraft der Kunst

##### 4. Der Mantel der Geschichte
Vertikale Resonanzachsen sind dadurch gekennzeichnet, dass sie den Subjekten die Erfahrung einer konstitutiven Verbindung mit einer Macht eröffnen, die ihre Existenz im Ganzen betrifft oder umgreift.

Eine solche Macht lässt sich mit Karl Jaspers als ein »Umgreifendes« auch insofern verstehen, als sie stets über ihre einzelnen Manifestationen und Erscheinungen hinausgeht. So meint die Rede von *Der Natur* oder *Der Kunst* stets mehr als die zugehörigen Phänomene - und auch mehr als die Summe dieser Phänomene. Die entsprechende Kraft wird dabei als ein konstitutives Anderes erfahren, das niemals in Gänze anverwandelt werden kann, sondern stets ein Moment der Unverfügbarkeit aufweist und gerade dadurch mit eigener Stimme spricht. Die entsprechenden Erfahrungen kann man, wie Hans Joas darlegt, als Erfahrungen der Selbsttranszendenz bezeichnen, weil sie stets die Begegnung mit einer Kraft beschreiben, welche über das Subjekt hinausgeht. Wie ich am Beispiel der (Kraft der) Kunst, aber auch der (Stimme der) Natur zu zeigen versucht habe, muss diese Erfahrung keineswegs religiöser Art sein, das heißt, sie setzt weder den Glauben an noch gar die Existenz transzendenter oder metaphysischer Mächte voraus. Auch in den vertikalen Achsen geht es um Weltbeziehungen, nicht um Überweltliches, doch steht in dieser Dimension die Beziehung zur Ganzheit dessen, was uns als Subjekten gegenübersteht - was uns umgreift -, auf dem Spiel.

Die Kultur der Moderne eröffnet und ermöglicht solche Erfahrungen nun nicht nur auf den Feldern der Kunst und der Natur, sondern ebenso in der Dimension der historischen Zeitlichkeit, die ihr im Begriff der Geschichte ebenso wie jene zu einer im Kollektivsingular zum Ausdruck kommenden Totalität geworden ist. Dies ist vermutlich der Grund dafür, warum Erfahrungen geschichtlicher Resonanz zumindest im europäischen Kulturraum oft mit dem Präfix »Welt-« versehen werden: Indem von welthistorischen Ereignissen, von Weltgeschichte oder von Weltstunden die Rede ist, in denen der Hauch der Geschichte spürbar werde, wird diese als eine umfassende Macht konzeptualisiert, welche die Welt als ganze durchwirkt.

*p. 507*

Historische und biographische Resonanzen teilen miteinander die zeiliche Struktur einer Ko-präsenz von Vergangenem, Gegenwärtigem und Zukünftigem, die sich in einzelnen Augenblicken zu einer unmittelbar sinnlichen Erfahrung verdichten kann, oft sich aber auch auf dem Wege narrativer Resonanzen einstellt. Die Erfahrung geschichtlicher Resonanz wird dann und dort besonders mächtig, wo sich Lebensgeschichte und (Zeit- bzw. ›Welt‹-) Geschichte wechselseitig berühren, wo mithin die erfahrenden Subjekte gleichsam in die Geschichte eintreten beziehungsweise sich als Teil der Geschichte erleben.

*p. 512*

Damit zeigt sich, dass die Furcht vor einem *Ende der Geschichte* nicht der Struktur der Ereignisse, nicht dem faktischen Geschichtsverlauf als solchem gilt, sondern dem Verlust der *Geschichte als Resonanzraum*. Der Mantel der Geschichte scheint sich aufzulösen in einen fragmentarischen Flickenteppich aus zahllosen unverbundenen Einzelereignissen, die ohne inneren Zusammenhang bleiben, oder gar in lauter einzelne Fäden einer *histoire croisée*, deren notwendige Multiperspektivität Resonanzen wenn nicht zu verunmöglichen, so doch gewaltig zu erschweren scheint. Die Verbesserung des geschichtlichen Wissens, die Aufbereitung und Musealisierung historischer Artefakte können dagegen ebenso wenig ausrichten, wie die Schärfung des Umweltbewusstseins die Sorge vor dem Verlust der Natur als Resonanzraum zu mildern vermag. Tatsächlich sind die Parallelen zwischen der Sorge um die Natur und der Sorge um die Geschichte bemerkenswert: In beiden Fällen werden aus meiner Sicht die Resonanzqualitäten dieser Sphären übersehen und vor allem die Bedingungen ihrer Erhaltung verkannt. Deshalb ist auch die vorherrschende Reaktion auf die wahrgenommene Krise - eine Verbesserung des Verfügungswissens - letztlich inadäquat.

Gewiss ist die insbesondere im Europa der Sattelzeit sich herausbildende Konzeption einer Weltgeschichte im Kollektivsingular nicht die einzig mögliche Form, in der diachrone Resonanzen erfahrbar werden, in der Geschichtliches zum Sprechen gebracht werden kann. Fast alle Kulturen der Welt verfügen über spezifische Orte, Narrationen und Praktiken, mittels deren sie zu ihren Ahnen, zu Helden, Geistern oder mit spezifischen Ereignissen ihrer Vergangenheit eine transhistorische Verbindung herstellen. Die vielfältigen Formen und Bedeutungen etwa von Ahnengräbern, Bestattungskulten, Trauer- und Erinnerungsorten zeugen davon. Stets gehören zu ihnen spezifische Riten, in deren Vollzug an den entsprechend ›aufgeladenen‹ Orten Resonanz erzeugt wird. Solche Resonanzen gelten jedoch kaum jemals *der* Geschichte als solcher (ebenso wenig wie sie eine Verbindung mit der Natur herzustellen versuchen), sondern vielmehr einzelnen und spezifischen historischen Entitäten, Gestalten oder Mächten. Sollte meine im Kontext der Beschleunigungstheorie entworfene These also richtig sein, dass die moderne Konzeption einer gerichteten und eigenbewegten Geschichte an ihre Dynamisierungsgrenze gestoßen ist, jenseits deren sie notwendig zerbrechen muss, so dass sie wieder durch eine Historie der vielen (unverbundenen) Geschichten abgelöst wird, so muss das nicht gleichbedeutend sein mit dem Verlust historischer Resonanz als solcher.

Nichtsdestotrotz ist diachrone Resonanz nur unter bestimmten sozialen Bedingungen möglich, die zu bestimmen für eine Kritik der Resonanzverhältnisse unerlässlich ist, jedoch zugleich eine genaue Kenntnis der zeiträumlichen Struktur von Resonanzerfahrungen voraussetzt. Zu vermuten steht, dass ein gelingendes Leben ohne die Fähigkeit zur responsiven Anverwandlung der eigenen Biographie und der diese tragenden Kollektivgeschichte kaum möglich ist. In Teil 3 dieses Buches werde ich daher versuchen, meine Analyse jener Bedingungen zu schärfen und zu präzisieren. Im Blick auf die Grundstruktur vertikaler Resonanzerfahrung gilt es jedoch zum Abschluss dieses Kapitels noch einmal festzuhalten, dass sie zunächst durch eine markante Verbindung und mentale Kopräsenz von Innen- und Außenwelt gekennzeichnet ist, welche die Rede von Resonanzachsen aus meiner Sicht rechtfertigt. So wie der Betende sich gleichzeitig nach außen und innen wendet und der Wüstenwanderer sich selbst im Sand sucht, wie er auf die Stimme der Natur nach außen und innen zugleich lauscht, richtet auch der Hörende oder Lesende seine Aufmerksamkeit nach außen und innen. Resonanz bezeichnet ihrem Wesen nach nichts anderes als die Herstellung oder Öffnung der entsprechenden Verbindungsachse, oder, metaphorisch gesprochen, das Vibrieren eines dazwischen aufgespannten Resonanzdrahtes. Die Erörterung der Geschichtserfahrung hat nun deutlich gemacht, dass diese Öffnung und Verbindung sich auch auf das Verhältnis von Vergangenheit, Gegenwart und Zukunft erstreckt: Innenwelt und Außenwelt, Vorwelt und Nachwelt werden in der Resonanzerfahrung auf eine Weise kopräsent, welche ihre Differenzen nicht verwischt oder einreißt, sondern vielmehr schärft und dabei in einem Hegel'schen Sinne ›aufhebt‹. In der (momenthaften) Wahrnehmung ihrer konstitutiven, dialogischen Verbundenheit besteht die Substanz vertikaler Resonanzachsen.

### Teil 3: Die Angst vor dem Verstummen der Welt: Eine resonanztheoretische Rekonstruktion der Moderne

### Teil 4: Eine kritische Theorie der Weltbeziehung

---

### Hartmut Rosa: *Unverfügbarkeit*

Das zentrale Bestreben der Moderne gilt der Vergrößerung der eigenen Reichweite: Die Welt soll ökonomisch und technisch verfügbar, wissenschaftlich erkennbar und beherrschbar, rechtlich berechenbar, politisch steuerbar und zugleich alltagspraktisch kontrollierbar und erfahrbar gemacht werden. Diese verfügbare Welt ist jedoch, so Hartmut Rosas brisante These, eine verstummte, mit ihr gibt es keinen Dialog mehr. Gegen diese fortschreitende Entfremdung von Mensch und Welt setzt Rosa die »Resonanz«, als klingende, unberechenbare Beziehung mit einer nicht-verfügbaren Welt. Zur Resonanz kommt es, wenn wir uns auf Fremdes, Irritierendes einlassen, auf all das, was sich außerhalb unserer kontrollierenden Reichweite befindet. Das Ergebnis dieses Prozesses lässt sich nicht vorhersagen oder planen, daher eignet dem Ereignis der Resonanz immer auch ein Moment der Unverfügbarkeit.

>*Leseprobe von [Hartmut Rosa: Unverfügbarkeit]({{ 'assets/downloads/Rosa_Hartmut_Unverfuegbarkeit_intro.pdf' | relative_url }})*

---

### Hartmut Rosa: *Spätmoderne in der Krise* Was leistet die Gesellschaftstheorie?

In Zeiten tiefgreifender sozialer Umbrüche und manifester Krisen schlägt die Stunde grundsätzlicher Analysen, welche die gegenwärtige Gesellschaft als ganze in den Blick nehmen, ihre Strukturmerkmale und Dynamiken untersuchen und vielleicht sogar Wege aus der krisenhaften Entwicklung aufzeigen. In jüngster Zeit haben Andreas Reckwitz und Hartmut Rosa großangelegte, jedoch ganz unterschiedlich akzentuierte Gesellschaftstheorien vorgelegt, welche die gegenwärtigen Debatten über die Spätmoderne maßgeblich bestimmen. In diesem gemeinsamen Buch treten sie nun in einen kritischen Dialog.

Ausgehend von dem geteilten Anliegen, dass die Analyse der Moderne als Sozialformation ins Zentrum einer Soziologie gehört, die ihre Aufgabe der Aufklärung der Gesellschaft über sich selbst ernst nimmt, entfalten sie in umfangreichen Essays zunächst ihre je eigene gesellschaftstheoretische Perspektive: Während Reckwitz »soziale Praktiken«, »Kontingenz« und »Singularisierung« als Leitbegriffe wählt, entscheidet sich Rosa für »Beschleunigung«, »Steigerung« und »Resonanz«. Im zweiten Teil des Buches spitzen sie ihre Positionen nochmals zu, arbeiten Gemeinsamkeiten heraus, markieren aber auch grundlegende Differenzen – und zwar im direkten, von Martin Bauer moderierten Gespräch.

>*Leseprobe von [Hartmut Rosa: Spätmoderne in der Krise]({{ 'assets/downloads/Reckwitz_Rosa_Spaetmoderne_in_der_Krise_intro.pdf' | relative_url }})*

#### Best Account: Skizze einer systematischen Theorie der modernen Gesellschaft

*p. 166*

Unter einem Best Account verstehe ich also in diesem Sinne und in Anlehnung an Charles Taylor den Versuch, in einer gegebenen soziohistorischen Lage auf der Grundlage aller zur Verfügung stehenden Ressourcen - heute etwa: statistische Daten, Interviews, Selbstbeobachtungen, aber auch Verfassungstexte, Gerichtsurteile, Zeitungsberichte, Schulbücher, Erzählungen sozialer Bewegungen, Literatur, Kunst, Kino etc. - und im Lichte der in ihr drängenden »Kulturprobleme« (Weber), aus denen sich die interesseleitenden Fragestellungen ergeben, den bestmöglichen (Selbst-)Deutungsvorschlag zu entwickeln. Ein solcher Deutungsvorschlag, das ist Taylors zentrale Einsicht, kommt nicht umhin, die Selbstdeutungen der Akteure kategorial ernst zu nehmen, weil sie die in Frage stehenden Phänomene mitkonstituieren.

*p. 175*

##### 1.3 Perspektivischer Dualismus und drei Ebenen des Best Account

Eine Formationsanalyse im Sinne des Best Account, so mein Argument, muss die Perspektive der ersten und die der dritten Person parallel einnehmen und die beiden Seiten analytisch unterscheiden, dabei aber sowohl in ihrem Eigensinn als auch in ihrem Zusammenwirken und in ihrer Verschränkung untersuchen. Erst dadurch werden die Konstitutionsgesetze und Bewegungskräfte einer sozialen Formation soziologisch erfassbar. Ohne ein (kulturelles) Verständnis der motivationalen Energien, der Begehrungen, Ambitionen und Ängste, der Verheißungen, Hoffnungen und Bedrohlichkeiten, welche diese Formation erzeugt, lassen sich die sozialen Veränderungsdynamiken und Bewegungsenergien nicht begreifen; aber ohne ein (strukturelles) Verständnis der institutionellen Wirklichkeit lässt sich die Form und Richtung jener Ängste und Begehrungen nicht verstehen. Erst in der Zusammenschau wird der Gesamtcharakter einer Sozialformation bestimmbar, der zugleich ein umfassendes, aber historisch spezifisches Weltverhältnis, eine spezifische Form der Weltbeziehung konstituiert.
