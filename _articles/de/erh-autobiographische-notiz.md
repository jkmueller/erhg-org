---
title: "Eugen Rosenstock-Huessy: eine autobiographische Notiz (1938)"
category: lebensbild
language: de
order: 3
page-nr: 110
---

![Eugen Rosenstock-Huessy, um1960]( {{ 'assets/images/rosenstock_150.png' | relative_url}}){:.img-left.img-small}

*„Meine Generation hat den sozialen Tod in allen Variationen überlebt, ich habe Jahrzehnte des Studierens und Lehrens in den scholastischen und akademischen Wissenschaften überlebt. Jeder ihrer ehrwürdigen Gelehrten hielt mich fälschlich für den intellektuellen Typ, den er am meisten verachtete.*   
*Der Atheist wünschte, ich sollte in die Theologie verschwinden.\
Die Theologen meinten, ich sei wohl ein Soziologe,\
die Soziologen murmelten: wahrscheinlich ein Historiker.\
Die Historiker waren darob entsetzt und riefen: ein Journalist.\
Aber die Journalisten verdammten mich als Metaphysiker.\
Die Metaphysiker ihrerseits hielten Wache am Tor der Philosophie\
  und fragten bei den Staatswissenschaftlern meinetwegen an.\
Die Juristen sind ja schon im Mittelalter als schlechte Christen bekannt gewesen,\
  und so wünschten sie mich in die Hölle.\
Damit konnte ich mich schließlich einverstanden erklären,\
denn als Mitglied der gegenwärtigen Gesellschaft\
kommt unsereiner aus der Hölle ja nur für Augenblicke heraus.”*

Eugen Rosenstock-Huessy: Out of Revolution, 1938 (Übersetzung: Eckart Wilkens)
