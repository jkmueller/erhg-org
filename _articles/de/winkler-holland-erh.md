---
title: "Heinrich August Winkler über Hollands Herrschaft und Rosenstock-Huessy"
category: rezeption
language: de
page-nr: 910
---

### Von Golgatha zur Gegenwart

***Tom Holland besteht mit Grund auf den christlichen Prägungen des
modernen Westens, lässt aber gute Argumente und Kronzeugen dafür aus und hält sich für ein wenig zu
originell.***

Es ist „die größte Geschichte aller Zeiten“, die der britische Autor und Publizist Tom Holland in sei-
nem neuesten Werk darstellen möchte: die Geschichte der Weltwirkungen des Christentums von Golgatha bis ins frühe 21. Jahrhundert. Nach erfolgreichen Büchern über Mohammed und das Perserreich und BBC-Sendungen über Homer, Herodot, Thukydides und Vergil legt der studierte Historiker und Literaturwissenschaftler jetzt ein Sachbuch vor, in dem er laut Vorwort zeigen möchte, „wie es dazu kam, dass wir im Westen wurden, wie wir sind und so denken, wie wir denken“.

Der Autor holt weit aus, um seine These zu belegen, dass die moderne Welt, auch wenn sie es nicht wahrhaben will, von christlichen Maßstäben geprägt ist. Soweit es um die alte und die mittelalterliche Geschichte geht, kann er sich dabei auf ausgedehnte Quellen- und Literaturstudien stützen. Er bezieht sich auf den britischen Mediävisten Robert I. Moore, wenn er von der „Revolution“ spricht, die Papst Gregor VII. im elften Jahrhundert nach Christus mit seiner Kampfansage an Kaiser und Könige auslöste, die der Kirche das Recht bestritten, Bischöfe aus eigener Machtvollkommenheit ein- zusetzen, und auch sonst die Kirche für ihre Zwecke instrumentalisieren wollten.
Die These von der „Papstrevolution“ des hohen Mittelalters ist freilich sehr viel älter. Es war nicht Moore, der sie 2000 in seinem Buch über die „erste europäische Revolution“ als Erster verfocht, sondern 1931 der deutsche Universalhistoriker Eugen Rosenstock-Huessy in seinem, nach 1945 mehrfach neu aufgelegten Werk „Die europäischen Revolutionen“.

Holland wertet das Wormser Konkordat von 1122, mit dem der „Investiturstreit“ zwischen Papst und Kaiser endete, trotz seines Kompromisscharakters als eindeutigen Sieg der Kirche. Er verkennt dabei die weltgeschichtliche Bedeutung der Auseinandersetzung zwischen den weltlichen Herrschern und dem Summus episcopus. Die Ausdifferenzierung von geistlicher und weltlicher Gewalt, für die das Wormser Konkordat und ähnliche Vereinbarungen mit den Königen Frankreichs und Englands stehen, war die erste einer Reihe von Gewaltenteilungen, die fortan die Geschichte des Okzidents prägten.

Der ansatzweisen Trennung von geistlicher und weltlicher Gewalt folgte die von fürstlicher und ständischer Gewalt (am markantesten in der englischen Magna Charta von 1215) und schließlich die klassische Gewaltenteilung, die Trennung von gesetzgebender, ausführender und rechtsprechender Gewalt, wie sie Montesquieu 1748 in seinem „Geist der Gesetze“ beschrieb. All das gab es nur im lateinischen Europa, dem Europa der Weltkirche, nicht im orthodoxen Europa, wo die geistliche Gewalt der weltlichen untergeordnet blieb, sich also nicht jener „Geist des Dualismus“ entwickeln konnte, der nach dem Urteil des deutschen Historikers Otto Hintze das westliche Europa prägte und ohne Übertreibung die Bedingung der Möglichkeit von Pluralismus und Freiheit genannt werden kann.

Holland erwähnt zwar, wenn auch eher beiläufig, das Wort von Jesus: „Gebt dem Kaiser, was des Kaisers ist, und Gott, was Gottes ist“. Dass diese kategorische Unterscheidung der Sphären von Gott und Kaiser und damit von göttlichen und irdischen Geboten eine wesentliche Voraussetzung für die Säkularisierung der Welt und die Emanzipation des Menschen bildet, bleibt aber ungesagt. Das erstaunt umso mehr, als die weltliche Wirkungsgeschichte des Christentums doch das zentrale Thema des Buches ist.

Düstere Kapitel in der Geschichte des Christentums und der westlichen Zivilisation spart Holland nicht aus. Die Kreuzzüge werden knapp, die Kriege gegen christliche Ketzer wie die Katharer und die Hussiten breit abgehandelt, desgleichen der deutsche Bauernkrieg von 1525 und die blutigen Kriege der spanischen Konquistadoren in Mittel- und Südamerika. Holland geißelt Sklavenhandel und Sklaverei und würdigt den Kampf frommer Christen gegen diese Menschheitsverbrechen. Er betont die christlichen Ursprünge der Menschenrechte, verschweigt aber nicht, dass sie lange Zeit nur für Weiße galten. Er kommt auf den Kolonialismus im Zusammenhang mit Witwenverbrennungen in Britisch-Indien, auf den Militarismus im Kontext der Materialschlachten des Ersten Weltkriegs und den Rassismus mit dem Blick auf die südafrikanische Apartheidspolitik und die Rassendiskriminierung in den USA kurz zu sprechen. Um eine Analyse dieser „Ismen“ bemüht er sich aber nicht. Den deutschen Nationalsozialismus präsentiert er vorrangig als eine Horst-Wessel-Story.

![Holland Herrschaft]({{ 'assets/images/holland-herrschaft.png' | relative_url }}){:.img-right.img-small}
Die stärksten Partien des Buches sind sicherlich die über die Antike und das Mittelalter. Doch je näher der Autor der Gegenwart kommt, desto subjektiver, ja willkürlicher wirken seine Schwerpunktsetzungen. Für Hobbes und Locke, Kant und Hegel ist in diesem Buch ungeachtet seiner überwiegend ideengeschichtlichen Ausrichtung kein Platz, wohl aber für Tolkiens „Herr der Ringe“, die Beatles und eine Patentante des Autors, der er die Anregung zu seinem Werk verdankt. Er malt einzelne Ereignisse wie antike Kreuzigungen oder die Ungarnschlacht auf dem Lechfeld von 955 üppig aus; er liebt die dramatischen Effekte. Strukturen und Institutionen interessieren ihn weniger.

Mit den Popularisierungsstrategien des Geschichtsfernsehens wohl vertraut, mag Holland beim Schreiben seines Buches auch schon an seine Verfilmbarkeit gedacht haben. Der Lesbarkeit dürfte das zugutekommen, nicht aber der intellektuellen Durchdringung seines wahrhaft großen Themas. So berechtigt Hollands Insistieren auf den christlichen Prägungen des modernen Westens ist, so wenig stringent ist seine Beweisführung. Und so neu, wie er meint, ist seine Botschaft auch nicht. Das von der Verlagswerbung zitierte Urteil des Spectator, Hollands „Herrschaft“ sei „eines der seltenen Bücher, die unser Verständnis der Welt vollständig verändern“, ist eine gelinde Übertreibung.

TOM HOLLAND [Herrschaft](https://www.klett-cotta.de/buch/Geschichte/Herrschaft/117344) \
Klett-Cotta, Stuttgart 2021. 624 Seiten, 28 €
