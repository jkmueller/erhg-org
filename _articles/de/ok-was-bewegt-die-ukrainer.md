---
title: "Otto Kroesen: Was bewegt die Ukrainer?"
category: rezeption
language: de
page-nr: 980
published: 2024-12-25
---
### Was bewegt die Ukrainer?

#### Heldentum

Seit dem Einmarsch der russischen Armee in die Ukraine 2022 bin ich erstaunt über die Überzeugung, den Mut und das Engagement der Ukrainer, sich aus dem Griff Russlands zu lösen. Selenskyj gab den Ton an, als er das amerikanische Angebot, aus der Ukraine zu fliehen und in den Vereinigten Staaten Asyl zu suchen, ablehnte und sich entschloss zu bleiben.
<!--more-->
Doch damit gab er offenbar der ganzen Nation eine Stimme, denn eine große Zahl von Menschen aus allen Gesellschaftsschichten mobilisierten sich, um Russland mit allen Mitteln zurückzudrängen. Der Mut der Zivilbevölkerung hielt die russische Armee einen Monat lang in Bucha auf, etwa 25 km von Kiew entfernt, also etwa so weit wie die Reichweite der russischen Geschütze. Woher kam dieser Mut?

Der komplette Text: [Was bewegt die Ukrainer?](https://www.rosenstock-huessy.com/ok-was-bewegt-die-ukrainer/)
