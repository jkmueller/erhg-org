---
title: Radioberich über die Dailmer-Werkszeitung
category: einblick
language: de
page-nr: 720
---
![Schmide 1904]({{ 'assets/images/schmiede_1904.jpg' | relative_url }}){:.img-right.img-large}

Mittwoch, 25. August 2010, DLF Kultur

***«Vor 90 Jahren: Nach der Schließung des Daimler-Werks in Stuttgart-Untertürkheim erscheint die letzte Ausgabe der Werkzeitung»***

Eugen Rosenstock-Huessy hatte das Angebot, als Rechtsgelehrter an der Weimarer Verfassung mitzuarbeiten. Stattdessen entschloß er sich, bei Daimler in Stuttgart-Untertürkheim die erste Werkzeitung Deutschlands zu gründen.

Ein Gespräch mit dem Vorstandsmitglied der Daimler-Motoren-Gesellschaft, Dr. Paul Riebensahm, im April 1919 hatte den Weg freigemacht. Zwischen Juni 1919 und August 1920 erschienen 19 Ausgaben der Daimler-Werkzeitung. Eugen Rosenstock-Huessy wollte den Arbeitern nach den Erschütterungen des Ersten Weltkrieges wieder eine Sprache geben und die klassische Kluft zwischen ihnen und der Werksleitung überbrücken.

Die Zeitung verstand sich beileibe nicht als ein PR-Organ der Werksleitung. Eugen Rosenstock-Huessy lehnte es sogar ab, daß sein Name auf der Gehaltsliste von Daimler erscheint, und schrieb selber nur unter Pseudonym. Auch inhaltlich wurden nicht nur interne Themen aufgenommen. Neben Beiträgen über Metallverarbeitung erschienen auch Artikel zu ausgewählten Kunstwerken und zur Architektur neuer Reihenhaussiedlungen.

Im August 1920 besetzten radikalisierte Arbeiter das Daimler-Werk in Untertürkheim. Das Werk wurde vorübergehend geschlossen, die Werkzeitung war am Ende. – Diesem Ereignis vor 90 Jahren widmete sich der Beitrag des Deutschlandfunks / Deutschlandradios Kultur.

Das Manuskript der Sendung finden Sie [hier](https://www.deutschlandfunk.de/ein-eigenes-magazin-fuer-mitarbeiter.871.de.html?dram:article_id=127071)

Ein schöner [Überblicks-Artikel](https://media.daimler.com/marsMediaSite/ko/en/9914426/) zum Thema: Die Daimler-Werkzeitung von 1919 bis 1920

Lothar Mack\
CH-Thalheim, den 17. August 2010\
aktualisiert am 25. August
