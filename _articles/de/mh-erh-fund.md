---
title: "Mark Huessy: Der Eugen Rosenstock-Huessy Fund"
category: bericht
created: 2010
language: de
page-nr: 710
---

Der [Eugen Rosenstock-Huessy Fund](https://erhfund.org) setzt sich dafür ein, dass das Werk Rosenstock-Huessys gelesen, gehört und in Projekten angewandt werden kann. Konkret sorgt der Fund dafür, daß ca. 90 Titel durch seine Verlagstochter, Argo Books, lieferbar sind. Der Fund hat das Gesamtwerk auf Mikrofilm und anschliessend auf DVD gebracht und so erschließbar gemacht. Er hat auch die englisch-sprachigen Vorlesungen abgeschrieben und auf Datenträger gebracht. Dadurch ist das schriftlich erfasste Werk um 6.000 Seiten erweitert worden. 2009 ist der Fund dabei, die Audiofassung dieser Vorlesungen zu digitalisieren. Gleichzeitig läuft ein Projekt, ca. 30.000 Privatpapiere, vorwiegend Korrespondenz, zu digitalisieren.

Mark Huessy, 2010
