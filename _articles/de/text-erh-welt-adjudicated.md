---
title: Unzentrierte Welt
category: online-text
language: de
page-nr: 700
---
![Uncentered World]({{ 'assets/images/bipolar_world.jpg' | relative_url }}){:.img-right.img-xlarge}
![Uncentered World]({{ 'assets/images/bipolar_world2.jpg' | relative_url }}){:.img-left.img-xlarge}

Back Endpaper Map, The World Adjudicated to Nobody: Keine Nation oder Kontinent ist im Zentrum. Die Karte ist zweimal vorhanden. Eine bipolare, transversale, elliptische, flächengleiche Karte. Wie eine Mercator-Projektion verzerrt diese Karte die Formen; im Gegensatz zu einer Mercator-Karte stellt sie die Flächen exakt dar und zeigt alle möglichen Verbindungen über die Pole hinweg. Die erste Karte dieses Typs wurde, vielleicht in einer wenig beeindruckenden Technik, von Sir C. F. Close in Great Britain's Ord-nance Survey, Professional Papers, New Series, Band II, 1927, gezeichnet. Dies scheint in der ansonsten umfassenden Studie von C. H. Deetz und Oscar S. Adams, Elements of Map Projection, U. S. Department of Commerce, Coast and Geodetic Survey, Special Publication No. 68 (Fourth edition revised April 2, 1934), ausgelassen worden zu sein. Siehe auch C. H. Deetz, Cartography (Special Publication No. 205), S. 50 ff., Washington, 1936, und dieses Buch, S. 465, 604 f., 630, 715, 717, 729.

Eugen Rosenstock-Huessy: Out of Revolution, 1938
