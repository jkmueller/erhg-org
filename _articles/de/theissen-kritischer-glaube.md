---
title: "Gerd Theißen: Argumente für einen kritischen Glauben (1978)"
category: rezeption
language: de
page-nr: 940
published: 2024-08-04
---
![Theißen Kritischer Glaube]({{ 'assets/images/theissen_kritischer_glaube.jpg' | relative_url }}){:.img-right.img-small}
### oder: Was hält der Religionskritik stand?

Gerd Theißen benutzt die moderne Religionskritik als Instrument, um den christlichen Glauben auf seine Gültigkeit und Wahrheit zu überprüfen. In einer Analyse ideologiekritischer Ansätze macht er deutlich, daß religiöse Erfahrung in der Struktur menschlichen Daseins begründet ist. In Auseinandersetzung mit dem empirischen Wahrheitsbewußtsein weist Theißen nach, daß religiöse Vorstellungen teilweise auf objektiv fundierter Erfahrung basieren. Ausführlich beschreibt er verschiedene Formen gegenwärtiger religiöser Erfahrung und weist auf den notwendigen Zusammenhang von religiöser Erfahrung und ethischem Handeln hin. Theißen macht plausibel, warum er Christ ist und sich gleichzeitig zur Tradition moderner Aufklärung einschließlich ihrer sozialwissenschaftlichen Varianten bekennt.

#### Inhalt

- Vorwort
- 1 **Drei Ansätze der Religionskritik**
- 1.1 Die Kritik des historisch-relativierenden Bewußtseins
- 1.1.1 Relativierung aufgrund historisch-kritischer Methoden
- 1.1.2 Relativierung aufgrund historisch-kritischer Ergebnisse
- 1.2 Die Kritik des empirischen Wahrheitsbewußtseins
- 1.3 Die Kritik des ideologiekritischen Bewußtseins
- 1.3.1 Vier Thesen ideologiekritischer Religionstheorie
- 1.3.2 Kritik an den Auswirkungen der Religion
- 1.3.3 Kritik an den Entstehungsbedingungen der Religion
- 1.3.4 Ideologiekritik und die Frage nach der Wahrheit
- 1.4 Zusammenfassung
- 2 **Was hält der Kritik des ideologiekritischen Bewußtseins stand?**
- 2.1 Ist Religion etwas Pathologisches?
- 2.1.1 Transzendenz religiöser Erfahrung
- 2.1.2 Ambivalenz religiöser Erfahrung
- 2.1.3 Konkurrenz religiöser Erfahrung
- 2.1.4 Transparenz religiöser Erfahrung
- 2.1.5 Zusammenfassung
- 2.2 Ist Religion etwas Überholtes?
- 2.2.1 Die Geschichtlichkeit der Religion
- 2.2.2 Das »Überholtsein« der Religion
- 2.2.3 Die Notwendigkeit der Religion
- 2.2.4 Zusammenfassung
- 3 **Was hält der Kritik des empirischen Wahrheitsbewußtseins stand?**
- 3.1 Sind religiöse Vorstellungen Projektionen?
- 3.1.1 Religiöse und wissenschaftliche Erfahrung
- 3.1.2 Die Struktur religiöser Vorstellungen
- 3.1.3 Die Erfahrungsbasis religiöser Vorstellungen
- 3.1.4 Die religiöse Erfahrung der Naturordnung:\
  Nomologische Resonanz- und Absurditätserfahrung
- 3.1.5 Die religiöse Erfahrung des Mitmenschen:\
  Hermeneutische, solidarische und erotische Resonanz- und Absurditätserfahrung
  - a) Hermeneutische Resonanz- und Absurditätserfahrung
  - b) Solidarische Resonanz- und Absurditätserfahrung
  - c) Erotische Resonanz- und Absurditätserfahrung
- 3.1.6 Die religiöse Erfahrung des Lebendigen:\
  Organologische Resonanz- und Absurditätserfahrung
- 3.1.7 Die religiöse Erfahrung des Schönen:\
  Ästhetische Resonanz- und Absurditätserfahrung
- 3.1.8 Die religiöse Erfahrung des Seins:\
  Existenzielle Resonanz- und Absurditätserfahrung
- 3.1.9 Religiöse Erfahrung und Gottesverständnis
  - a) Das Symbol der Allmacht
  - b) Das Symbol der Person
  - c) Das Symbol der Herrseins
- 3.2 Sind religiöse Gebote Suggestionen?
- 3.2.1 Religiöse Erfahrung als Motivation ethischen Verhaltens
  - a) Religiöse Sensibilisierung für den Anregungscharakter der Wirklichkeit
  - b) Religiöses Vertrauen in den Verstärkungscharakter der Wirklichkeit
  - c) Religiöse Impulse zur Steuerung von Handlungen: Vorbild und Verantwortung
- 3.2.2 Religiöse Erfahrung als Grundlage ethischer Normen
- 3.2.3 Religiöse Erfahrung und die Krisen ethischen Handelns
- 3.2.4 Religiöse Symbole und ethisches Verhalten
- 4 **Was hält der Kritik des historisch-relativierenden Bewußtseins stand?**
- 4.1 Traditionsökonomische Überlegungen
- 4.2 Wirkungsgeschichtliche Überlegungen
- 4.3 Religionstheoretische Überlegungen
- 4.3.1 Fünf Probleme beim Umgang mit dem historischen Jesus
- 4.3.2 Die Wahrheit neutestamentlicher Christologie
- 4.4 Anthropologische Überlegungen
- Anmerkungen
