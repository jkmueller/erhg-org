---
title: Das Rosenstock-Huessy Huis
category: rezeption
language: de
page-nr: 400
---
Das [Rosenstock-Huessy Huis](https://www.rosenstock-huessy-huis.nl/geschiedenis/) in Haarlem wurde 1972 gegründet und war eine Lebensgemeinschaft bis in die 1990er Jahre.

Verschiedene Berichte wurden darüber veröffentlicht:
- in [Stimmstein 2](https://www.rosenstock-huessy.com/stimmstein/)
  - [Bas Leenman: Im Rosenstock-Huessy-Huis in Haarlem](https://www.rosenstock-huessy.com/bl-ins-rosenstock-huessy-huis/)
  - [Wim Leenman: Wie bestimmend ist Rosenstock für das Rosenstock-Huessy-Huis](https://www.rosenstock-huessy.com/wl-wie-bestimmend-ist-rosenstock-fuer-das-rosenstock-huessy-huis/)
