---
title: "Otto Kroesen: Intellektuelle Konstruktion und Lebendige Erfahrung"
category: rezeption
language: de
page-nr: 950
published: 2024-08-23
---


#### Ehrenberg und Rosenstock-Huessy über Dogma und Gesellschaft in Russland und Westeuropa

#### Zusammenfassung
Dieser Beitrag befasst sich mit der konstruktiven Verfahrenssweise des Westens im Gegensatz zur erfahrungsorientierten des Ostens, hier speziell der russischen Orthodoxie.
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}

Der komplette Text: [Intellektuelle Konstruktion und Lebendige Erfahrung](https://www.rosenstock-huessy.com/ok-intellektuelle-konstruktion-und-lebendige-erfahrung/)
