---
title: "Hans-Christof Kraus: Eugen Friedrich Moritz Rosenstock-Huessy (bis 1925 Rosenstock)
Historiker, Kulturphilosoph"
category: lebensbild
order: 110
language: de
page-nr: 160
---

\* 6.7.1888 Berlin-Steglitz  
† 24.2.1973 Four Wells bei Norwich (Vermont, USA)

Nach dem Besuch des Joachimsthalschen Gymnasiums in Berlin studierte Rosenstock Jurisprudenz, Geschichte und klassische Philologie in Zürich, Berlin und Heidelberg. 1909 in Heidelberg mit einer rechtsgeschichtlichen Studie (Herzogsgewalt u. Friedensschutz) zum Dr. iur. promoviert, habilitierte er sich zwei Jahre später – als damals jüngster Privatdozent Deutschlands – in Leipzig mit einer Arbeit zu „Ostfalens Rechtsliteratur unter Friedrich II.“ 1914 folgte seine Aufsehen erregende und als bahnbrechend angesehene historische Studie „Königshaus und Stämme in Deutschland zwischen 911 und 1250“. Am 1. Weltkrieg nahm Rosenstock als Offizier teil (EK I).

Gleichzeitig gehörte er einem Freundeskreis um Franz Rosenzweig (1886–1929) an, in dem theologische und religionsphilosophische Fragen vor dem existenziellen Hintergrund der Weltkrise diskutiert wurden. Der Neubeginn eines intensiven jüdisch-christlichen Dialogs wurde in dieser Zeit von Rosenstock (auch durch einen wichtigen Briefwechsel mit Rosenzweig 1916) maßgeblich mitgeprägt. 1919-21 redigierte er in Untertürkheim die „Daimler-Werkszeitung“, amtierte dann als Direktor der in Frankfurt/M. neubegründeten „Akademie der Arbeit“, wurde 1922 in Heidelberg zum Dr. phil. promoviert und nahm 1923 einen Ruf auf den Lehrstuhl für Deutsche Rechtsgeschichte, Bürgerliches, Handels- und Arbeitsrecht in Breslau an. In dieser Zeit entstanden bedeutende Arbeiten zur Industriesoziologie und zum Arbeits- und Wirtschaftsrecht.

Daneben betätigte er sich weiterhin im Bereich der Erwachsenenbildung und gehörte 1926 zu den Mitbegründern des „Löwenberger Arbeitslagers“, in dem Studenten, Arbeiter und Bauern durch gemeinsame Arbeit, durch Diskussionen und Gespräche traditionelle Klassengegensätze und schichtenspezifische Vorurteile überwinden sollten. Zu Rosenstock-Huessys Schülern und Mitstreitern in diesen Jahren zählten später einige der führenden Gestalten des „Kreisauer Kreises“: Adolf Reichwein (1898–1944), Helmuth James v. Moltke (1907–45), dessen Witwe Freya (* 1911) nach dem Tod seiner Frau seine Lebensgefährtin wurde, und Peter Yorck v. Wartenburg (1904–44).

1931 publizierte Rosenstock-Huessy die erste Fassung seines historiographischen Hauptwerkes „Die europäischen Revolutionen“, das er später noch zweimal in überarbeiteter Form vorlegte (21951, 31961). Es handelt sich um einen unter dem Eindruck der Geschichtsphilosophie Oswald Spenglers (1880–1936) entstandenen kühnen universalhistorischen Versuch, die Geschichte Europas als eine logische Abfolge von fünf großen „Umwälzungen“ zu rekonstruieren: Am Beginn steht die geistliche „Papstrevolution“ des 11. Jahrhunderts, ihr folgt die deutsche „Fürstenrevolution“ der Reformationszeit, der sich wiederum die englische „Parlamentsrevolution“ des 17., die französische Bürgerrevolution des 18. Jahrhunderts und schließlich die vorerst letzte, die proletarische Revolution Rußlands im frühen 20. Jahrhundert anschließen. Diese weniger auf empirischer Analyse als auf der in den Jahren nach dem 1. Weltkrieg verbreiteten „Wesensschau“ beruhende geschichtsphilosophische Deutung hat eine lebhafte, kontroverse Resonanz gefunden.

Schon am 1.2.1933 legte Rosenstock-Huessy sein Breslauer Lehramt nieder und emigrierte in die USA, wo er 1934 in Harvard kurze Zeit als „Kuno Francke Lecturer in German Art and Culture" wirkte. 1935 wechselte er als Professor für „Social Philosophy“ an das Dartmouth College, wo er bis 1957 lehrte und in dessen Nähe er seinen Wohnsitz „Four Wells“ bezog, zu dem eine kleine Landwirtschaft gehörte.

Nach dem 2. Weltkrieg lehrte er mehrmals als Gastprofessor in Deutschland (u. a. in Göttingen, Heidelberg, Münster u. Köln), kehrte jedoch nicht mehr auf Dauer zurück. Seine späteren theologischen, religionsphänomenologischen, kultursoziologischen und sprachphilosophischen Schriften waren in einer sperrigen, hermetischen und schwer zugänglichen Sprache formuliert. Als religiöser Denker und christl Gesellschaftsreformer, der zeitlebens darum bemüht war, die Dichotomie zwischen Vernunft und Wissenschaft auf der einen, dem Glauben und der religiösen Erfahrung auf der anderen Seite aufzubrechen, wird er noch heute rezipiert und intensiv diskutiert.

Auszeichnungen:

M. A. h. c. (Dartmouth College 1936);  
Dr. theol. h. c. (Münster 1958);  
Dr. phil. h. c. (Köln 1961);  
Gr. BVK (1960);  
LL. D. h. c. (Univ. California 1967);  
Eugen Rosenstock-Huessy Gesellschaft (Bethel, 1963).

Hans-Christof Kraus (2005)
