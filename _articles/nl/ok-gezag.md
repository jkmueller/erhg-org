---
title: "Otto Kroesen: Onderweg naar een andere vorm van gezag"
category: receptie
language: nl
page-nr: 325
---

### Inleiding

Er heeft zich in de westerse geschiedenis een opmerkelijke ontwikkeling voorgedaan
met betrekking tot de bron van gezag. De manier, waarop wij gezag ervaren is driemaal
in de westerse geschiedenis grondig veranderd. Om die verandering aan te duiden in
heb ik in de titel de term metabletica gebruikt, wat letterlijk betekent: "leer der
veranderingen". Allereerst heetft het christelijk godsbeeld een breuk teweeggebracht met
de kosmische levenservaring van de oudheid. De tweede verandering vindt plaats door
het utopische denken, dat vanaf de middeleeuwen zijn intrede doet. Wijzelf staan
midden in de derde omslag, waarin de bron van gezag niet meer gevonden wordt in een
verdwijnpunt achter de wereld, maar in de onmiddellijke verhouding met de andere
mens. Wij kunnen dat veranderingsproces volgen aan de hand van het gebruik van de
term "oneindig". Het christelijk denken stelt God als oneindig voor tegenover de eindige
kosmos van de oudheid. Het utopische denken past de term oneindig toe op de wereld
en de natuur. In onze tijd komt de ervaring open te liggen van de andere mens als
oncindig, in het appel van het gelaat. In al die drie wijzen van ervaren en denken duidt
de term "oneindig" op de bron van gezag. Datgene, wat gezag over ons heeft kan niet
cen constructie zijn van ons eigen denken, is aan ons onttrokken en in die zin oneindig.

### Augustinus: de oneindige God

Augustinus nam de ideeënleer van Plato over, maar met een belangrijke wijziging: bij
hem werden deze ideeën tot de gedachten van God. V0or Plato zijn de ideeën de
"vormen" waarin de kosmos buiten ons zich aan ons kennen presenteert. In de
veelvormigheid van de kosmos zoekt Plato de achterliggende eenheid in duurzame
gestalten. De vele bomen zijn een afspiegeling van de ene idee "boom". De vele
stadstaten van Griekenland zijn een afspiegeling van de idee "staat" (de Griekse polis).

Augustinus' opvatting van wat hij dan ook schepping noemt in plaats van kosmos komt
niet langer tot stand door schouwend naar buiten te kijken, maar door de blik naar
binnen te richten. Wij leren de schepping kennen door in Gods gedachten deel te nemen
en die zijn toegankelijk door de liefde. Het licht van God schijnt in de ziel. Die ervaring
van liefde en genade is alleen aan de mens in de eerste persoon gegeven. De
werkelijkheid wordt niet gekend buiten die persoonlijke betrokkenheid bij de
werkelijkheid om. Kennen wij Gods gedachten door zijn genade en liefde, dan zijn wij
niet meer burgers van deze wereld, maar van de stad van God. God is oneindig. Dat
betekent, dat zijn oordelen boven onze manier van oordelen verheven zijn en zijn
kennen ons kennen te boven gaat.

Wat Plato betreft, doen wij het goede, als wij het maar kennen. Dat is voor Augustinus
niet voldoende. Het goede doen eist ook een inzet van de wil. En de wil moet eerst door
genade geheeld zijn, voordat zij het goede kan doen. Liefde, een kracht van gene zijde,
moet de wil omvormen. Het gaat hier derhalve niet om een natuurkundig, maar om een
moreel godsbegrip. Tegenover 'oneindigheid' staat niet strikt genomen 'eindigheid,
maar menselijke gebrekkigheid, falen, schuld en morele onvolkomenheid. Met deze
manier van ervaren is de middeleeuwse kerk opgetreden tegen het geloof in voorouders,
het gezag van stammentradities, en het gezag van koning en keizer.

Vanaf het jaar 1000 vindt er een radicalisering plaats van deze manier van denken en
ervaren. LDe kerk is er niet langer op uit op basis van het moreel gezag van het evangelie
de macht van stammen en koningen alleen in te perken, maar wil hen overtroeven en
0ok in het wereldlijke leven dit hogere gezag laten gelden. Als God de mensen liefheeft,
dan kan dat de gebruikelijke omgang met de wereld niet onberoerd laten. Je kunt dan
niet de toestanden in de wereld meer nemen voor wat ze zijn. Zo komt de opvatting op,
dat de wereld niet noodzakelijk is zoals die is. Dit inzicht komt tot uitdrukking in de
contingentiestrijd in de middeleeuwen. De wereld is niet noodzakelijk zo als die zich
aan ons voordoet. Dat heeft als consequentie, dat mensen in de wereld kunnen ingrijpen
en er iets aan veranderen. Zo worden de natuur en de wereld tot een reservoir van
onbeperkte mogelijkheden. En daarmee neemt het utopische denken zijn aanvang, en
ook de ontwikkeling van techniek als een onderneming gedragen door religieuze
inspiratie. De ontraadseling - in de dubbele betekenis van onttovering en ontdekking -
van de wereld neemt nu zijn loop.

De religieuze aandacht verschuift daarmee van de openbaring van liefde en
barmhartigheid in Christus naar God als schepper en onderhouder van deze wereld. De
idee van het oneindige wordt vanaf nu ook betrokken op de wereld en de natuur.
Volgens Nicolaas van Cusa is de ruimte onbeperkt, maar zijn latere volgeling en
bewonderaar Giordano Bruno gebruikte het woord "oneindig" reeds voor. Volgens
More, voorganger van Newton, is God gelijk aan de oneindige ruimte, waarin hij
aanwezig gedacht wordt als een soort fluidum, dat de dingen bij elkaar houdt. Ook
Newton schijnt heimelijk zo gedacht te hebben. Descartes is in dat spoor verder gegan
en stelt de ruimte nog louter voor als een geometrische grootheid: uitgebreidheid.

### Descartes en Kant: het oneindige als garant van de wereld

In het spoor van de Augustinus richten Descartes en Kant eveneens de blik naar binnen.
Zij zoeken echter niet Gods gedachten tegenover de wereld, maar in plaats daarvan de
wetmatigheden en het model achter de wereld. De waargenomen fenomenen in de
buitenwereld worden door het verstand zo geordend, dat het komt tot een logische
Voorstelling van zaken. Dat is 0ok een vorm van secularisering. De dingen worden
onttoverd waargenomen, als louter mechanisme en los van enige geestelijke essentie,
0ok los van hun bestemming in de toekomst. Ze worden op afstand geplaatst: men laat
zich niet door de dingen imponeren. Men zoekt de samenhang achter de toevallige
fenomenen. Zo doet Newton het ook, als hij het vallen van de appel en de baan van de
maan rond de aarde in én natuurwet onderbrengt.

Nu de eeuwige ideeën van Plato nog een stap verder verinnerlijkt worden en tot onze
eigen bewustzijnsinhouden worden verklaard, ontstaat er een kloof tussen idee en
werkelijkheid, geest en materie, model en mechanica. Onze modellen kunnen immers
fout zijn. Descartes zelf ging al de mist in met het verklaren van de bloedsomloop, toen
hij, alle fenomenen in aanmerking genomen, meende dat het stromen van het bloed
verklaard moest worden uit een innerlijke warmtebron, alsof het hart het bloed dusdanig
zou verwarmen dat het bij elke nieuwe hartslag 'overkookte'. Zowel Columbus, de
astronauten op reis naar de maan, maar ook elke vrachtwagen, die een viaduct moet
oversteken, hebben te maken met de vraag of onze geestesconstructies betrouwbaar zijn:
spoort de werkelijkheid ook met de door ons geconstrueerde ideeën? Kan men de
berekeningen vertrouwen?

Descartes loste dit probleem op door de idee van het oneindige. Die kan niet uit ons
eindige ik voortkomen en moet dus wel van buiten komen. Er is dus een hogere macht
achter de werkelijkheid. Dat zou ook een "malin genie" kunnen zijn, iemand die een
gemeen spel met ons speelt. Maar hier komt het geloof te hulp! De ware God bedriegt
ons niet! En zo wordt de kloof tussen gedachte en de materiële werkelijkheid gedicht
door het geloof. Spinoza heeft later de kloof op een iets andere manier gedicht, nl. door
'denken' en 'uitgebreidheid' te verklaren tot attributen van één substantie, God of natuur.
Maar ook hier moest de kloof gedicht worden. Zelfs als Kant nog weer later dergelijke
"bewijzen' van God reduceert tot slechts een 'postulaat' van de praktische rede wij
weten niet of kunnen niet bewijzen, dat er een God achter de wereld zit, maar postuleren
dat slechts als voorwaarde van zinvol praktisch handelen - blijft de Godsidee garant
staan voor de praktische realiseerbaarheid of betrouwbaarheid van onze rationele
gedachtenconstructie.

Op het terrein van geloot en theologie deed zich dezelfde verandering in ervaring en
denken voor. Vertrouwen in God werd als vanzelf vertaald in geloof in de
voorzienigheid, geloof in Gods "plan" met de wereld. De geloofs- en gezagscrisis, waar
wij middenin staan heeft alles te maken met het einde van deze periode. Een tijd lang
kon het godsvertrouwen legitiem de vorm aannemen van geloof in de innerlijke
samenhang van de natuur, voorzienigheid, en de organisatie van de samenleving
overeenkomstig Gods plan, verstaan als een soort blauwdruk van gerechtigheid voor de
tockomst. Voor ons voldoet dat niet meer.

### Levinas: het oneindige als relatie tot de ander

De massieve verdinglijking van de mens gedurende twee wereldoorlogen 1s het
omslagpunt. Voor Levinas is niet langer het probleem, of er wel orde in de wereld
steekt. Eerder is er voor hem sprake van een teveel aan orde, of beter: een tirannieke
orde. Orde en wet vormen nu niet meer de oplossing voor de chaos van de samenleving,
maar worden zelf het probleem. Levinas stelt daarvoor in de plaats een sOort "anarchie
van verantwoordelijkheid als tegenwicht tegen een tirannieke totalitaire orde, of die nu
de orde is van het kennen en de wetenschap, die mensen als objecten behandelt, of de
"orde" van de oorlog, die mensen als materie behandelt. De "anarchie" schuilt daarin,
dat deze verantwoordelijkheid niet vanuit en centrum georganiseerd kan worden, maar
overal opduikt. Niemand heeft er het monopolie over. In de verantwoordelijkheid voor
de ander treft hem opnieuw de idee van het oneindige. Deze idee duidt nu niet meer op
een macht voorbij onze kennis, die garant staat voor de betrouwbaarheid van die kennis,
maar heeft betrekking op de andere mens, die aan greep van onze eindige kennis
ontsnapt. Zo wordt het idee van het oneindige concreet in de morele relatie tot de andere
mens. Wat wil dat zeggen?

Eindig is onze kennis omdat zij op begrip brengt en definieert. Er is niets, dat als
fenomeen verschijnt, wat ons kennen niet aankan. Nergens wordt de betekenisgeving -
"Sinngebung" aldus Husserl van het kennende subject doorbroken. Er is echter een
betrekking waarin dat toch gebeurt, waar zich als het ware "van buitenat" een
tegenwoordigheid opdringt, die niet in mijn kenactiviteit te vatten is. Dat is de morele
relatie tot de andere mens, die niet van mij uit betekenis krijgt, maar aan mij betekenis
geeft, d.w.z. mij benoemt en aan mij appelleert. Deze weerstand aan de greep van het
kennende subject in het aangezicht van de andere mens is oneindig, d.w.z. ontgrenst het
definiërende en objectiverende kennen.

De abstracte notie van het oneindige krijgt zijn concretisering in het aangezicht van de
andere mens, die in zijn essentiële misère aan mij appelleert en vraagt om antwoord.
Meer: het aangezicht van de andere mens roept mij wakker en maakt mij waakzaam!
Het waken van het bewustzijn, een oplettendheid die intensiever is dan louter "kennis
hebben van", heeft een morele connotatie. Het waken van het bewustzijn is in de grond
van de zaak een gevoeligheid voor de kwetsbaarheid van de ander, die in de naaktheid
van zijn aangezicht blootgesteld is aan mijn greep. Die kwetsbaarheid maakt, dat je
goed moet opletten! In de kern van het bewustzijn zelf is daarom een morele alertheid
present, een sensitiviteit voor de verontrustende mogelijkheid reeds geweld uit te
oefenen, en deze alertheid nu is "het oneindige" in mij. De greep, die het intellect in zijn
begrijpen doet, deze mogelijkheid om als kennend subject "te pakken", de ander in te
vangen in mijn begrip, schrikt als het ware halverwege de uitvoering zodanig van
zichzelf, dat zij verkeert in verantwoordelijkheid in de blootstelling aan de ander, tot
aan gijzelname en obsessie toe. Mijn innerlijkste identiteit is dit blootgesteld zijn aan...
Daar waar ik "ik" zeg, verwijst dit "ik" als ik-voor-de-ander naar deze blootstelling in de
verantwoordelijkheid. "Ik" zeggen is reeds zich verantwoordelijk stellen, antwoord
geven.

De rede is daarom niet het primaire feit van mijn bestaan. Voordat ik ontwaak, ben ik
veeleer genietend bij mijzelf aanwezig, ga ik op in de elementen, en leef ik naïef, als
een kracht, die zich uitleeft. Door de morele schok van de blootstelling in de
verantwoordelijkheid aan de ander wordt ik pas als rede wakker geroepen: nu moet ik
gaan nadenken, hoe ik mijn verantwoordelijkheid gestalte geef. Rationaliteit is niet een
primair en vanzelfsprekend gegeven, maar afgeleid, dat wil zeggen, het is een resultaat
uit deze botsing of spanning tussen genietend in het leven opgaan en geïnterpelleerd
worden vanwege de misère van de ander. Tot de waakzaamheid van de redelijkheid
wordt het ik, ik, wakker geroepen.

### Een andere bron van gezag

De ontdekking van Levinas staat niet op zichzelf. Zelf verwijst hij naar Rosenzweig en
diens taaldenken als bron van zijn filosofie. En op zijn beurt is Rosenzweig weer
ingebed in de zogenaamde Patmosgroep, een groep intellectuelen van vooral joodse
komaf, die gedurende en na de Eerste Wereldoorlog als het ware een nieuwe openbaring
en oriëntatie ontving. Vandaar ook de naam Patmosgroep, en verwijzing naar de
openbaringen van Johannes op het eiland Patmos. Zoals Johannes een wereld zag
instorten en een nieuwe tijd aankondigde, zo deden zij dat ook. De bron van zingeving
en oriëntatie was vanaf nu niet meer de wereld of de natuur met in het spoor daarvan
wetenschap en techniek, die de wereld beloofden te ordenen, maar die ook geleid
hebben tot de vernietiging van de oorlog. De bron is de taal, het feit zelf, dat mensen
met elkaar spreken en voor elkander aanspreekbaar zijn. Vanaf de 20e eeuw heeft de
taal dan ook in de filosofie steeds meer aandacht gekregen, zoals bij de Franse
structuralisten, maar 0ok bij Habermas en zIJn volgelingen en de constructivisten.
Overal heeft het inzicht terrein gewonnen, dat de werkelijkheid door de taal gecreëerd
wordt, of zoals men graag zegt, geconstrueerd. Niet altijd wordt in dit denken de
metafysische omslag onderkend, die eraan ten grondslag ligt. In het spoor tredend van
het taaldenken van Rosenzweig en Rosenstock-Huessy heeft Levinas deze omslag
filosofisch het meest scherp gearticuleerd.

Theologisch brengt dit opnieuw een verschuiving in aandacht met zich mee, nu van God
als schepper naar God als Geest. Zoals bij Pinksteren de Geest meerdere talen voor
elkaar toegankelijk maakte, zo heelt de heilige Geest de meertaligheid van de mens. De
heilige Geest is de helende Geest. In de verantwoordelijkheid als antwoord op het appel
van de ander "spreekt" de mens, en dat in een omvattende betekenis van blootstelling
aan diens appel. Levinas noemt dat het "spreken" in onderscheiding van het
"gesprokene". Levend spreken betekent het gestolde repertoire van de taal - het
gesprokene - vernieuwen in het "spreken", verstaan als antwoord, als woord dat teken is
van mij voor-de-ander, en een orde van vrede sticht, telkens anders in elke nieuwe
Situatie. Niet voor niets hangen de woorden zeggen en gezag etymologisch samen. In dit
levende spreken, hoe kwetsbaarheid ook - omdat ik kan weigeren te horen, omdat ik
met mijn concepten geweld kan oefenen - hebben mensen elkaar iets te zeggen. Dat is
de bron van alle geestelijk gezag. Geest zelf duidt op de taal, die mensen met elkaar
spreken. Eensgeestes worden mensen, die vanuit verschillende talen en invalshoeken tot
overeenstemming komen en in gemeenschappelijk handelen de dingen tot hun
bestemming kunnen voeren.

Dat sluit verschillen niet uit. Maar mensen blijven in hun verschillen niet onverschillig
voor elkaar, als zij voor elkaar de verantwoordelijkheid voelen. Dat betekent ook, dat de
verschillen tussen religieuze en culturele tradities niet uitgevlakt moeten worden of op
één noemer gebracht. Juist in hun verschillen hebben zowel mensen als tradities elkaar
iets te zeggen, vullen ze elkaar aan en meer nog: corrigeren zij elkaar. Hier ligt de
nieuwe imperatief v0or de komende honderden jaren. En deze nieuwe manier van
denken en ervaren dringt zich haast onweerstaanbaar op in een tijd, waarin niet ons
gebrek aan technisch kunnen het probleem is, maar waarin de eerste prioriteit is, dat
mensen, die een verschillende taal spreken vrede kunnen sluiten en gemeenschappelijk
richting kunnen geven aan hun technisch kunnen.

* Noble, David F., The Religion of Technology, Penguin Books, New York, 1997
* Koyré, Alexander, From the Closed World to the Infinite Universe, Hopkins, Baltimore,
1957
* Kroesen, Otto en Ravesteijn, Wim, De Toekomst als Opdracht Utopie, revoutie en
Techniek in Europa, in "Europa, Balans en Richting", Lannoo Campus, Tielt, 2003, blz.
137-1
* Levinas, Emmanuel, Totalité et lnfini, Nijhoff, Den Haag., 1963
* Picht, Georg, Glauben und Wissen, Klett Cotta, Stuttgart, 1991
* Rosenstock-Huessy, Eugen, Speech and Reality, Argo Books, Norwich, 1970
* Taylor, Sources of the Self, Harvard, Massachusets, 1989

Otto Kroesen: Onderweg naar een andere bron van gezag, in Speling: Tijdschrift voor bezinning, 59(3), 28-34, 2007
