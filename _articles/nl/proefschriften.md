---
title: Proefschriften
category: receptie
language: nl
page-nr: 350
---
* Adrie G. M. van Dinteren-Goosesens: *De mens lijfelijk in het kruis der werkelijkhjeid bij Eugen Rosenstock Huessy*, Doctoral thesis in Theology, Tilburg, 1985
* Bart Voorsluis: *Taal en Relationaliteit: Over de scheppende en verbindende kracht van taal volgens Eugen Rosenstock-Huessy*, Amsterdam 1988
