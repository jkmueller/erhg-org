---
title: "Victor Kal: Nietzsches ogenblik"
category: receptie
language: nl
page-nr:
published: 2025-02-19
---
### Interview
in: Trouw, 10-02-2025:\
[Hoe uitgerekend Friedrich ‘God is dood’ Nietzsche een religieus antwoord op het nihilisme kan bieden](https://www.trouw.nl/religie-filosofie/hoe-uitgerekend-friedrich-god-is-dood-nietzsche-een-religieus-antwoord-op-het-nihilisme-kan-bieden~bbd45cb3/)
In onze nihilistische samenleving zijn mensen moreel stuurloos, schrijft filosoof Victor Kal in zijn nieuwe boek *Nietzsches ogenblik*. Juist Nietzsche, die God doodverklaarde, heeft daar volgens Kal een religieus antwoord op.

### Boek
Victor Kal: [Nietzsches ogenblik](https://uitgeverijprometheus.nl/boeken/nietzsches-ogenblik-paperback/), Uitgeverij Prometeus, 2025
![Victor Kal: Nietzsches ogenblik]({{ 'assets/images/Victor_Kal_Nietzsches_ogenblik.jpg' | relative_url }}){:.img-right.img-small}

Van Nietzsche weet men dat hij de ‘dood van God’ propageert, en ook de ‘wil tot de macht’. Minder bekend is dat die ‘dood van God’ de weg opent voor een betovering die in feite religieus is, terwijl de ‘wil tot de macht’ duidt op de mogelijkheid om zich als mens niet langer ergens achter te verschuilen en voortaan als mens voluit ‘soeverein’ te zijn. Het is Nietzsche te doen om persoonlijke verantwoordelijkheid en om ernst die niets ontziet. Zulke verantwoordelijkheid en ernst zouden het nihilisme en de hypocrisie waarin de moderne samenleving zich heeft laten vangen moeten doorbreken.

Intussen laat Nietzsche vrijheid en verantwoordelijkheid teruggaan op het ogenblik waarin een mens alsnog waarneemt wat hem te doen staat, zo kwetsbaar als die mens zich daarmee maakt. Door dat ogenblik telkens opnieuw tegemoet te zien verwerft een leven tegelijk grote allure en bescheiden inhoud.

Nietzsches ogenblik betreft echter ook de gedachte dat Nietzsche heden ‘zijn moment heeft’. We maken immers mee hoe volksmenners er met een zeker gemak in slagen de geesten in beslag te nemen. Nietzsche maakt duidelijk wat voor mentaliteit vereist is, wil je deze trend doorbreken. Op het spel staat de morele ruggengraat van de vrije burger.
Zorgvuldige analyse van Nietzsches denken onthult niet alleen de betekenis van zijn soms lelijke provocaties, maar maakt ook de buitengewone liefde zichtbaar die hij mensen en dingen toedraagt. Dat denken is tegelijk religieus en emancipatoir; het tweede kan niet zonder het eerste. Ook maakt de lezer kennis met een Griekse Nietzsche, en zelfs met een joodse Nietzsche.

Victor Kal (1951) was van 1994 tot 2022 hoofddocent wijsbegeerte aan de Universiteit van Amsterdam. Eerder schreef hij *De list van Spinoza. De grote gelijkschakeling* (2020) en *Poetins filosoof. Alexander Doegin* (2023).

#### Inhoud
- Inleiding 7
- I *De wil tot de macht* 13
- II *De Übermensch* 47
- III *De herwaardering van alle waarden* 70
- IV Intermezzo: *religie in drievoud* 101
- V *De verspilling* 121
- VI *De verlossing van het toeval* 146
- VII *De eeuwige terugkomst* 172
- VIII *Nihilisme* 193
- IX *De goden* 220
- X *Nietzsche* 247
- *Slotbeschouwing* 275
- Noten 285
- Bibliografie 287

#### uit de [Inleiding](https://uitgeverijprometheus.nl/app/uploads/book/9789044655537-inkijkexemplaar.pdf)
Men heeft het denken van Nietzsche wel als volgt in twee zinnen samengevat: ‘**God is dood**’; en: ‘**Aan goed en kwaad zijn we voorbij**’. Beide zinnen kun je met gemak herleiden tot wat Nietzsche inderdaad schrijft. In de tekst die volgt verdedig ik echter, eveneens onder verwijzing naar het werk van Nietzsche, de volgende twee stellingen: ‘**God heeft zijn beste tijd nog voor de boeg**’; en: ‘**Wat goed en kwaad betreft staat de mensheid wel beschouwd nog maar aan het begin**’ – zoals iedereen kan waarnemen. Nihilisme en hypocrisie beheersen het toneel.\
‘**Nihilisme**’: *je aan de vraag hoe het moet onttrekken door je te verschuilen achter een façade van correctheid.* \
‘**Hypocrisie**’: *de zaken mooi voorstellen, terwijl de ellende niet te overzien is*; \
of: rechtvaardigheid aanhangen, als universeel principe, alleen niet als het over ‘buitenstaanders’ gaat. Kortom, de leugen regeert. Is de moderne wereld tot iets beters werkelijk niet in staat?\
...\
Het is mijn opzet **Nietzsche** te presenteren als ***religieus denker***, om precies te zijn als een ***revolutionair*** religieus denker.
