---
title: Wie is Eugen Rosenstock-Huessy
category: biografie
order: 1
language: nl
page-nr: 100
landingpage: front
order-lp: 1
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_jung.jpg' | relative_url }}){:.img-right.img-small}
Geboren op 6 juli 1888 in Berlijn, gestorven op 24 februari 1973 in Four Wells, Norwich, Vermont, USA
<!--more-->
Eugen Rosenstock-Huessy had van zichzelf kunnen zeggen dat hij veel levens heeft geleid:

* als kind van geëmancipeerde ouders van Joodse afkomst, opgegroeid tussen zes zussen;
* als rechtenstudent
* als jongste privaatdocent in 1914
* als oorlogsdeelnemer (Officier in Verdun)
* als eerste uitgever van de Daimler-Werkzeitung
* als eerste leider van de Akademie der Arbeit
* als hoogleraar in de Rechten in Breslau
* als initiatiefnemer en inspirator van werkkampen op basis van vrijwilligheid voor arbeiders, boeren en studenten in Silezië.
* als historicus, theoloog, socioloog voor Hitler aan de macht kwam.
* als emigrant 1933
* als immigrant van de Verenigde Staten
* als professor in Harvard en aan het Dartmouth College, Hanover, New Hampshire,
* als initiator van Camp William James,
* als stichter van de nieuwe richting in de volwasseneneducatie na de eerste wereldoorlog en in dezelfde geest ook na de tweede wereldoorlog in Duitsland
* als auteur van een sociologie, die systematisch heden en algemene geschiedenis omvat.

![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_5.jpg' | relative_url }}){:.img-left.img-small}

Rosenstock-Huessy onderkende met zijn fijne neus de historische gebeurtenissen van zijn tijd, die hij alert en met verrassende snelheid in grotere verbanden kon plaatsen. Hij gold als een van de degenen, die na de fundamentele ervaring van de ineenstorting van het Duitse Rijk na de eerste wereldoorlog, nieuwe wegen zocht om de sociale ordening van na de wereldoorlogen de weg te kunnen wijzen. Tot deze groep hoorden met name de auteurs van het tijdschrift ‘Die Kreatur”, dat van 1926 tot 1930 verscheen.[^1] De ontmoeting met [Franz Rosenzweig](http://www.ersterweltkrieg.eu/rosenzweig/rosenzweigichbleibealsojude.html) maakte hem duidelijk, dat ondanks het christendom ook het jodendom moet voortbestaan.

De door Rosenstock-Huessy geïnitieerde vrijwillige werkkampen voor arbeiders, boeren en studenten in Silezië legden de grondslag voor de verzetsbeweging rond Helmut James Graaf von Moltke. Walter Hammer (1888-1966) noemde hem daarom de aartsvader van de [Kreisauer Kreis](https://nl.wikipedia.org/wiki/Kreisauer_Kreis).

Meteen na de machtsovername van Hitler op 31 januari 1933 begreep hij wat dat voor de mensen van Joodse afkomst zou betekenen en hij verliet het land al in hetzelfde jaar. Als hoogleraar in de Verenigde Staten drukte hij zijn stempel op veel mensen. In Duitsland, waar hij pas weer in 1950 zijn stem kon laten horen, aan de universiteit van Göttingen, ging hij alleen nog maar om met mensen, die zich rechtstreeks door hem lieten inspireren. Zijn trouwste Europese luisteraars na 1945 zijn wel in Nederland te vinden.[^2]

Eckart Wilkens , Köln, april 2007  
vertaald door Wilmy Verhage

![Eugen Rosenstock-Huessy]({{ 'assets/images/110kk.jpg' | relative_url }}){:.img-right.img-small}

[^1]: Walter Benjamin, Nikolaj Berdjajew, Hugo Bergmann, Martin Buber, Edgar Dacqué, Hans Ehrenberg, Rudolf Ehrenberg, Marie Luise Enckendorff, M Gerschenson und W. Iwanow, Eberhard Grisebach, Willy Haas, Hermann Herrigel, Edith Klatt, Fritz Klatt, Georg Koch, Ernst Loewenthal, Ernst Michel, Wilhelm Michel, Albert Mirgeler, Karl Nötzel, Alfons Paquet, Werner Picht, Florens Christian Rang, Eugen Rosenstock-Huessy, Franz Rosenzweig, Heinrich Sachs, Leo Schestow, Justus Schwarz, Ernst Simon, Dolf Sternberger, Eduard Strauss, Ludwig Strauss, Hans Trüb, Viktor von Weizsäcker, Joseph Wittig.

[^2]: Zijn argument dat het bestaan van Duitsland qua geest en ziel er van afhing of men in staat was de martelaren van het Hitlerregime naar waarde te schatten, moet zijn werking sterk in de weg hebben gestaan in de periode dat Adenauer en Ulbricht aan de macht waren, toen zo’n gedachtegang uit verschillende gronden zeer onwelkom en impopulair was.
