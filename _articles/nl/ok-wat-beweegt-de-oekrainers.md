---
title: "Otto Kroesen: Wat beweegt de Oekraïners?"
category: receptie
language: nl
page-nr: 980
published: 2024-12-25
---

#### Heldenmoed

Vanaf de invasie van het Russische leger in Oekraïne 2022 heb ik versteld gestaan van de overtuiging, moed en inzet van de Oekraïners om zich te ontworstelen aan de greep van Rusland.
<!--more-->
Zelensky zette de toon toen hij het aanbod vanuit Amerika om weg te vluchten uit Oekraïne en asiel aan te vragen in de Verenigde Staten, afsloeg en besloot om te blijven. Maar hij gaf daarmee kennelijk stem aan heel de natie, want grote aantallen mensen uit alle lagen van de bevolking hebben zich ingezet om op alle manieren Rusland terug te drijven. Door burgermoed is het Russische leger in Bucha een maand lang tegengehouden, op zo’n 25 km van Kyjiv, ongeveer zo ver als het bereik van de kanonnen. Waar komt die moed vandaan?

De complete tekst: [Wat beweegt de Oekraïners?](https://www.respondeo.nl/ok-wat-beweegt-de-oekrainers/).
