---
title: Essays
category: receptie
language: nl
page-nr: 330
---

* Otto Kroesen: Technologie en het “Verlossende Woord, I , in 'In de Waagschaal', januari/februari 2003, jaargang 32 Publicaties 2003
* Otto Kroesen: Technologie en het “Verlossende Woord, II, in 'In de Waagschaal', januari/februari 2003, jaargang 32 Publicaties 2003
* Otto Kroesen: Technologie en het “Verlossende Woord, III, in 'In de Waagschaal', januari/februari 2003, jaargang 32 Publicaties 2003
* Otto Kroesen: [Islam en seculiere wetenschap](https://www.karlbarth.nl/kroesen-islam-wetenschap/), In de Waagschaal, 1 mei 2004, jaargang 33, blz. 13-18
* Otto Kroesen: [Openbaring, taal en geschiedenis](https://www.karlbarth.nl/kroesen-rosenstock-rosenzweig/), In de Waagschaal, 25 september 2004, jaargang 33, blz. 25-28
* Otto Kroesen: [Heilseconomie en heilsgeschiedenis (I)](https://www.karlbarth.nl/kroesen-rosenstock-levinas-kruis/), In de Waagschaal, 35(10), 10-14, 2006.
* Otto Kroesen: [Heilseconomie en heilsgeschiedenis (II)](https://www.karlbarth.nl/kroesen-rosenstock-globalisering/), In de Waagschaal, 35(12), 2006.
* Otto Kroesen: [Wat hebben joden, christenen en moslims elkaar te zeggen?](https://www.karlbarth.nl/kroesen-joden-christenen-moslims/), In de Waagschaal, 26 februari 2005, jaargang 34, blz. 10-13
* Reinier Gosker & Otto Kroesen & Henk van Olst: [Christus als centrum van de wereldgeschiedenis I](https://www.karlbarth.nl/gosker-ea-christus-centrum-wereldgeschiedenis-rosenstock/), in de Waagschaal, 36, 2007
* Reinier Gosker & Otto Kroesen & Henk van Olst: [Christus als centrum van de wereldgeschiedenis II](https://www.karlbarth.nl/gosker-ea-christus-centrum-rosenstock/), in de Waagschaal, 37, 2007
* Otto Kroesen: [Onderweg naar een andere bron van gezag]({{ 'ok-gezag' | relative_url }}), in Speling: Tijdschrift voor bezinning, 59(3), 28-34, 2007
