---
title: "Otto Kroesen: Eugen Rosenstock-Huessy - Gebieden van zijn denken"
category: biografie
order: 10
language: nl
page-nr: 130
---
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}


In 1888 geboren behoort Rosenstock-Huessy tot de generatie, die de verschrikkingen van de Eerste Wereld Oorlog bewust meegemaakt heeft. Hij heeft deze oorlog niet alleen geïnterpreteerd als een crisis van de Europese nationale staten, maar meer nog als een crisis van het objectiverende wetenschappelijke denken. De wetenschappen hadden niet alleen de natuur, maar ook de mens tot louter voorwerp gemaakt. Dat mensen op het slagveld tot dingen gemaakt werden is equivalent aan de objectivering en misvorming van levende menselijkheid in de analyse van de wetenschap. Het levenswerk van Rosenstock-Huessy bestaat erin niet deze of gene methode in het wetenschappelijk discours te veranderen, maar heel het referentiekader van de wetenschappen door middel van een nieuw paradigma te benaderen. De kloof tussen subject en object wordt overwonnen, doordat de mens waargenomen wordt als levend tussen de verworvenheden van het verleden en de uitdagingen van de toekomst. In deze spanning tussen verleden en toekomst heeft Eugen Rosenstock-Huessy ook zelf geleefd. In 1933 is hij naar de Verenigde Staten geëmigreerd, een nieuwe toekomst tegemoet. Daar heeft hij zijn grote sociologische werken geschreven, die ook aan de huidige generatie een interpretatie van het verleden ter beschikking stellen met het oog op een toekomstige geschiedenis van de mensheid. In 1973 is Eugen Rosenstock-Huessy in de Verenigde Staten gestorven.

## Taal

Het centrale thema van Rosenstock-Huessy is de taal. De taal is niet maar een communicatiemiddel, maar voert de regie over de communicatie: 'De taal is wijzer dan degene die haar spreekt'. Wij leven in de wendingen, interpretaties en articulaties van taalsedimenten, die uit het verleden tot ons komen, en de taal vernieuwt zich in onze mond, als wij antwoord geven op de uitdagingen die ons in onze tijd gesteld zijn. Alleen als het juiste woord bij onze huidige ervaringen gevonden is, opent zich een nieuwe toekomst, een weg die verder gaat. Sprakeloosheid is toekomstloosheid: als wij het verlossende woord, de juiste benaming voor onze situatie nog niet gevonden hebben, blijven wij nog gevangen op een doodlopende weg of in een vicieuze cirkel.

## Geschiedenis

De historische perioden zijn evenzovele spreekwijzen, rechtsverhoudingen, tijdbogen, waarin zich een taal uitbreidt over een bepaalde groep mensen. Sedert de middeleeuwen heeft zich bijvoorbeeld een tijdboog gevormd, waarin nationale staten met behulp van universele rechtsprincipes en algemeengeldige wetenschappelijke methoden tot ontwikkeling gekomen zijn. In de twee wereldoorlogen heeft deze taal zijn definitieve grens gevonden.

## Sociologie

In zijn sociologische werk volgt Rosenstock-Huessy de verschillende tijdenbogen en maatschappijordeningen. Hij beschrijft de geschiedenis als een dialoog tussen deze maatschappijordeningen. Telkens beërft en vernieuwt een maatschappijvorm de vorige. Toen de stammensamenleving op een doodlopende weg geraakt was, hebben de keizerrijken van Egypte en Babel (vgl. China en Mexico) een nieuwe hiërarchische en universelere maatschappijordening mogelijk gemaakt. Toen de hiërarchische maatschappijordening van Egypte het leven en de gerechtigheid verstikte, zijn de Hebreeën uitgetrokken en hebben voor het eerst in de geschiedenis een op de toekomst georiënteerde maatschappijordening gesticht. Het tijdperk van de kerk heeft deze oriëntatie op de toekomst beërfd  en uit de toekomst terug een brug geslagen naar de voorafgaande maatschappii-vormen.

## Het derde millennium

Rosenstock-Huessy heeft het als een taak voor het derde millennium gezien, de verbrokkeling en atomisering en de daarbij horende verwarring van alle overgeleverde spreekwijzen met behulp van levende taal tegemoet te treden. Waar de onderlinge verstandhouding tussen mensen geen vanzelfsprekendheid meer is, moet de taal bewust - dat betekent niet meer vanzelfsprekend - ingeoefend worden. De taal van het derde millennium zal een taal van weliswaar voorbijgaande maar intensief beoefenende gemeenschappen zijn. Rosenstock-Huessy heeft deze ontwikkeling geïnterpreteerd als een gezuiverd herleven van de erfenis van de stammen, zoals in het eerste millenium Israël’s ene God tot de hele wereld kwam door de kerk en in het tweede millennium  de oude rijken werden omgevormd tot globale wereldsamenhang verbonden in de natuurweten-schappen.

## De Rosenstock-Huessy Gesellschaft

De Eugen Rosenstock-Huessy Gesellschaft heeft tot doel de kiemen van de nieuwe taal, die tot uiting komt in de werken van Rosenstock-Huessy vruchtbaar te maken. Daartoe bevordert  zij de heruitgave van zijn werken, organiseert conferenties en studiedagen, beheert het Rosenstock-Huessy archief, en zet zich in voor de verwerkelijking van deze nieuwe taal in communicatievormen die passen bij de huidige samenleving.

Otto Kroesen, in het jaar 2003
