---
title: "Otto Kroesen: Intellectuele constructie en levende ervaring"
category: receptie
language: nl
page-nr: 950
published: 2024-08-23
---

### Ehrenberg en Rosenstock-Huessy over dogma en maatschappij in Rusland en West-Europa

#### Samenvatting

Deze bijdrage gaat over de activistisch constructivistische levenshouding van het Westen in contrast met de eerder passief ervarende van het Oosten, hier speciaal van de Russische orthodoxie.

<!--more-->
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}

De complete tekst: [Intellectuele constructie en levende ervaring](https://www.respondeo.nl/ok-intellectuelle-constructie-en-levende-ervaring/).
