---
title: Boeken
category: receptie
language: nl
page-nr: 360
---
* M. Darrol Bryant & Hans R Huessy (eds): *Eugen Rosenstock-Huessy: Studies in His Life and Thought*, Lewiston, NY: Mellen 1986
* Clinton C. Gardner (ed): *Life Lines: Quotations from the Work of Eugen Rosenstock-Huessy*, Norwich, Vermont: Argo Books 1988
* Bas Leenman & Lise van der Molen & Eckart Wilkens (eds): *Eugen Rosenstock-Huessy – Zum hundertsten Geburtstag*, Mössingen-Talheim: Talheimer Verlag 1990
* Ko Vos: *Auf Dem Weg Zum Planeten: Einsichten von Eugen Rosenstock-Huessy*, 1992
* Carl Friedrich von Weizsäcker: *Zeit und Wissen*, Carl Hanser Verlag 1992
* Frank Böckelmann & Dietmar Kamper & Walter Seitter (eds): *Eugen Moritz Friedrich Rosenstock-Huessy (1888-1973)*, Wien: Turia & Kant 1995
* Otto Kroesen: *Tegenwoordigheid van Geest in het Tijdperk van de Techniek – een inleiding in de sociologie van Eugen Rosenstock-Huessy*, Meinema, Zoetermeer 1995
* Samuel Cramer-Naumann, *Weltgeschichte als Weltrevolution, Die Rolle der Heerschildrevolutionen nach Rosenstock-Huessy*, Verlag Dr. Kovac, 1996
* Willibald Huppuch: *Eugen Rosenstock-Huessy (1888-1973) und die Weimarer Republik Erwachsenenbildung, Industriereform und Arbeitslosenproblematik*, Hamburg 2004
* Fritz Vilmar: *Herrschaftskritik und solidarisches Leben. Beiträge zur Kritischen Friedensforschung*, herausgegeben vom Arbeitskreis für Friedenspolitik Berlin 2005
* Christoph Richter: *Im Kreuz der Wirklichkeit — Die Soziologie der Räume und Zeiten von Eugen Rosenstock-Huessy*, Frankfurt am Main: Peter Lang 2007
* Wayne Cristaudo: *Power, Love and Evil: Contribution to a Philosophy of the Damaged*, Radopi, 2007
* Wayne Cristaudo: *Messianism, Apocalypse And Redemption in 20th Century German Thought*, ATF Press, 2007
* David B. Goldman: *Globalisation and the Western Legal Tradition - Recurring Patterns of Law and Authority*, Cambridge, 2007
* Otto Kroesen: *Leven in Organisaties: Ethiek, Communicatie, Inspiratie*, Vught, Skandalon, 2008
* Clinton C. Gardner: *Beyond Belief - Discovering Christianity’s New Paradigm*, White River Press 2008
* Tom Holland: *Millennium: The End of the World and the Forging of Christendom*, Little, Brown 2008
* Alan Jacobs: *Original Sin: A Cultural History*, HarperOne, 2008
* Heinrich August Winkler: *Geschichte des Westens: Von den Anfängen in der Antike bis zum 20. Jahrhundert*, C.H.Beck 2009
* Wayne Cristaudo & Frances Huessy (eds): *The Cross and the Star: The Post-Nietschean Christian and Jewish Thought of Eugen Rosenstock-Huessy and Franz Rosenzweig*, Cambridge Scholars Publishing, 2009
* Svein Loeng: *Andragogy - A historical and professional review*, 2010
* Hartwig Wiedebach (Hg.): *"Kreuz der Wirklichkeit" und "Stern der Erlösung" - Die Glaubensmetaphysik von Eugen Rosenstock-Huessy und Franz Rosenzweig*, Freiburg (Alber) 2010
* Thomas Kreuder: *Der moderne Revolutionsbegriff und die Rolle der Sprache der Revolution von 1989*, (Studienarbeit), Grin 2010
* Eva-Maria Heinze: *Einführung in das dialogische Denken*, Freiburg (Alber) 2011
* Wayne Cristaudo: *Religion, Redemption and Revolution: The New Speech Thinking Revolution of Franz Rozenzweig and Eugen Rosenstock-Huessy*, Toronto 2012
