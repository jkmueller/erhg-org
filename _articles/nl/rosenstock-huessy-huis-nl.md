---
title: Het Rosenstock-Huessy Huis
category: receptie
language: nl
page-nr: 400
---
Het [Rosenstock-Huessy Huis](https://www.rosenstock-huessy-huis.nl/geschiedenis/) in Haarlem werd 1972 gesticht en was een leefgemeenschap tot in de jaren 1990.

Verscheidene berichten werden daarover gepubliceerd:
- in [Stimmstein 2](https://www.rosenstock-huessy.com/stimmstein/)
  - [Piet Blankevoort](https://www.rosenstock-huessy.com/pb-ins-rosenstock-huessy-huis/)
  - [Elly Hartman](https://www.rosenstock-huessy.com/eh-ins-rosenstock-huessy-huis/)
  - [Lien Leenman](https://www.rosenstock-huessy.com/ll-ins-rosenstock-huessy-huis/)
  - [Elias Voet](https://www.rosenstock-huessy.com/ev-ins-rosenstock-huessy-huis/)
- in Trouw, 1993:
  - [In het Rosenstock-Huessy-huis gaat het om mensenrechten, vrede en milieu](https://www.trouw.nl/nieuws/het-sint-jacobs-gasthuis-haarlem-in-het-rosenstock-huessy-huis-gaat-het-om-mensenrechten-vrede-en-milieu~b6853b41/)
- [Harrie Lieverse](https://nl.linkedin.com/in/duurzaambouwadvies): Een Opgetekend Leven, Gouda, 2021
