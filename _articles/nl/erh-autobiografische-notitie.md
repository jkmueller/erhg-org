---
title: "Eugen Rosenstock-Huessy: een autobiografische notitie (1938)"
category: biografie
language: nl
order: 3
page-nr: 110
---

![Eugen Rosenstock-Huessy, um1960]( {{ 'assets/images/rosenstock_150.png' | relative_url}}){:.img-left.img-small}

*"Mijn generatie heeft de maatschappelijke dood in al haar gedaanten overleefd en ik heb tientallen jaren van onderzoek en onderwijs overleefd in scholastieke en academische wetenschappen.  
Elk van hun eerbiedwaardige geleerden hield mij ten onrechte voor het intellectuele type waaraan hij de grootste hekel aan had.*\
*De atheïst wilde me in de theologie laten verdwijnen,\
de theologen in de sociologie,\
de sociologen in de geschiedenis,\
de historici in de jouranlistiek,\
de journalisten in de metafysica,\
de filosofen in het recht en - moet ik het nog zeggen? - \
de juristen in de hel,\
die ik als bewoner van de huidige wereld nooit had verlaten. Want niemand verlaat de hel op eigen kracht zonder gek te worden."*  

Eugen Rosenstock-Huessy: *De grote revoluties - autobiografie van de westerse mens*, Skandalon 2003 p.718-719 (Vertaling: Feico Houweling)
