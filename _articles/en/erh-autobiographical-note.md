---
title: "Eugen Rosenstock-Huessy: an autobiographical note"
category: lebensbild
language: en
order: 3
page-nr: 110
---

![Eugen Rosenstock-Huessy, um1960]( {{ 'assets/images/rosenstock_150.png' | relative_url}}){:.img-left.img-small}

*"My generation has survived social death in all its variations, and I have survived decades of study and teaching in scholastic and academic sciences.  
Everyone of their venerable scholars mistook me for the intellectual type which he most dispised*   
*The atheist wanted me to disappear into Divinity,\
the theologians into sociology,\
the sociologists into history,\
the historian into journalism,\
the journalists into metaphysics,\
the philosophers into law and - need I say it? -\
the lawyers into hell,
which as a member of the present world, I never had left. For nobody leaves hell all by himself without going mad."*

Eugen Rosenstock-Huessy: Out of Revolution - Autobiography of Western Man, 1938/1969 Berg Publishers, p.758
