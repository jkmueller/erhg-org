---
title: "Conference 2010 - Programme"
category: reception
language: en
page-nr: 421
---




### The Moral Equivalent of War - From William James to Camp William James and Beyond:
**Eugen Rosenstock-Huessy and the Social Representation of Truth**  
Conference at Dartmouth College - November, 12-13 2010

A conference at Dartmouth College in Hanover, New Hampshire

Friday, November 12– Saturday, November 13, 2010

All sessions of the conference will take place in Room 041, at the Haldeman Center, 27 N. Main Street, Hanover, N. H.

Sponsored by the Eugen Rosenstock-Huessy Society and the Eugen Rosenstock-Huessy Fund, in cooperation with the William Jewett Tucker Foundation and the Institute for the Study of Applied and Professional Ethics at Dartmouth College. The gathering has been made possible, as well, by generous donations from Paul Lee, Theodore Weymouth, and others.

#### Introduction

This conference commemorates the publication a century ago of William James’s famous essay, “The Moral Equivalent of War.” It refers as well to Rosenstock-Huessy’s evocation of the central task of the third millennium, the “social representation of truth.” In Chapter 18 of Out of Revolution: Autobiography of Western Man (1938), entitled “Farewell to Descartes,” Rosenstock-Huessy wrote:
* *The “Cogito ergo sum,” for its rivalry with theology, was one-sided. We post-War thinkers are less concerned with the revealed character of the true God or the true character of nature than with the survival of a truly human society. In asking for a truly human society we put the question of truth once more; but our specific endeavour is the living realization of truth in mankind. Truth is divine and has been divinely revealed––credo ut intelligam. Truth is pure and can be scientifically represented––cogito ergo sum. Truth is vital, and must be socially represented––Respondeo etsi mutabor.* (pp. 740-741).

So much did Prof. Rosenstock-Huessy find of practical and prophetic value in James’s essay that the work camp he and a group of young men formed in Vermont in 1940 was named Camp William James. Some of the participants were fresh from Ivy League colleges, and some, who were assigned by the Civilian Conservation Corps, could not even dream of a college education. One purpose of the camp was to train leaders for a new, permanent national service corps that would go far beyond merely emergency relief for unemployed, disadvantaged youth.

Camp William James, which survived barely two years, was as much ideal as reality. It was concerned not only with the elimination of war as a horrendous human institution, but with soil conservation and forestry, community revitali-zation, land re-settlement, leadership, the integration of antithetical social and economic classes, universal education, the treatment of unemployment, and more. Many of the ideals of the Camp are realized today, seventy years later, or partially realized, yet the Camp retains a fascination because of what was aspired to and not accomplished.

Rosenstock-Huessy was a profound and original thinker, with few equals in the twentieth century. Always the learned philosophical historian and the passionate Christian prophet, he saw deeply into the nature of man and human society. He spent most of his life in academe, but so ardent and unconventional was his desire for reform, indeed for the entire re-structuring, of Western thought and for its re-planting on new foundations, that he remains unclassifiable in the context of present-day higher education. Yet one theme he held to, from an epiphany in 1918 until his death in 1973, namely that although mere change in human affairs is incessant and inescapable, a better future for humankind, so-called “progress,” is far from automatic. The fulfillment of human dreams requires always the engaged investment of individual and communal energies, by both men and women, to create the future.

We struggle to know what must be done. When the need is apparent to us, it is then that we must respond although it may mean radical change in ourselves: Respondeo etsi mutabor, I respond even though I must change. It is a call for action, but not necessarily programmatic action, and has less to do with idiosyncratic personal impulse than with the role of spirit in human history, with “spirit” referring to nothing at all transcendental, magical, or mystical, but simply to the spoken word and the listener.

#### Programme

FRIDAY, November 12, 2010

| 2:00 – 3:15 p.m. | Opening remarks (15 mins.) |
|                  | I. Rosenstock-Huessy and William James |
|                  |	Moderator:  *Norman Fiering* |
|                  | 1) *Paul Lee, “A Moral Equivalent of War: The Theme for a Century”* (20 mins.) |
|                  | 2) *Justin Reynolds*, Columbia University, *“Rosenstock-Huessy and William James”* (20 mins.) |
|                  | Floor discussion (20 mins.) |
|                  | Break (15 mins.)
| 3:30 – 4:30 p.m. | II. The Mission in the Third Millennium |
|                  | Moderator: *Raymond Huessy* |
|                  | 3) *Michael McDuffee*, Moody Bible Institute, *“A Re-reading of Rosenstock-Huessy’s Christian Philosophy of History”* (20 mins.)|
|                  | 4) *Clinton C. Gardner*, *“Camp William James and Rosenstock-Huessy’s Vision of Christianity in the Third Millennium”* (20 mins.) |
|                  | Floor discussion ( 20 mins.) |
| 4:30 – 5:15 p.m. | III. Featured Address |
|                  | Moderator: Paul Lee |
|                  | *Harris Wofford* (President of Bryn Mawr College, 1970-1978; Senator from Pennsylvania, 1991-1995; CEO of the Corporation for National and Community Service, 1995-2001) *“Cracking the Atom of Civic Power”* |
| 6:30 p.m.        | Cash bar/ Cocktail reception (Norwich Inn.) |
| 7:00 p.m.        | Dinner (Norwich Inn). ($35 payable in advance for a reservation.)

SATURDAY, November 13, 2010

| 9:00 - 10:00 a.m. | IV. The Social Representation of Truth- (part 1) |
|                  | Moderator: M. Darrol Bryant, Waterloo University |
|                  | 6) *Svein Loeng*, Hoegskolen i Nord-Troendelag, Norway, *“Rosenstock-Huessy and Andragogy”* (20 mins.) |
|                  | 7) *William Young*, Endicott College, *“Rosenstock-Huessy, Rosenzweig, and the Work of Education.”* (20 mins.) |
|                  | Floor discussion (20 mins.) |
|                  | Break (15 mins.) |
| 10:15 – 11:15 a.m. | V. The Social Representation of Truth- (part 2) |
|                  | Moderator: Clinton Gardner |
|                  | 8) *Andreas Leutzsch*, Bielefeld University, *“Planetary Service”* (20 mins.) |
|                  | 9) *Feico Houweling* [Netherlands], *“Rosenstock-Huessy House in Haarlem, The Netherlands”* (20 mins.) |
|                  | Floor discussion (20 mins.) |
| 11:15 - 11:45 a.m. | Interventions (30 mins.) |
| 12:00 – 1:30 p.m. | Buffet Lunch for those registered (Russo Gallery, Ground floor, Haldeman) |
| 1:30 – 2:30 p.m. | VI. Rosenstock-Huessy and the Call of the Spirit |
|                  | Moderator: *Feico Houweling* |
|                  | 10) *J. Otto Kroesen*, Technical Univ. of Delft, *“Planetary Internships and Cultural Diversity”* (20 mins.) |
|                  | 11) *Norman Fiering* , *“ERH on the Structure of Significant Lives”* (20 mins.) |
|                  | Floor discussion (20 mins. ) |
| 2:30 – 3:30 p.m. | VII. “The Traveling Guru.” |
|                  | A documentary film featuring Prof. M. Darrol Bryant, University of Waterloo. |
|                  | Comments by Professor Bryant |
|                  | Break (15 mins.) |
| 3:45 - 5:00 p.m. | VIII. Voluntary Work Service and the Campus Mood Today |
|                  |	Moderator:  (t. b. a.) |
|                  | 12) Slide presentation illustrating Camp William James, 1940-1941, with remarks by *Clinton Gardner*, a participant in the Camp (15 mins.) |
|                  | 13) Panel of Dartmouth College students who are engaged in voluntary service (45 mins.) |
|                  | Floor discussion  (15 mins.) |
| 5:00 – 5:30 p. m.| Recapitulations

Also scheduled:

Friday, November 12, 2010  
12:00-1:30 - Business meeting of the ERH Society. Members only. Light lunch will be served. (Haldeman, Room 125).

Sunday, November 14, 2010  
9:00-12:00 – Bus or van will depart at 9:00 a.m. from the Norwich Inn for a visit to Tunbridge, Vermont, one of the sites of Camp William James.. Courtesy of Mr. and Mrs. William Chester.

#### TRAVEL INFORMATION

Lodging: A limited number of rooms have been reserved for the conference at the two hotels listed below. Note: These rooms will not be held after October 1. When calling to make a reservation, specify that you are attending the “Rosenstock conference.”

* Hotel Coolidge, 39 South Main St., White River Junction, Vermont. 800-622-1124 or 802-295-3118. Info@hotelcoolidge.com. Hotel Coolidge is about 15 minutes distant from Hanover via private auto or bus. Rates start at $89.00.
* Norwich Inn, 325 Main St., Norwich, Vermont. 802-649-1143. innkeeper@norwichinn.com. Norwich Inn is less than 10 minutes distant from Hanover. Rates start at $135. There will be a shuttle van to the conference from the Norwich Inn.

Travel (to Hanover, New Hampshire, and the nearby towns in Vermont):
* By Air  
Lebanon Airport: (Lebanon, N.H.) is about 10 miles from campus. Flights are available to and from LaGuardia Airport in New York City. Rental cars and taxis are available at the airport. Manchester Airport: The Manchester Airport (Manchester, N.H.) is served by the major airlines. It is located about 80 miles southeast of Hanover. Rental cars are available at the airport, and the drive to Hanover takes approximately 90 minutes. The Vermont Transit bus company provides transportation from the Manchester Airport to Hanover. Buses wait outside the airline terminal to transport you to the Vermont Transit station, where you purchase a ticket and then continue to Hanover. Logan Airport, Boston: Logan Airport (Boston, Mass.) is about 120 miles southeast of Hanover. Rental cars are available at the airport, and the drive to Hanover takes approximately 2 1/2 hours. Dartmouth Coach, a local bus service, provides transportation from Logan Airport and from Boston's South Station to The Hanover Inn next to the Dartmouth campus.
* By Bus  
From Boston: Service to the bus station in White River Junction, Vermont, is provided by Vermont Transit. From New York City: Service to the bus station in White River Junction, Vt., is provided by Greyhound (direct bus becomes Vermont Transit in Springfield, Mass.). The White River Junction bus station is about seven miles from the Dartmouth campus, and taxi service is available.
* By Train  
Amtrak provides daily passenger service from Washington, D.C.; Philadelphia; New York; and intermediate points to White River Junction, Vt. Information is available on the Amtrak website or by calling 800-872-7245 from within the U.S. The White River Junction train station is about ten miles from the Dartmouth campus, and taxi service is available. Parking
The little college town of Hanover is congested with autos, and parking is always hard to find. See the enclosed sheet with instructions, plus a map.

For additional information, please be in touch with Norman Fiering, 401-487-8008, or norman_fiering'at'brown.edu.

#### ERH Fund

The Eugen Rosenstock-Huessy Society is affiliated with the Eugen Rosenstock-Huessy Fund, based in Essex, VT (http://www.erhfund.org/). The Fund has kept in print, or brought into print, many works by and relating to Prof. Rosenstock-Huessy. For a full list of available works, including recordings and transcripts of 400 hours of Professor Huessy’s lectures at Dartmouth, visit the Argo website.

#### Book Recommendations

* Those wishing to familiarize themselves with the history of Camp William James should read Jack J. Preiss’s *Camp William James* (Argo, 1978).
* Also closely relevant to the conference is Rosenstock-Huessy’s *Planetary Service* (Argo, 1978).
* For an introduction to a major segment of Rosenstock-Huessy’s historical thought, there is nothing to equal *Out of Revolution: Autobiography of Western Man* (orig. publ., 1938; available from Argo in a modern paperback).  

Rosenstock-Huessy is often described as a Christian thinker, a description that he might not dispute, but his thinking about religion is often unconventional and surprising.
* See in this regard, *The Christian Future, or The Modern Mind Outrun* (orig. publ. by Scribner, 1946; available from Argo in a modern paperback).
* *The Origin of Speech* (Argo, 1981) is another masterly work, of only 100 pages, that leads deeply into the core of his thinking, as does *The Multiformity of Man* (Argo, 1973), dating from lectures originally delivered in 1935.

Although Out of Revolution is some 800 pages in length, and Rosenstock-Huessy’s Soziologie, not yet available in an English translation, is even longer, he wrote dozens of essays that convey the essence of different sides of his thought. Argo has published two notable collections of Rosenstock’s essays,
* *I Am an Impure Thinker* (1969) and
* *Speech and Reality* (1970).

The entire corpus of Rosenstock-Huessy’s writing, from 1903 to the 1990s, including many posthumous publications (Professor Huessy died in 1973), is recorded in Lise van der Molen’s *A Guide to the Works of Eugen Rosenstock-Huessy* (Argo, 1997).
With this book, one may also acquire from Argo a DVD, *The Collected Works of Eugen Rosenstock-Huessy*, which includes virtually everything he wrote and is searchable. Van der Molen’s bibliography serves as a table of contents to this digital compilation.

A relatively complete list of what has been published about the thought of ERH since his death may be found in [Recent Publications Relating to the Work of Eugen Rosenstock-Huessy, 1973 to the Present](http://www.erhsociety.org/wp-content/uploads/1.RECENTpubls.relatingtoERH-10.15.13_combined.pdf), which is regularly updated and is on file at the [ERH Society website](http://www.erhsociety.org/).

Two good shorter introductions online are:
* Wayne Cristaudo’s [essay](https://plato.stanford.edu/entries/rosenstock-huessy/) for the Stanford Encyclopedia of Philosophy, which is somewhat technical, and
* Peter Leithart’s June 28, 2007, essay in the online version of the journal First Things, [The Relevance of Eugen Rosenstock-Huessy](https://www.firstthings.com/web-exclusives/2007/06/the-relevance-of-eugen-rosenst), which is more accessible to the lay reader.

George Morgan’s *Speech and Society: The Christian Linguistic Social Philosophy of Eugen Rosenstock-Huessy* (orig. publ. 1987 by the Univ. of Florida Presses, but now available from Argo) is excellent but more descriptive than interpretive.

See also the several published collections of essays about the work of ERH derived from conferences, all listed in “Recent Publications” cited above, such as *Eugen Rosenstock-Huessy: Studies in His Life and Thought* (Lewiston/Queenston: Edwin Melln Press, 1986), ed. M. Darrol Bryant and Hans R., Huessy.
