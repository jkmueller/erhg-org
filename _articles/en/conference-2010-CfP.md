---
title: "Conference 2010 - CfP"
category: reception
language: en
page-nr: 420
---

### The Moral Equivalent of War - From William James to Camp William James and Beyond:
**Eugen Rosenstock-Huessy and the Social Representation of Truth**  
Conference at Dartmouth College - November, 12-13 2010

A Call for Papers

On the occasion of the 100th anniversary of the publication in 1910 of William James’s famous essay, “The Moral Equivalent of War,” the Eugen Rosenstock-Huessy Fund and the Eugen Rosenstock-Huessy Society, in cooperation with the William Jewett Tucker Foundation at Dartmouth College, are proposing a two-day conference to be held in November 2010 at Dartmouth on the theme of Rosenstock-Huessy’s evocation of the central task of the third millennium, the “social representation of truth.”

In Chapter 18 of Out of Revolution: Autobiography of Western Man (1938), entitled “Farewell to Descartes,” Rosenstock-Huessy wrote:
The “Cogito ergo sum,” for its rivalry with theology, was one-sided. We post-War thinkers are less concerned with the revealed character of the true God or the true character of nature than with the survival of a truly human society. In asking for a truly human society we put the question of truth once more; but our specific endeavour is the living realization of truth in mankind. Truth is divine and has been divinely revealed––credo ut intelligam. Truth is pure and can be scientifically represented––cogito ergo sum. Truth is vital, and must be socially represented––Respondeo etsi mutabor. (pp.740-741).

So much did Prof. Rosenstock-Huessy find of practical and prophetic value in James’s essay that the work camp he and a group of young men formed in Vermont in 1940 was named Camp William James. Some of the participants were fresh from Ivy League colleges, and some, who were assigned by the Civilian Conservation Corps, could not even dream of a college education. One purpose of the camp was to train leaders for a new, permanent national service corps that would go far beyond merely emergency relief for unemployed, disadvantaged youth. For Rosenstock-Huessy, the Camp was a renewal of his investment in the concept and practice of work service in Germany in the 1920s.

Camp William James, which survived barely two years, was as much ideal as reality. It was concerned not only with the elimination of war as a horrendous human institution, but with soil conservation and forestry, community revital-ization, land re-settlement, leadership, the integration of antithetical social and economic classes, universal education, the treatment of unemployment, and more. Many of the ideals of the Camp are realized today, seventy years later, or partially realized, yet the Camp retains a fascination because of what was aspired to and not accomplished.

Rosenstock-Huessy was a profound and original thinker, with few equals in the twentieth century. Always the learned philosophical historian and the passionate Christian prophet, he saw deeply into the nature of man and human society. He spent most of his life in academe, but so ardent and unconventional was his desire for reform, indeed for the entire re-structuring of Western thought and for its re-planting on new foundations, that he remains unclassifiable in the context of present-day higher education.

Yet one theme he held to, from an epiphany in 1918 until his death in 1973, namely that although change in human affairs is inescapable, a better future for humankind, so-called “progress,” is far from automatic. The fulfillment of human dreams requires always the engaged investment of individual and communal energies, by both men and women, to create the future.

We struggle to know what must be done. When the need is apparent to us, it is then that we must respond although it may mean radical change in ourselves: Respondeo etsi mutabor, I respond even though I must change. It is a call for action, but entirely unprogrammatic action, having to do with the role of the spirit in human history and nothing to do with idiosyncratic, personal impulse than with the role of spirit in human history, with "spirit" referring to nothing at all transcendental, magical, or mystical, but simply to the spoken word and the listener.

Proposals for papers will be welcomed on diverse themes that relate substantially to Rosenstock-Huessy’s life and work, including his influence in Europe as well as the United States. Some suggested topics follow:
* Rosenstock-Huessy on the nature of war and peace;
* the implications of the “Moral Equivalent of War”;
* work service in Germany during the Weimar period;
* Rosenstock-Huessy’s practical influence in the Netherlands;
* addenda to Jack Preiss’s, Camp William James (1978);
* religion and work service;
* Christianity in the third millennium;
* Rosenstock-Huessy’s Multiformity of Man: Ecodynamics of a Mechanized World (1936);
* Rosenstock-Huessy’s Dienst auf dem Planeten: Kurzweil und Langeweile im dritten Jahrtausend (Stuttgart, 1965)/ Planetary Service (1978);
* Rosenstock-Huessy’s contributions to Adult Education;  

and so forth.

For sources, see especially Lise van der Molen, *A Guide to the Works of Eugen Rosenstock-Huessy: Chronological Bibliography* (1997), and the file entitled [Recent Publications Relating to the Work of ERH](http://www.erhsociety.org/wp-content/uploads/1.RECENTpubls.relatingtoERH-10.15.13_combined.pdf) (see also List of Publications )

Proposals should be no more than three pages in length and sent electronically to: Norman Fiering's email, or by post to Norman Fiering, Sec./Treas., ERH Society, P. O. Box 603233, Providence, RI 02906 (401-487-8008). The deadline for proposals is November 1, 2009.
