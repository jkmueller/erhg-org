---
title: "Norman Fiering: Anthropurgy"
category: reception
language: en
page-nr: 313
---
Ray Huessy and others are hurriedly completing the processing and scanning of the papers of Eugen Rosenstock-Huessy and his wife, Margrit, which will all be turned over to the Dartmouth College Library before the end of this calendar year. More news about that transaction will be coming soon.

The other day, Ray called attention to the following note by ERH found amidst the Papers:

"In 1903, President William DeWitt Hyde of Bowdoin College wrote a hymn which has been curiously overlooked by most hymnbook editors.

*Creation's Lord, we give thee thanks  
That this thy world is incomplete,  
That battle calls our marshalled ranks  
That work awaits our hands and feet,*  

*That thou hast not yet finished man,  
That we are in the making still -  
As friends who share the Maker's plan,  
As sons who know the Father's will."*

Then John Baldwin, in response, provided me with the address of a [website](http://www.hymntime.com/tch/htm/c/r/e/a/creation.htm) that contains all five verses of the hymn, plus the music that Hyde found to set it to.

There is also a link that tells a little about Hyde (1858-1917):

* B.A. Harvard, 1879;
* Union Theological, 1879-1880;
* Andover Theological (1882);
* ordained Congregational minister, 1883;
* pastored in Patterson, N. J. (1883-85);
* then called to the presidency of Bowdoin College where he served until his death in 1917.

Rosenstock was undoubtedly delighted to come upon that little-known hymn because he referred often to Man's unfinished state, strikingly and unforgettably on p. 727 of Out of Revolution (1938), as follows:  

* *"In the Bible there are two names for God: one is grammatically a plural, Elohim; the other is the singular Jahve. The Elohim are the divine powers in creation; Jahve is he who will be what he will be. When man sees through the works of Elohim and discovers Jahve at work, he himself begins to separate past from future. And only he who distinguishes between past and future is a grown person; if most people are not persons, it is because they serve one of the many Elohim. This is a second-rate performance; it deprives man of his birthright as one of the immediate sons of God."*

* *"In the Sistine Chapel of the Vatican, Michelangelo shows God creating Adam, and keeping in the folds of his immense robe a score of angels or spirits. Thus at the beginning of the world all the divine powers were on God's side; man was stark naked. We might conceive of a pendant to this picture; the end of creation, in which all the spirits that had accompanied the Creator should have left him and descended to man, helping, strengthening, enlarging his being into the divine. In this picture God would be alone, while Adam would have all the Elohim around him as his companions."*

* *"Regularly the news from natural science and technology shows that humankind already has powers that formerly were only in the possession of divinities. The fact is terrifying since we hardly seem capable of handling such responsibilities, and they are magnifying. And yet there is no turning back. As William Hyde said in the continuing verses, we have a big "field for toil and faith and hope."*

In The Christian Future (1946), ERH writes:

*  *"The Church Fathers interpreted human history as a process of making Man like God. They called it 'anthropurgy': as metallurgy refines metal from its ore, anthropurgy wins the true stuff of Man out of his coarse physical substance."* (p. 108)

ERH's concept of *"anthropurgy"* is treated in Peter Leithart's June 28, 2007 essay in the online version of the journal First Things, [*"The Relevance of Eugen Rosenstock-Huessy"*]()
and also in Wayne Cristaudo's essay on [ERH](https://plato.stanford.edu/entries/rosenstock-huessy/) in the Stanford Encyclopedia of Philosophy.

Note:

* *"Making Man" does not mean that we as individuals will become significantly better than we are, now or 500 years from now. There is no parallel here to the New Soviet Man or other utopian dreams of perfecting the individual. The isolated human being is fatefully flawed. Humankind, however, "Man" capitalized, has and will progress. "Good," Rosenstock wrote "is not in any man." It exists only by "propagation," it originates only "between teacher and student, between father and son. . . ."*

* *"Exactly as children are begotten, so the gifts of the spirit, the fertility of goodness, the contagion of enthusiasm, the fecundity of thought, the influence of authority, are interhuman processes which spring to life only between people. No man is good. But the word or act that links men may be good. And by link work evil has to be constantly combatted." ("Soul of William James,"* in I Am an Impure Thinker (Argo, 1970), pp. 26-27.

### Complete Lyrics

Creation's Lord, we give Thee thanks\
That this Thy world is incomplete;\
That battle calls our marshaled ranks;\
That work awaits our hands and feet.

That Thou hast not yet finished man;\
That we are in the making still,\
As friends who share the Maker's plan\
As sons who know the Father's will.

Beyond the present sin and shame,\
Wrong's bitter, cruel, scorching blight,\
We see the beckoning vision flame,\
The blessèd kingdom of the right.

What though the kingdom long delay,\
And still with haughty foes must cope?\
It gives us that for which to pray,\
A field for toil and faith and hope.

Since what we choose is what we are,\
And what we love we yet shall be,\
The goal may ever shine afar—\
The will to win it makes us free.
