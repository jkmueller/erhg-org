---
title: "Otto Kroesen: Eugen Rosenstock-Huessy and his Thinking"
category: lebensbild
order: 10
language: en
page-nr: 130
---
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}


Born in 1888 Rosenstock-Huessy belongs to the generation for which the atrocities of the first World War became the most intense experience of their life. He did not only interpret this war as a crisis of the European nation states, but even more as a crisis of the objectifying scientific way of thinking. The sciences  not only had made nature to be a mere object, but  even man. The way in which men on the battlefield were treated as material and mere object is equivalent to the way science in its analyses is treating human beings. It is one and  the same disfigurement. The life work of Rosenstock-Huessy does not consist in the change of  some method in scientific discourse, but in a change of paradigm.  The whole cadre of reference of the sciences, their universal horizon, is at stake. The gap between subject and object is bridged in perceiving  human being  as living between the achievements of the past and the challenges of the future. In this tension between past and future  Rosenstock-Huessy himself has lived. In 1933 he emigrated to the United States, entering upon a new future. There he wrote his great sociological works, which offer to the present generation an interpretation of the past in view of a future history of mankind. In 1973 Eugen Rosenstock-Huessy passed away in his home “Four Wells” in Norwich Vt.  USA.

## Language

The central theme of Rosenstock-Huessy is language. Language is not  in the first place a means of communication,  language presides over communication. ‘Language is wiser than the one who is speaking it’. We live  by articulating,  modulating  and interpreting the sediments of language which come to us from the past.  And language renews itself in our mouth, when we answer to the challenges which  in our own time are posed to us. Only when the right word for our present experiences is found, a new future opens  up, a way to go  forward. To be without speech is to have no future :  When we haven't found yet the liberating word , the right name for our situation, we are caught in a deadlock  and move in  vicious circles.

## History

There are as many historical periods  as there are modes of speech and legal relationships. They are arches of time, in which a language spreads itself over a certain group of people. From the middle ages on for instance an arch of time is constituted. In this epoch  the nation states developed  from the great upheavals which made for the change in search of universal principles of law and universal scientific methods. In the two world wars this language found its final limit.

## Sociology

In his sociological work Rosenstock-Huessy follows the different arches of time and social orders of history. He describes the history as a dialogue between these social orders. Time and again the new form of society  inherits the former one by renewing it. When the tribal society ran into a deadlock, the Empires of Egypt and Babel (compare China and Mexico) have made possible a new  hierarchical and more universal social order. When the hierarchical social order of Egypt suffocated life and justice, the Hebrews left Egypt and established for the first time in history a future oriented social order. The church has inherited this orientation to the future and bridges the gap towards the former social orders from the future as its departing point.

## The third millennium

Rosenstock-Huessy considered it to be the task of the third millennium to   confront  the fragmentation and atomization and the accompanying confusion of all inherited  forms of language of our time overcoming them  by living speech. Whereas mutual understanding among people is no longer self evident, speech  has to become the opposite:  a consciously made effort. The language of the third millennium will be a language of transient but intensively  living communities. Rosenstock-Huessy interpreted this development as a purified revival  of the tribes: one humankind in polyglot tongues, just like in the first millennium the one God of Israel came to the whole world by means of the church and just like in the second millennium the old Empires were revived and modified into one global interrelated world by the natural sciences.

Otto Kroesen, in 2003
