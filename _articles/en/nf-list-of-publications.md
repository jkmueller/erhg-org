---
title: A List of Publications collected by Norman Fiering
category: reception
language: en
page-nr: 390
---
### Books

- Raley, Harold C., José Ortega y Gasset: Philosopher of European Unity (University, Alabama: University of Alabama Press, 1971).
    Ortega and R-H have much in common, particularly in their understanding of history. Raley cites R-H in a number of footnotes and is full of praise (cf. p. 123n): “In language as powerful as Ortega’s and with an understanding at least as deep, [R-H] says of Rationalism: ‘The abstractions that prevailed in philosophy from Descartes to Spencer, and in politics from Machiavelli to Lenin, made caricatures of living men. . . . etc.’” quoting from Out of Revolution. The European Union is in a phase now of determining essentially what “Europe” means, which inescapably calls attention to “its” history. Out of Revolution should find new readers because of this quest. Raley says of Out, “An extraordinary, indeed a stupendous, achievement in historiography, with fresh original insights on virtually every one of its 800 pages.”
- Ritzkowsky, Ingrid, Rosenstock-Huessys Konzeption einer Grammatik der Gesellschaft (Berlin, 1973)
- Rohrbach, Wilfred, Das Sprachdenken Eugen Rosenstock-Huessys; historische Erörterung und systematische Explikation (Stuttgart: W. Kohlhammer, 1973).
    A revision of the author’s thesis at the University of Saarlandes.
- Hasselaar, J. M., Inleiding tot het denken van E. Rosenstock-Huessy (Baarn: Ten Have, 1973; reprinted 1974).
- Riebensahm, Paul, Daimler Werkzeitung 1919-1920 (Moers: Brendow, 1974).
	Riebensahm was co-editor with R-H of the Daimler Corp. newspaper.

- Schmid, Manfred, Eugen Rosenstock-Huessys Herausforderung der Philosophie Grammatik statt Ontologie (Wien, 1976)
- Preiss, Jack J., Camp William James (Essex, VT: Argo Books, 1978).
- Martin E. Marty, By Way of Response (Nashville: Abingdon Press, 1981).
    A short autobiographical memoir by a distinguished historian of religion. . Marty is quite aware of the range of Rosenstock’s work and is especially appreciative of “A Farewell to Descartes,” where R-H enunciates his counter-Cartesian motto, “Respondeo etsi mutabor.”
- Gardner, Clinton C., Letters to the Third Millennium (Norwich, VT: Argo Books, 1981).
    An attempt to introduce the general public informally to Rosenstock’s work. The book is written as a journal of the 1970s.
- Evangelische Akademie (Berlin), Unser Zeitpunkt: nach Darwin, Marx, Freud, Nietzsche: Tagung von 26-28 September 1980 mit d. Eugen Rosenstock-Huessy (Berlin: Evang. Bildungswerk, 1981).
- Albrecht, Renate, and René Tautmann, eds., Paul Tillich, Briefwechsel und Streitschriften: theologische, philosophische und politische Stellungnahmen und Gespräche (Frankfurt: Evangelisches Vergswerk, 1983).
    Includes correspondence between R-H and Tillich.
- Bryant, M. Darrol, and Hans R. Huessy, eds., Eugen Rosenstock-Huessy: Studies in His Life and Thought (Lewiston, NY/Queenston, Ontario: Edwin Mellen Press, 1986).
    The book is vol. 28 in the series, Toronto Studies in Theology.
    Seventeen essays derived from a conference on R-H at Renison College at the University of Waterloo, in Waterloo, Ontario, in 1982. \
    Contents:
    -   Harold J. Berman, “Renewal and Continuity: The Great Revolutions and the Western Tradition”;
    - Harold M. Stahmer, “Christianity in the Early Writings of E. R.-H.”;
    - Raymond Huessy, “Joseph Wittig’s Life in Silesia, Vermont & Elsewhere”;
    - Clinton C. Gardner, “From Theology to a Higher Sociology: The Promise of E. R.-H.”
    - Stanley Johannesen, “The Problem of American History in Out of Revolution”
    - Eugene D. Tate, “E. R.-H.: Revolutionizing Communication Theory”;
    - W. Thomas Duncanson, “Mercenary or Pirate: Life in a Rhetorical Culture”;
    - Patricia A. North, “Labor and the Spirit”;
    - Terry Simmons, ”The Bridge Builder in Quest of Community”;
    - Hans R. Huessy, “Contributions to Psychiatry from the Writings of Eugen Rosenstock-Huessy”;
    - Cynthia Oudejans Harris, “E. R.-H. and Fritz Perls: A Study in Complementarity”;
    - Richard Feringer, “The Grammatical Method in the Light of Research in the Psychology of Learning: A Posthumous Letter”;
    - Richard Shaull, “E. R.-H.: My Guide on a Lonely Journey”;
    - Dale Irvin, “Mission as Dialogue”;
    - W. C. Strickland, “To Hear Again the One Voice of the Gospel”;
    - M. Darrol Bryant, “The Grammar of the Spirit: Time, Speech and Society”;
    - Bas Leenman, “The Ever Growing Word.”
- Theunissen, Michael, The Other: Studies in the Social Ontology of Husserl, Heidegger, Sarte, and Buber, trans. Christopher Macann (Cambridge, Mass.: MIT Press, 1986).
- Morgan, George Allen, Speech and Society: The Christian Linguistic Social Philosophy of Eugen Rosenstock-Huessy (Gainesville: University of Florida Press, 1987).
    This is an excellent work by a friend who was closely associated with R-H for many years and, indeed, helped him to bring to press The Christian Future (1946). Morgan attempted to encompass and epitomize the whole range of R-H’s writing––theology, language, society, family, philosophy, history, prophecy, and so on––and did so accurately, but at the cost of extended analysis of any one part of it. He more or less abstains from reconciling or resolving numerous contradictions, or apparent contradictions, in the writings of a man of extraordinary fecundity.
- Schmied-Kowarzik, Wolfdietrich, ed. Der Philosoph Franz Rosenzweig (1886-1929). Internationaler Kongreß Kassel 1986. (Karl Alber, Freiburg / München 1988).
    - Band I: Die Herausforderung jüdischen Lernens.
    - Band II: Das neue Denken und seine Dimensionen. Various essays herein relate to R-H.
- Bossle, Lothar, ed., Eugen Rosenstock-Huessy, Denker und Gestalter (Würzbur: Creator-Verlag, 1989).
    Papers presented at a symposium held in Würzburg in 1988 on the occasion of the 100th anniversary of R-H’s birth.
    Lévinas, Emmanuel, Joodse filosofie? Rosenstock, Levinas, Soloveitchik, Fackenheim (Amsterdam, 1989).
    [More information is needed about this book; the citation may be faulty.]
- Leenman, Bas, Lise van der Molen, & Eckart Wilkens, Eugen Rosenstock-Huessy – Zum hundertsten Geburtstag (Mössingen-Talheim: Talheimer Verlag, 1990). ISBN 3-89376-010-5
- Smith, Page, Killing the Spirit: Higher Education in America (New York: Penguin Books, 1990)
    Smith (d. 1995) was a student of R-H’s in the 1930s and was among the group that founded Camp William James. He became a historian, essayist, and social commentator, with a long career at the University of California, first at UCLA and then at the Santa Cruz campus, where he was the first provost of Cowell College. He published many books on diverse historical subjects, and every one is imbued with the singular spirit of Rosenstock-Huessy, sometimes implicitly, but often outrightly. Needless to say, R-H was a profound and vigorous critic of higher education as it is presently instituted, a point of view well reflected here.
- Beyfuss, Viktor, Die soziologische Interpretation der europäischen Revolutionen im Werk Eugen Rosenstock-Huessys (München: Kyrill & Method, 1991).
    Originally a dissertation, 1990, Universität Würzburg.
- Bergman, Hugo, Dialogical Philosophy from Kierkegaard to Buber, trans. Arnold Gerstein (New York: SUNY Press, 1991).
- Gardner, Clinton C., Mezhdu Vostokom i Zapadom: Vozrozhdenie darov russkoi dushi. (Moscow: Nauka, 1993).
    Russian translation of Between East and West: Rediscovering the Gifts of the Russian Spirit. This book compares the thought of several Russian thinkers, including Nikolai Berdyaev and Mikhail Bakhtin, with that of R-H, , Martin Buber, and Franz Rosenzweig. (Norwich Center Books, P. O. Box 710, Norwich, VT 05055.)
    Vos, Ko, Eugen Rosenstock-Huessy: een kleine biografie (Aalsmeer: DABAR/Luyten, 1993; reprinted Aachen: Shaker, 1997).
- Böckelmann, Frank, Dietmar Kamper, and Walter Seitter, Eugen Moritz Friedrich Rosenstock-Huessy (1888-1973), Wien: Turia & Kant, 1995.
- Kroesen, Otto, Tegenwoordigheid van Geest in het Tijdperk van de Techniek – een inleiding in de sociologie van Eugen Rosenstock-Huessy. ( Zoetermeer: Meinema, 1995). 228 pp.
- Ward, Graham, Barth, Derrida, and the Language of Theology (Cambridge Univ. Press, 1995).
    A reviewer on Amazon, P. Soen, writes: “After von Humboldt, we are then given a brief sketch of the Patmos group, which was comprised most importantly of Huessy, Rosenzweig, Buber, and for a short time Karl Barth. This is again where Ward shines most brightly. I have been reading Huessy for about five years now and Karl Barth for about three, and biographers of both of these men have alluded to brief encounters and connections, but none have explored or explicated those connections as clearly and scholarly as Ward.” A highly sophisticated and informative book.
- Van der Molen, Lise, comp., A Guide to the Works of Eugen Rosenstock-Huessy. Chronlogical Bibliography with a Key to the Collected Works on Microfilm. (Essex, VT: Argo Books, 1997).
    This book must be the bible of anyone hoping to study Rosenstock-Huessy.
- Fuller, John, Auden: A Commentary (Princeton, 1998).
    There are many references to R-H in this book.
- Manz, Werner Justus, Arbeit und Persönlichkeit: betriebliche Erwachsenenbildung als wesentlicher Aspekt der Betriebspolitik, in Sinne von Eugen Rosenstock-Huessy (München; Mering: Hampp, 1998).
    The work was originally a doctoral dissertation at Oldenburg University, 1997.
- Bryant, M. Darrol, Woven on the Loom of Time: Many Faiths and One Divine Purpose (New Delhi: Suryoday/Decent Books, 1999)
    Two chapters in this work relate particularly to R-H, “Dialogical Humanity: In the Crucible of Transcendence,” pp. 29-55, and “A Grammar of the Spirit: Making Humanity in Persons, Society, and History,” pp. 93-123.
- Mendelson, Edward, Later Auden (New York: Farrar, Straus and Giroux, 1999).
    This is the second volume of Mendelson’s two-volume biography of the great poet. Auden read Out of Revolution in 1946, and thereafter much of his poetry is suffused with Rosenstock’s ideas, which Mendelson traces to some degree.
- Don, Arie, Li-kerat dialog Yehudi-Notrri: `iyunim be-hagutam shel Barukh Shpinozah, Mosheh Mendelson, Frants Rozentsvaig, Oigen Rozenstock (Tel Aviv: Ma`arhot hinukh Levinski, 2000).
    Judeo-Christian dialogue, beginning with Spinoza.
- Schwartz, Hans, Eschatology: Complete Introduction to the Christian View of the Future (Eerdmans, 2000).
- Hart, Jeffery P., Smiling Through the Cultural Catastrophe: Towards the Revival of Higher Education (New Haven: Yale University Press, 2001).
    In the Preface, R-H is cited as an inspiration when Hart was an undergraduate at Dartmouth. R-H said, “the goal of education is the citizen. He defined the citizen in a radical and original way. . . . He said that a citizen is a person who, if need be, can re-create his civilization.”
    R-H is not otherwise mentioned in the book.
- Hermeier, Rudolf, ed., Friedensbedingungen der planetarischen Gesellschaft: zur Ökonomie der Zeit/ Eugen Rosenstock-Huessy (Münster: Agenda, 2001).
- Cane, Bill, Passing on the Spirit: Celebrating Eccentric Mentors (Aromas, Calif.: MMPublishing, 2002).
    Contains appreciative chapters on R-H’s student and a friend of Cane’s, the late Page Smith, and on R-H himself as an inspiration. Cane is presently the executive director of IF, a non-profit corporation in California dedicated to helping the poor and disadvantaged.
- Bade, David, Khubilai Khan and the beautiful princess of Tumapel: the Mongols between history and literature in Java (Ulaanbaatar: Chuluunbat, 2002). Also translated into Mongolian, 2006.
    David Bade writes: *“I have cited or quoted ERH's works in several of my publications, including my book on the Mongol invasion of Java. In other works the influence of ERH is evident to anyone who knows his work, but not directly mentioned. Discovering Eugen's work was a major breakthrough for me in 1996. I discovered his work completely by accident while browsing a bookstore for something on sign language and the origin of language.”*
- Berman, Harold J., Law and Revolution II: The Impact of the Protestant Reformations on the Western Legal Tradition (Cambridge, Mass.: Harvard University Press, 2003).
    Berman was a student of Rosenstock’s at Dartmouth. He was a distinguished professor at Harvard for many years and a leading specialist on Soviet law. He later moved to Emory University. This book is one of several by him that explicitly extend the insights of his teacher.
- Klenk, Dominik, Metanomik. Quelenlehre jenseits der Denkgesetze Eugen Rosenstock-Huessys Wegbereitung vom ich-einsamen Denken der neuzeitlichen Philosophie zur gelebten Sprachvernunft (Münster: Agenda, 2003)
- Surall, Frank, Juden und Christen––Toleranz in neurer Perspektive: der Denkweg Franz Rosenzweigs in seiner Bezügen zu Lessing, Harnack, Baeck, und Rosenstock-Huessy (Chr. Kaiser/Güterloher, 2003).
- Gardner, Clinton C. D-Day and Beyond: A Memoir of War, Russia, and Discovery. Philadelphia (Xlibris, 2004).
    This memoir describes the author's lifetime pursuit of the work of R-H. The closing five chapters describe how the author introduced Rosenstock-Huessy's work in Russia during the period 1983-2000. (Norwich Center Books, PO Box 710, Norwich, VT 05055.)
- Huppuch, Willibald), Eugen Rosenstock-Huessy (1888-1973) und die Weimarer Republik: Erwachsenenbildung, Industriereform und Arbeitslosenproblematik (Hamburg: Kovac, 2004). ISBN 3-830-01683-2
- Kirsch, Arthur, Auden and Christianity (New Haven: Yale University Press, 2005).
    In the frontmatter, p. xiv, Kirsch writes of Auden: *“He constantly referred to, reviewed, or echoed such writers as Saint Augustine, Pascal, Soren Kierkegaard, Martin Buber, Paul Tillich, Reinhold Niebuhr, and Eugene Rosenstock-Huessy. . . .”* R-H is here placed in good company, but Kirsch does not discuss him in the body of the book.
- Bade, David, Perra Loca's Terra animata (Ulaanbaatar: Chuluunbat, 2006)
    Fiction and poetry, published under the pseudonym Perra Loca. Bade reports: *“It was while writing this book and doing research on sign language that I first bought and read ERH. The final section is entitled "Respondeo etsi mutabor.”* Bade is a learned librarian at the University of Chicago.
- Baker, Wendy, and Wayne Cristaudo, eds., Messianism, Apocalypse, Redemption: 20th-Century German Thought (Australian Theological Forum [ATF], 2006.
    Includes Cristaudo’s
    - “Redemption and Messianism in Franz Rosenzweig’s The Star of Redemption,” and
    - “Revolution and the Redeeming of the World: The Messianic History of Eugen Rosenstock-Huessy’s Out of Revolution.”
- Schmied-Kowarzik, Wolfdietrich ed., Franz Rosenzweigs “neues Denken”. Internationaler Kongress, Kassel 2004. (München: Verlag Karl Alber, 2006). 2 vols.
    (Vol. 1: Selbstbegrenzendes Denken – in philosophos; Vol. 2: Erfahrene Offenbarung – in theologos).
    This collection is continuously paginated over two volumes. Much in it has bearing on the thought of R-H; the following essays explicitly so.
    - Regina Burkhardt-Riedmiller, “Franz Rosenzweigs Erneuerrung der jüdischen Lerntradition im zeitgenössischen Kontext (inbesondere Eugen Rosenstock-Huessy),” pp. 553-574;
    - Wayne Cristaudo, “Rosenzweig’s and Rosenstock’s Critiques of Idealism. The Common Front of Contrary Allegiances,” pp.1121-1140;
    - Michael Gormann-Thelen, “Franz Rosenzweig Transfigured,” 1141-1150;
    - Harold Stahmer, “Franz, Eugen, and Gritli. Respondeo etsi mutabor,” 1151-1168.
        Pp. 1155-1161 in Stahmer’s piece incorporates Freya von Moltke’s “Uber Eugen, Margrit und Franz,” trans. by Raymond Huessy,“About Eugen, Margrit and Franz.”
- Meir, Ephraim, Letters of Love: Franz Rosenzweig’s Spiritual Biography and Oeuvre in Light of the Gritli Letters (New York: Peter Lang Publishing, 2006).
    Meir observes that virtually nothing has been written about R-H’s influence on Rosenzweig, and declares that the Gritli letters establish that *“More than any other writer, it is Rosenstock who most influenced Rosenzweig.”* (p. 35). However, on the same page he says that *“Hermann Cohen appears to be the thinker who most profoundly influenced Rosenzweig.”*
- Richter, Christoph, Im Kreuz der Wirklichkeit—Die Soziologie der Räume und Zeiten von Eugen Rosenstock-Huessy (Frankfurt am Main: Peter Lang, 2007). ISBN 978-3-631-55773-0
- Goldman, David B., Globalisation and the Western Legal Tradition: Recurring Patterns of Law and Authority (Cambridge, U.K.: Cambridge University Press, 2007).
    Globalisation and the Western Legal Tradition is a broad legal history. It examines jurisprudence and legal philosophy, international public law, jurisdiction and patterns of law and authority, all with the purpose of understanding 21st -century globalization and commercial law. Goldman is influenced significantly by ERH’s Out of Revolution and by Harold Berman’s Law and Revolution, vols. I and II. He is a practicing attorney in Sydney, Australia.
- Cristaudo, Wayne, Power, Love, and Evil: Contributions to a Philosophy of the Damaged (Rodopi: Amsterdam, 2008)
    A remarkable, original work that draws much from R-H as well as other writers. *“As sign-ificant beings we need commandments. That is why, as Rosenstock-Huessy perceptively observes in his writings on speech, the original grammatical move is the imperative. The imperative is the condition that ensures survival, and hence perpetuity. The imperative is the voice of order, the divine voice. God first speaks in the imperative mood.“*
- Gardner, Clinton C., Beyond Belief: Discovering Christianity’s New Paradigm (Norwich, Vermont: White River Press, 2008.
    Christian Century editor Martin Marty lauds Eugen Rosenstock-Huessy as *“a visionary… [who] writes about … Christianity without old-line appeal to transcendence.”* Gardner presents R-H’s work as *“additional scaffolding”* for the new Christian paradigm being constructed by New Testament scholar Marcus Borg, retired Episcopal Bishop John Shelby Spong, and others.
- Neretina, Svetlana Sergeevna. Filosofskie odinochestva (Moskva: Rossiiskaia Akademiia Nauk, Institut Filosofii, 2008).
    [Philosophical Solitudes. Russian].
    The second chapter of this work is on “the grammatical and dialogical method.”
    Viitorul creştinului sau Depăşim Modernitatea. [The Christian Future, or the Modern Mind Outrun. Rumanian].
    Presented to the public on February 20, 2009, at the Protestant Academy of Transylvania in Sibiu.
- Cristaudo, Wayne, The Star and the Cross: Franz Rosenzweig’s and Eugen Rosenstock-Huessy’s Post-Nietzschean Revivals of Judaism and Christianity (forthcoming 2010).
- Cistaudo, Wayne, Religion, Redemption and Revolution: The New Speech Thinking of Franz Rosenzweig and Eugen Rosenstock-Huessy. (2012)
    This is the first book length comparison of Franz Rosenzweig and Eugen Rosenstok-Huessy. It overthrows the longstanding error in Rosenzweig scholarship that Rosenstock-Huessy’s interest in Rosenzweig lay largely in his attempt to convert him. The book argues that they were part of a common project dedicated to reappraising Judaism and Christianity and enabling modern men and women to participate in what Rosenstock-Huessy called a metanomic society, i.e. a society in which we are able to live in creative tension with our differences.

### Journal Articles, Essays in Collections, & Book Reviews

- Bruce Boston review of Die Sprache des Menschengeschlets, 2 vols. (1963), and Speech and Reality (1970) in Theology Today, XXVII, no. 3 (Oct. 1970), pp. 345-348.
In only a few pages Boston excellently summarizes some of ERH’s esessential ideas, but also points out that his work may be too schematic. See also Boston’s 1973 dissertation, cited below.
- ––––––––––, “’I Respond Although I Will Be Changed,’: Reflections on Eugen Rosenstock-Huessy,” Princeton Seminary Bulletin, LXIV, no. 1 (March, 1971), pp. 77-89.
- Hermassi, E., “Towards a Comparative Study of Revolutions,” Comparative Studies in Society and History, XVIII (1976), pp. 211-235.
- Duncanson, Thomas, “Eugen Rosenstock-Huessy and the Moral Criticism of Rhetoric.” Iowa Journal of Speech Communication, XI (Spring 1979), 9-16.
- Faulenbach, Bernd, "Eugen Rosenstock-Huessy," in Hans-Ulrich Wehler, Deutsche Historiker (Göttingen, 1983), pp. 102-126.
- Funke-Schmitt-Rink, Margret, "Rosenstock-Huessy, Eugen,” in Wilhelm Bernsdorf & Horst Knospe, Internationales Soziologen-Lexikon (Stuttgart: Enke, 1984), II, p. 725
- Stahmer, Harold, “’Speech Letters’ and ‘Speech-thinking’: Franz Rosenzweig and Eugen Rosenstock-Huessy,” Modern Judaism, IV, (1984), pp. 57-81. See also Stahmer’s “Speak That I May See Thee!”: The Religious Significance of Language (New York: MacMillan, 1968).
Stahmer was a student of Rosenstock’s at Dartmouth in the 1940s.
- Bryant, M. Darrol, "Toward a Grammar of the Spirit in Society: The Contribution of Eugen Rosenstock-Huessy" in The Many Faces of Religion & Society (St. Paul, MN: Paragon House, 1985), pp. 173-189.
This essay focuses on the contribution of ERH to an understanding of the "spirit in society." It argues that in a planetary era it is imperative for the religions of humankind to disclose their contributions to "our common but multiform social future."
- Duncanson, Thomas, “A Reply to Tate and McConnell’s ‘Afterword and Comment’ to the ‘Special Issue on Teaching Critical Communication Studies.’” Canadian Journal of Communication, 11 (1985), 419-423. Errata, XII (1986), 83.
- Kidd, J. ,“Dialogical Modes of Presence: Buber, Rosenstock-Huessy, and Strasser in Relation to Frankl and Scheler,” in Norman N. Goroff, ed., The Social Context of the Person’s Search for Meaning (Hebron: Practitioner’s Press, 1985, pp. 50-69.
- Mayer, Reinhold, “Zum Briefwechsel zwischen Franz Rosenzweig und Eugen Rosenstock,” in Franz Rosenzweig und Hans Ehrenberg: Bericht einer Beziehung, Arnoldshainer Text, Band 42, ed. Werner Licharz and Manfred Keller (Frankfurt/M: Haag u. Herschen, 1986).
- Stahmer, Harold, “Speech Is the Body of the Spirit: The Oral Hermeneutic in the Writings of Eugen Rosenstock-Huessy (1888-1973),” Oral Tradition, II, no. 2 (1987), pp. 301-322.
- Dietmar Kamper, “Das Nachtgespräch vom 7. Juli 1913. Eugen Rosenstock-Huessy und Franz Rosenzweig,” in Wolfdietrich Schmied-Kowarzik, ed. Der Philosoph Franz Rosenzweig (1886-1929). Internationaler Kongreß Kassel 1986. (Karl Alber, Freiburg / München 1988), 97-104.
- Collins, James, Mentors: Noted Dartmouth Alumni Reflect on the Teachers Who Changed Their Lives (Hanover: Dartmouth College, 1991).
Contains an appreciation of R-H by a former student, Ronald Spiers (Dartmouth, 1948). *“It’s hard to be specific about how he influenced my way of thinking. It was simply broadening and seeing things through a different prism.”* Spiers was U. S. Ambassador to Turkey and Pakistan.
- Hallo, William W., “Two Centenaries,” Leo Baeck Institute Year Book, 1991, vol. XXXVI, pp. 491-499.
Concerns Franz Rosenzweig and R-H.
- Kohlenberger, Helmut, Wilfrid Gärtner, and Michael Gormann-Thelen, ”Eugen Moriz Friedrich Rosenstock-Huessy,” in Tumult, XX (Wien: Turia U. Kant, 1995).
- Moses, Stephan, “On the Correspondence between Franz Rosenzweig and Eugen Rosenstock-Huessy,” in The German-Jewish Dialogue: A Symposium in Honour of Gorge Mosse, ed. Klaus Berghahn (New York: Peter Lang, 1996), 109-123.
- Van der Pijl, Kees, “A Theory of Transnational Revolution: Universal History According to Eugen Rosenstock-Huessy and Its Implications,” Review of International Political Economy, III, no. 2 (1996), pp. 0000
Van der Pijl teaches at the Universeity of Sussex.
- Kaufmann, Franz-Xaver, “Religion and Modernization in Europe,” Journal of Institutional and Theoretical Economics, vol. 153 (1997), pp. 80-99.
Out of Revolution is briefly cited, as is Harold Berman extensively. The focus is on Max Weber, but Kaufmann draws from R-H more than he admits to. Anyone interested in Weber’s question as to the origins of “abendländische Sonderweg,” the specialness of the West in world history, implicit in Rosenstock’s work, will find this piece suggestive.
- Sloterdijk, Peter and Hans-Jürgen Henirichs, “Kantilenen der Zeit: Zur Entidiotisierung des Ich und zur Entgreisung Europas,” Lettre international, no. 36 (1997), pp. 71-77.
Sloterdijk, one of the most celebrated of contemporary German philosophers, refers to R-H in this piece as the greatest theorist of revolution. In his acceptance speech in 2005, when he was awarded the Sigmund Freud Prize by the German Academy of Language and Literature, Sloterdijk referred to R-H as the greatest philosopher of language of the twentieth century.
- Cristaudo, Wayne, “Philosophy, Christianity, and Revolution in Eric Voegelin and Eugen Rosenstock-Huessy,” European Legacy, IV, no. 6 (December 1999), 58-74.
- Kroesen, Otto, “Waarheen voert ons de netwerktechnologie?” in Tijdschrift voor Wetenschap, Techniek en Samenleving (Assen: Uitgeverij vn Gorcum, 1999), vol. 7, no. 3, pp. 100-107.
- Büchsel, E., “Das verlässliche Wort Eugen Rosenstock-Huessy und Johann George Hamann,” Neue Zeitschrift fur systematische Theologie un Religionsphilosophie, XLII, no. 1 (2000), pp. 32-42.
*“The question is whether there is any substantial relation between Johann Georg Hamann and Eugen Rosenstock-Huessy concerning their concentration on speech and language being a gift from heaven and the basis and cornerstone of human society and history. In 1957 Rosenstock-Huessy published in German an imaginary letter from Heraclitus to Parmenides. . . . It is in this letter that the main elements of his speech-thinking are to be found (the emphasis is on names, imperatives, and mutual and true communication). These are set out and compared with the corresponding statements and quotations by Hamann. . . . “*
- Kroesen, Otto, “The Empowerment of Floating Identities,” in Ethics and the Internet, ed. Anton Vedder (Antwerpen, Groningen Oxford: Intersentia, 2001), pp. 143-159.
- Kroesen, Otto, “Eigen en Anders - de wederzijdse toegankelijkheid van culturen in de Twintigste Eeuw,” in Otto Kroesen, Jan Baars, and Richard Starmans, eds, Acta van de Nederlands-Vlaamse Filosofendag, ( Delft: Katholieke Universiteit Brabant, 1999), pp. 161 – 175.
- Kroesen, Otto, “Van theologie naar technologie: duizend jaar ordening’ in Wereld en Zending , no. 1 (2000) pp. 39-48.
- Kroesen, Otto, “Een sociologie van de techniek - E. Rosenstock-Huessy,” in Filosofie , vol. 10, nr. 3 (2000), pp. 29-33.
- Kroesen, Otto, “Post-Christelijke Ethiek?,” in Aan Babels’s stromen. Een Bevrijdend Perspectief op Ethick en Technik ed. Kees Boersma, Jan van der Stoep, Maarten Verkerk, and Ad Vlot (Amsterdam: Buijten en Schipperheijn, Amsterdam, 2002), pp.. 36-54 .
- Samson, Steven Alan, “Edward Rozek: A Student’s Tribute,” Humanitas, XV, no. 2 (2002), 109-112.
Rozek was a teacher of Samson’s at the University of Colorado. Samson cites Rosenstock several times on the meaning of teaching.
- Goldman, David B, “Historical Aspects of Globalization and Law,” in Jurisprudence for an Interconnected Globe, ed. Catherine Dauvergne (London: Ashgate, 2003). \
Goldman is a practicing lawyer in Australia and a productive scholar of legal history on the side. This article develops R-H’s fundamental distinction in chapter 9 of Out of Revolution (1938)––seventy years ago: *“Many sects, many creeds, many races, many ways of education and self-expression, but one unshakable bondage––or freedom––of economic organization will remain for us in the future. The various creeds and denominations and national beliefs will be small parishes in a world-wide economic society.“* *“In the beginning of European history, the opposite proportions between Church and economy prevailed. Economy was husbandry––something local, parochial, narrow––split into myriads of atoms. Christianity claimed universality and unity. One great ocean of creed and an archipelago of economic islands––that was the situation in year 1000. . . . Church and economy have changed their places during the last thousand years.”*
- Ravesteijn, W. en J. O. Kroesen, “De toekomst als opdracht – Utopie, Revolutie en Techniek in Europa,” in Europa, Balans en Richting, (Tielt: Lannoo Campus, 2003).
- Zank, Michael, “The Rosenzweig-Rosenstock Triangle, Or, What Can We Learn from Letters to Gritli?: A Review Essay,” Modern Judaism, XXIII (2003), 74-98. \
A balanced treatment of a sensitive topic about which much information is not yet publicly available..
- Leithart, Peter J., "The Sociology of Infant Baptism," Christendom Essays: Biblical Horizons Issue No. 100. (Niceville, Florida, DATE)
- Cristaudo, Wayne, “Eugen Rosenstock-Huessy: Before, During, and After Post-Modernism,” in Revue Roumaine de Philosophie (2004), 190-203.
- Gormann-Thelen, Michael, “Franz Rosenzweigs Briefe an Margrit (‘Gritli’) Rosenstock. Ein Zwischenbericht mit drei Documenten,” in The Legacy of Franz Rosenzweig. Collected Essays, ed. by Luc Anckaert et al. (Louvain, 2004).
- Hovitz, Rivka, “The Shaping of Rosenzweig’s Identity According to the Gritli Letters,” in Martin Brasser, ed., Rosenzweig als Leser: Kontextuelle Kommentare zum Stern der Erlösung (Tubingen, 2004).
- Kroesen, Otto, ”Imperatives, Communication, Sustainable Development,” in Mitteilungsblätter der Eugen Rosenstock-Huessy Gesellschaft, (Körle, 2004), pp. 101-109.
- Kroesen, Otto, “From Thou to IT: Information Technology from the Perspective of the Language Philosophy of Franz Rosenzweig and Eugen Rosenstock-Huessy,“ in Ulf Görman, W. B. Drees, & H. Meisinger, eds., Creative Creatures: Values and Ethical Issues in Theology, Science and Technology (London - New York: T & T Clark International, 2005) (ISBN 0-567-03088-1).
- Kroesen, Otto,
    - “Wat hebben joden, christenen en moslims elkaar te zeggen?” In de Waagschaal, vol. 34, no. 26 (February 2005), pp. 10-13; ** “Heilseconomie en heilsgeschiedenis, I” In de Waagschaal, vol. 35, no. 10 (February 2005), pp. 10-14;
    - “Heilseconomie en heilsgeschiedenis, II” In de Waagschaal, vol. 35, no. 12, pp. 356-360, pp. 10-14;
    - “Christus als centrum van de wereldgeschiedenis, In de Waagschaal, vol. 36, no. 7-8 (2007), pp. 210-213, 240-242. \
    These are the most recent of many brief contributions Kroesen has made to this publication, In de Waagschaal, going back to 1991, if not before. The reader is advised to check the archives of the publication to see all of the relevant entries.
- Kroesen, J. O., K. F. Mulder, & W. Ravesteijn, “Innovation through pluriformity: technology development in European history and in a globalizing world,“ in D. Trzmielak & M. Urbaniak, eds., Value-added partnering in a changing world (Lodz: Innovation Center, University of Lodz, 2005), pp. 215-220. (ISBN 83-9223750-1).
- Ravesteijn, W. , E. Graaff, & J. O. Kroesen, “Competent to communicate technology: a new perspective on developing communicative skills in engineering education,“ in Cagdas Simsek & Yavuz Yaman, eds., SEFI Proceedings: Engineering education at the cross-roads of civilization (Ankara: Middle East Technical University, 2005), pp. 480-487. (ISBN 975-429-236-1).
- Miyajima, Naoki, Review of Out of Revolution, in Hogaku Shimpo- The Chuo Law Review (Tokyo: Chuo University Law Association), 112 (3.4), (July, 2005) pp. 235-246. \
There is a translation of this review available, by Prof. Lloyd Craighill, Amherst, MA. The review was prompted by the publication of Harold Berman’s Law and Revolution: The Formation of the Western Legal Tradition, which explicitly acknowledges the influence of R-H’s Out of Revolution.
- Leutzsch, Andreas "'Zwischen Welt und Bielefeld. Eugen Rosenstock-Huessy, Georg Müller und ihr Archiv in Bielefeld-Bethel,” Jahresbericht des Historischen Vereins für die Grafschaft Ravensberg (Bielefeld, 2006), vol. XCI, pp. 225-250
- Sherraden, Margaret; John Stringham; Simona Sow; Amanda McBrida, “The Forms and Structures of International Voluntary Service,” Voluntas: International Journal of Voluntary and Nonprofit Organizations, XVII, no. 2 (June 2006), 156-180. \
R-H’s Planetary Service is cited twice, but not discussed substantively.
- Bade, David, “Colorless green ideals in the language of bibliographic description: making sense and nonsense in the library. Language & Communication, XXVII no.1 (January 2007), 54-80. \
Cites R-H with relevance to the implicit communication between a library cataloguer, making decisions about how to describe a book, and the researcher who may be seeking such a book. See also, Bade, Misinformation and meaning in library catalogs (Chicago: D. W. Bade, 2003) and Bade, The theory and practice of bibliographic failure, or, Misinformation in the information society (Ulaanbaatar: Chuluunbat, 2004).
- Cristaudo, Wayne, “Revolution and the Redeeming of the World: The Messianic History of Eugen Rosenstock-Huessy’s Out of Revolution,” in Messianism, Apocalypse, Redemption: 20th-Century German Thought, ed. Wendy Baker and Wayne Cristaudo (Adelaide: Australia Theological Forum, 2006).
- --------------, “Rosenzweig’s and Rosenstock’s Critiques of Idealism: The Common Front of Contrary Allegiances,” in Franz Rosenzweig’s “Neues Denken,” ed. Wolfgang Schmied-Kowarzik (Freiburg: Karl Alber, 2006).
- Brueggemann, Walter , “Life-Giving Speech Amid an Empire of Silence,” Michigan Law Review, CV, (April 2007), pp. 1115-1132. \
A review essay of James Boyd White, Living Speech: Resisting the Empire of Force. Brueggemann is a distinguished Old Testament scholar, retired from Columbia Theological Seminary. The essay includes a long quotation from R-H’s Speech and Reality.
- Kroesen, J. O., “From empire to globalization and oecumene,” in Student World: Ecumenical Review World Student Christian Federation (2007), 26-34.
- Kroesen, J. O., “Onderweg naar een andere bron van gezag,” in Speling: Tijdschrift voor bezinning (2007), 59(3), pp. 28-34.
- Hart, Jeffrey, “Jeffrey Hart on Prof. Eugen Rosenstock-Huessy,” The Dartmouth Review (Monday, August 11, 2008). \
This short essay, in a publication edited by Dartmouth College students, is most easily found on the Review’s website, at: dartlog@dartreview.com. Hart is a professor emeritus of English literature at Dartmouth who, when he was an undergraduate at the College, took courses from ERH.
- Duncanson, Thomas, “Discursive Disease and Toxic Leadership in the Speech-Thinking of Eugen Rosenstock-Huessy.” Das Beispiel Russland. Rudolf Hermeier, ed. (forthcoming)
- Cristaudo, Wayne, “Love Is as Strong as Death: The Triadic Love of Franz Rosenzweig, Eugen Rosenstock-Huessy, and Gritli Rosenstock-Huessy,” in Persons, Love, and Intimacy (Rodopi: Amsterdam forthcoming). Vol. I, At the Interface Series.
- Jakubowski, Zbigniew, “Myślenie mowy w dążeniu do osiągnięcia ładu i tożsamości społecznej według Eugena Rosenstocka-Huessy” [Speech-thinking in Pursuit of Social Order and Peace by Eugen Rosenstock-Huessy. Polish] (forthcoming in a Warsaw journal). \
Jakubowski has also translated Rosenstock’s “Farewell to Descartes” chapter (from Out of Revolution) into Polish.

### Online/Electronic

(In alphabetic order by author)

- Castle, Robert, "From Desperation to Salvation: Concealing and Revealing Nothing in History," Archipelago, Volume 6, Numbers 3 and 4, 2003. (online at: www.archipelago.org/vol6-3/castle.htm)
- Cristaudo, Wayne, “Eugen Rosenstock-Huessy,” Stanford Encyclopedia of Philosophy. (https://plato.stanford.edu/). Available online in what has become a highly authoritative source, this essay is now probably the best short introduction to RH’s thought.
- Floyd, Douglas, “Doug Watching” (dougfloyd.wordpress.com), writes in December 1, 2006, on his blog: “Here’s a thoughtful quote by one of my new mentors: Eugen Rosenstock-Huessy. I discovered him about two years ago, and the life force that exudes from his speaking (and writing) overwhelms me.” \
    Here’s the quote from R-H: *“The world was created for peace. But . . . the act of creating the world is a perpetual act. What we call the creation of the world is not an event of yesterday, but the event of all times, and goes on right under our noses. Every generation has the divine liberty of recreating the world.”* \
    See also this website of Floyd’s: https://www.scribd.com/doc/49922/What-Can-We-Learn-from-Eugen-RosenstockHuessy
- Hartman, Charles Howard, offers this website: https://www.scribd.com/doc/33383/HOST6733Rhyming-Covenant-Sequences
    It is esoteric. Make of it what one will. However, here is an injunction from it that everyone can understand: “To taste [R-H] completely, listen to his lectures, listen to him singing to his students and audiences. ‘Cross of Reality 1953’ is second; first listen to ‘Universal History 1957’. ERH said that he put his musical talents into history, and teaching history. The phrase ‘symphony of history’ is his, in Fruit of Lips.”
- Leithart, Peter J. See at Leithart.com:“Grammatical Sociology”; "The Politics of Emma's Hand" (under “First Things”)
- Peter Leithart, “The Relevance of Eugen Rosenstock-Huessy,” an essay in the online version of First Things: The Journal of Religion, Culture, and Public Life (June 28, 2007).
    (https://www.firstthings.com/onthesquare/?p=786)
    This recent work is one of the best brief introductions to R-H’s thought.
- McDuffee, Mike, “An Introduction to the Christian Thought of ERH: The Strange Catechism of the Christian Future,” presented at the 56th Annual Meeting of the Evangelical Theological Society, Nov. 2004. A copy of this paper is accessible at https://www.reclaimingthemind.org/papers/ets/2004/Mcduffee2004/Mcduffee2004.pdf \
    McDuffee concludes his paper with this comment: “The compressed complexity yet consistency of [Rosenstock’s] thinking causes both intrigue and frustration. It is literally painful to read him, so much grates against the sensibilities we accrue and hone through subjecting ourselves to the norms of the academic institution. At the same time, however, his flashing insights and surgical aphorisms stun the reader, evoking spells of intellectual vertigo, forcing sober reflection, offering respites of refreshment, which altogether generate, I believe, a call for further examination. It is my hope that others might hear this call and risk being changed through the rewards of response.”
- Preston, Scott M., "The Dark Age Blog" (www.darkage.ca/blog). \
    This blog, which is written under the pseudonym "Longsword," includes dozens of references to R-H.
    Williamson, Donald, Jr., has a private, online publishing venture (no website), a Christian educational project, he calls it, and often quotes R-H. “I hope I can be of some help in applying ERH's teachings to the practical problems we face,” he wrote.

### Doctoral Dissertations and Other Special Formats

- Boston, Bruce O., “’I Respond Although I Will Be Changed’: The Life and Historical Thought of Eugen Rosenstock-Huessy" (Ph.D. dissertation, Princeton Theological Seminary, 1973). \
See also the prospectus, "'I Respond Although I Will Be Changed': Reflections on Eugen Rosenstock-Huessy,” Princeton Seminary Bulletin, LXIV, No. 1 (March, 1971), pp. 77-89.
- Ritzkowsky, Ingrid Brings, Rosenstock-Huessy’s Konzeption einer Grammatik der Gesellschaft (Ph.D. dissertation, Freie Universität Berlin, 1973).
- Pfister, Lauren Frederick, “Fraught with Reality: Philosophical Anthropology in the Dialogue and Works of Franz Rosenzweig and Eugen Rosenstock-Huessy” (Master’s degree thesis, San Diego State University, 1982)
- Dinteren-Goosesens, Adrie G. M. van, “De mens lijfelijk in het kruis der werkelijkhjeid bij Eugen Rosenstock Huessy,” (Doctoral thesis in Theology, Tilburg, 1985).
- Leithart, Peter J., “The Priesthood of the Plebs: A Theology of Baptism” (Ph. D. dissertation, Cambridge University, 1998).
Integrities, XII, Double Issue, 1998. \
This is a quarterly magazine based in Watsonville, California, and edited by Bill Cane, for whom R-H is a regular source of inspiration. Integrities is the organ of IF, a charitable organization that focuses on economic and social assistance to people in need, primarily in Central America. There is rarely an issue of Integrities that does not include quotations from R-H, but in this case not as a historian, or sociologist, or theologian, or philosopher of language, but as a spur to social action. This 1998 issue cited is devoted almost entirely to R-H.
- Duncanson, Thomas, “Never One Thing: Philosophical Anthropology in the Speech Thought of Eugen Rosenstock-Huessy” (Ph.D. dissertation, University of Iowa, 2001).
- Lilla, Mark, “A Battle for Religion,” New York Review of Books (Dec. 5, 2002). Notorious for its offhand, uninformed belittling of R-H in relation to Rosenzweig.
- Marty, Martin E., “A Life of Learning,” Charles Homer Haskins Prize Lecture
(New York: American Council of Learned Societies Paper, No. 62, 2006).
- Rutler, George W., “Cloud of Witnesses/Eugen Rosenstock-Huessy,” Crisis Magazine, XXIV, no. 8 (October 2006). \
An appreciative one-page biographical sketch that incorrectly identifies R-H as a Roman Catholic. Rutler graduated from Dartmouth in 1965 and writes a regular column for Crisis.
- “Eugen Rosenstock-Huessy,” Encyclopaedia Judaica, 2nd ed. (Thomson Gale, 2007), XVII, pp. 450-451. \
Williamson, Donald, Jr., has a private, online publishing venture (no website), a Christian educational project, he calls it, and often quotes R-H. “I hope I can be of some help in applying ERH's teachings to the practical problems we face,” he wrote.

### Unpublished Conference Presentations and the Like

- Tate, Eugene D., “The Communication Theorist as Pirate and Argonaut: Eugen Rosenstock-Huessy and Communication Theory,” \
Paper presented at the Canadian Communication Assn. Conference, Univ. of Guelph, Guelph, Ontario, 1984. 27 leaves.
- Stahmer, Harold, “Mikhail Bakhtin (1895-1975) and Eugen Rosenstock-Huessy (1888-1973): Speech, the Spirit, and Social Change,” Paper presented at the Transnational Institute East-West Conference, Moscow, 1993.
- Papers presented at the conference entitled “Respondeo etsi mutabor: An International Conference for the Rosenstock-Huessy Centenary,” sponsored by Dartmouth College and the E. R.-H. Fund, Hanover, New Hampshire, August 15-19, 1988.
    - Hans Achterhuis, “Beyond Wage Labor”;
    - Berman, Harold, “Law and History After the World Wars”;
    - Bryant, M. Darroll, “Tracking the Spirit: History and Culture in R-H”;
    - Bürger, Wolfram, “Universitât und Kirchliche Hochschule Erwägungen zu einer Ortsbestimmung von Lehre und Forschung der evangelischen Kirche in der DDR”;
    - Castle, Robert, “Desperation to Salvation: R-H’s Higher Education”;
    - Davidson, Frank, “Voluntary Service: Society’s Third Sector”;
    - Duncanson, W. Thomas, “System, Subversion, and Crisis: R-H and the Sociology of the Higher Learning”;
    - Farmer, William, “The Gospels as the Lips of Jesus”;
    - Feringer, Richard and Philip Chamberlin, “Correspondence: The World View of R-H”;
    - Fiering, Norman, “R-H as Teacher”;
    - Fraser, James, “How Do We Control the Accelerating Speed of the Global Economy in an Age of Continuous Change”;
    - Gardner, Clinton, “Christianity in the Third Millennium”;
    - Huessy, Hans R., “An Impure Thinker Collides with a Progressive School”;
    - Huessy, Mark, and Frances Huessy, “Vox Clamantis in Deserto: R-H after His Immigration”;
    - Huebschmann, Heinrich, “Dialogue as Therapy”;
    - Jacob, Wolfgang, “Die ‘Vollzahl der Zeiten’ als Aporie der Medizin”;
    - McConnell, Kathleen and Eugene Tate, “Glasnost: Restoring Health to Academic Discussion”
    - Meyer, Marshall Rabbi, “Revolution, Speech, and Spirit”;
    - Möckel, Andreas, “Die Höhere Grammatik als Grundlage der Heilpädagogik”;
    - Paasche, Gottfried, “Reflections on Teaching Sociology”;
    - Scott, Mark Murphy, “The Promise of Law and the Law of Promise in R-H’s American Revolution”;
    - Shaull, Richard, “Revolution, Speech, and Spirit”;
    - Stahmer, Harold, “Revolution, Speech, and Spirit”;
    - Steinlein, Stephan, “Gebet und Arbeit. Die evangelischen Kirchen in der DDR zwischen ‘religiöser Sinnstiftung’ und Kreuzesnachfolge”;
    - Ullmann, Wolfgang, “R-H’s Opposition to Linguistic Agnosticism”;
    - Von Hammerstein, Franz, “Friedensdienste in Europa nach dem 2. Weltkrieg auf dem Hintergrund von ‘Dienst auf dem Planeten’”;
    - Vos, Ko, “De werkwijze van de wetenschap en het onderwijs”;
    - Wells, Michael, “Work Service: A Personal Perspective”;
    - Jacob, Wolfgang, “Die Vollzahl der Zeiten als Aporie der Medizin”;
- Papers presented at the conference entitled, “Planetary Articulation: The Life, Thought, and Influence of Eugen Rosenstock-Huessy,” sponsored by Millikin University and the E. R.-H. Fund, meeting in Monticello, Illinois, June 2002.
    - Beicheng, Liu, and Xu Weixiang, “Report on translating R-H into Chinese”;
    - Bryant, M. Darroll, “Encountering ERH”;
    - Carvalho, Olavo de, “Translating R-H’s Origin of Speech into Portuguese”;
    - Castle, Robert, “R-H and Ortega y Gassett on Generations”;
    - Cristaudo, Wayne, “The Relevance of R-H Before, During, and After Post-Modernism”;
    - Duncanson, W. Thomas, “Never One Thing: Metanoia, Decision, Love, and Difference”;
    - Feringer, Richard, “A Conversation between an Aboriginal Tribal Member and R-H, April 1966”;
    - Gardner, Clint, “R-H in Russia and Iran: Responding to the Events of September 11”;
    - Hartman, Charles, “Seated With: Articulation and Participation by the Calendar”;
    - Houweling, Feico, “Planetary Posts: The Moral Equivalent to Globalization in Loving Memory of Bob O’Brien”;
    - Huessy, Frances, “R-H and His Student’s Bequest”;
    - Huessy, Mark, “Piracy, Technology, September 11, and the Cellar Walls”;
    - Huessy, Peter, “Notes on Creating the Future: Martin Luther King and Ronald Reagan”;
    - Huessy, Raymond, “Life, Teaching, and Influence: In Search of a Biography”
    - Jackson, Giles, “Venture Smith and the Cross of Reality”;
    - Kroesen, J. Otto, “Fourfold Responsibility in an Engineering Course in Ethics”;
    - Lee, Paul A., “R-H and the Moral Equivalent of War”;
    - Makhlin, Vitaly, “After the Revolution: In Search of a New Orientation with Constant Reference to R-H’s Analysis of Russia”; and “Report on translating Rosenstock-Huessy into Russian”;
    - Myers, Paul, “From Studying Eugen to Standing in Eugen’s Study: The Grammatical Method for Group Leadership”;
    - Pfister, Lauren, “Metaphysical Musings on Play, Sport, and the Temptation of the Extraordinary”;
    - Redmond, Barbara, “As the Spirit Soars, So Does the Heart Sound- Voice – Silence”;
    - Simmons, Terry, “R-H Among the Redwoods: Language, Universal History, and the Liberal Arts College Ideal”;
    - Smith, Robert S., “Planetary Pedagogy”;
    - Ullmann, Wolfgang, “R-H’s Contribution Toward Solving a Problem Facing Modern Society”;
    - Van der Molen, “Trinity Sunday”;
    - Wilson, Donald, “The Hegemony of Professionalism: From ‘Awe’-thority to ‘Author’-ity.”
- Papers presented at the Rosenstock-Huessy Roundtable, meeting in Norwich, Vermont, July 7, 2006. (Note: The texts of these papers are available online via the Argo Books website or http://erhpaperdownloads.blogspot.com.
    - Gardner, Clinton, “In Whom We Live and Move and Have Our Being: R-H and Nikolai Berdyaev as Prophets of Panentheism.”
    - Simmons, Terry, “Varieties of Military Experience: R-H, William James, and the Moral Equivalent of War”;
    - Richter, Christopher, “Approaching Temporality and Human Time-Consciousness Through the Sociology of R-H”;
    - Duncanson, W. Thomas, “Rodomontade: The Attack on the Student as Pedagogical Tactic”;
    - Emerick, Christopher, “Waiting to Inhale: Toward an Understanding of the Spirit as the (Pre)Condition for the Possibility of Speech”;
    - Wilson, Donald, “The Hegemony of Professionalism: Part II”;
    - Oglice, Emanuel, “Self in Community: Man’s Spirit in the Coming Age”;
    - Berman, Harold, “World Law: An Ecumenical Jurisprudence of the Holy Spirit”;
    - Cargill, Meredith, “The Communication Theory of R-H”;
    - Goldman, David, “What R-H Can Teach Us About Globalization and Law”;
    - Lane, Eric, “Why R-H’s ‘Speech and Reality’ Is Important.”
- Papers presented at the Dartmouth College conference on July 11-12, 2008, “Eugen Rosenstock-Huessy / Franz Rosenzweig: The Dimensions of a Relationship”
    - Wayne Cristaudo, “Rosenstock, Rosenzweig, and Nietzsche”;
    - Robert Erlewine, “The Stubbornness of the Jews: Symmetries and Assymetries in Judaism Despite Christianity” ;
    - H. Michael Ermarth, “From Here to Eternity: The Philosophy of History of Eugen Rosenstock-Huessy as Eschatology on the Transmodern Installment Plan;
    - Michael Gormann-Thelen, “ Rosenstock-Huessy’s Soziologie”;
    - Gregory Kaplan, “Why Rosenstock-Huessy Thought Rosenzweig Could Not Simply Remain a Jew”;
    - Claire Katz, “Training Soldiers at Camp William James”;
    - Peter Leithart, “The Social Articulation of Time”;
    - Donald Pease, “Liturgical Thinking”:
    - Randi Rashkover, “Judaism Despite Christianity”.

Appendix A: **Duncanson, W. Thomas**, Various Scholarly Presentations

- “Names, Imperatives, and Articulation in the Speech Thought of Eugen Rosenstock- Huessy,” presented to the Central States Communication Association, meeting at Indianapolis, Indiana, 8 April 2006.
- “The Ontology of Dishonesty: A Study in Communication Ethics,” presented to the Central States Communication Association, meeting at Indianapolis, Indiana, 6 April 2006.
- “Discursive Disease and Toxic Leadership in the Speech-Thinking of Eugen Rosenstock-Huessy,” presented to the “Globalisierte Wirtschaft und HumaneGesellschaft—das Beispiel Russland,” meeting at the Martin Niemöller Haus of The Evangelische Akademie Arnoldshain, Arnoldshain, Germany, 18 November 2005.
- “Philosophy Matters in Service Learning: Dewey, Freire, and Rosenstock-Huessy.” Presented to the National Communication Association, meeting at Chicago, Illinois, 13 November 2004.
- “Anti-Rhetoric and Anti-Philosophy in the Speech Thought of Eugen Rosenstock-Huessy.” Presented to the Eastern Communication Association, meeting at Washington, DC, 27 April 2003.
- “Semiosis and Genocide.” Presented to the National Communication Association, meeting at New Orleans, Louisiana, 23 November 2002. (A substantially revised and re-contextualized version of “Broken Crossed Unreality. . .” presented to the Southern States Communication Association, April 2001.)
- “Theological Language and Symbolic Action in the Speech Thought of Eugen Rosenstock-Huessy.” Presented to the National Communication Association, meeting at New Orleans, Louisiana, 21 November 2002.
- “A Science of Bodies and the Appeal to Somebody: Rosenstock-Huessy’s ‘Rhetoric’ of Science.” Presented to the National Communication Association, meeting at Atlanta, Georgia, 1 November 2001.
- “Broken Crossed Unreality: A Rosenstock-Huessyan Reading of Shoah Discourse.” Presented to the Southern States Communication Association, meeting at Lexington, Kentucky, 8 April 2001.
- “Rosenstock-Huessy’s Odd Requirement.” Presented to the National Communication Association, meeting at Seattle, Washington, 9 November 2000.
- “Knowing / Speaking: A Note on Rosenstock-Huessy, Rhetoric, Epistemology.” Presented to the Western States Communication Association, meeting at Sacramento, California, 28 February 2000.
- “Theorizing Revolution and the Revolutionary From the Cambodian Example.” Presented to the Speech Communication Association, meeting at Chicago,Illinois 30 October 1992.
- “The Power of Death; the Remnants of Love.” Contributed to “Towards an Economy of Times: An International Conference on the Work of Eugen Rosenstock-Huessy” sponsored by the University of Twente, meeting at Enschede, The Netherlands, 18 August 1992.
- “Audi, ne moriamer: A Reply to Lamoureux, Wildeson, Leroux, and O’Rourke.” Presented to the Speech Communication Association, meeting at New Orleans, Louisiana, 4 November 1988.
- “A Note on Rosenstock-Huessy, Communication Studies, and the Prospects for Peace in Our Time.” Contributed to the International Communication Association, meeting at New Orleans, Louisiana, 1 June 1988.
- “In the Service of Disagreement: The Multiformity of ‘No’ in the Speech Thought of Eugen Rosenstock-Huessy.” Contributed to “Word, Service, and Reality: An International Conference” sponsored by the Eugen Rosenstock-Huessy Society of Germany, meeting at Berlin, BRD, 22 July 1985.
- “Six Propositions Toward a Unified Theory of Decadence for Rhetorical Criticism.” Presented to the Speech Communication Association, meeting at Chicago, Illinois, 3 November 1984.
- “Critical Knowing, Cultural Ideals, and the University.” Presented to the Iowa Communication Association, meeting at Newton, Iowa, 16 September 1983.

Appendix B: **Kroesen, J. Otto**, Various Scholarly Presentations

- ‘Fourfold Responsibility in an Engineering Course on Ethics’, in ‘Planetary Articulation: The Life, Thought, and Influence of Eugen Rosenstock-Huessy’, Conference Proceeedings, Millikin University, Decatur, Illinois USA, June 2002, pp. 81-90
- ‘Preparation of students for Participatory and Sustainable Development Projects in Non-Western Countries’ - Otto Kroesen and Martine Ruijgh-van der Ploeg, in de conference proceedings van de ‘Conference on Engineering Education in Sustainable Development’ TU-Delft 24-25 October 2002 (Paper 61) pp 238-246. ISBN 90.5638.090.0
- Europa met de blik op Oneindig, Nederlands Vlaamse filosofiedag, 6 november 2004, blz. 1-5
- Kroesen, JO (Sect. Philosophy) (2005, juni 03). A socio-historical perspective on policy and value transfer. Boston, Mass., 25th Annual Conference of the International Association for Impact Assessment.
- Kroesen, JO (Sect. Philosophy) (2005, juni 03). Participatory development in rural Bangladesh. Boston, 25th Annual Conference of the International Association for Impact Assessment.
- Kroesen, JO (Sect. Philosophy), & Ravesteijn, W (Sect. Technology Assessment) (2006). A cascade of inspiration: a new perspective on the periodization of European history. In 6th European Social Science History Conference . Amsterdam: International Institute of Social History.
- Kroesen, JO (Sect. Philosophy), & Ravesteijn, W (Sect. Technology Assessment) (2006). Priests of Positivism: Engineers and the Legacy of the Great European Revolutions in a Globalizing World. In Locating engineers: Education, Knowlegde , Desire (pp. 1-14). Blacksburg: Virginiatech.
- Jong, WM de (TPM Sect. Policy, Organisation and Management) & Kroesen, JO (TPM Sect. Philosophy) (2007). Understanding how to import good governance
- Practices in Bangladeshi villages. Knowledge, technology and policy, 19(4), 9-25.
- Kroesen, JO (TPM Sect. Philosophy) & Ravesteijn, W (TPM Sect. Philosophy) (2007). Between Spengler and Rosenstock-Huessy: twofold or threefold thinking within a fourfold reference framework. In G Bluhm (Ed.), The communicatioive construction of transnational political spaces and times (pp. 1-13). Bielefeld. Wetensch. publicatie (artikel in bundel - proceedings / Conf.proc. > 3 pag)

[to beginning of page](#top)
