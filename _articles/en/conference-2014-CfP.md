---
title: "Conference 2014 - CfP"
category: reception
language: en
page-nr: 425
---

### Eugen Rosenstock-Huessy: Then & Now
Conference at Renison University College - June 19-22, 2014

A Call for Papers

This conference will focus on particular and historic moments, events, relationships, and writings in the life of ERH and an assessment of his enduring contributions to our time and to the future of the emerging planetary society.

It is our hope that all those attending the conference will make a written contribution to our deliberations. However, we happily welcome as well those who would like to attend more as listeners and as part of the dialogue than paper writers. Those written contributions can be long or short and can range from personal recollections of the impact of ERH on your life and work to more scholarly studies of his writings, topics ranging from ERH’s Christianity, his Speech-Thinking, the industrial revolution, sociology, ERH & Martin Buber, adult education, and his public legacy. You will recall that we will NOT be reading papers at the Conference, but papers will be sent to everyone by e-mail to be read in advance so that you come ready to enter into the discussion concerning ERH: Then and Now.

The conference will begin with registration on Thursday afternoon, June 19, 2014, and conclude by 1:00 PM on Sunday, June 22, 2014. Registration will be open at 1:00 on Thursday and our first session will be held at 4:30 that afternoon.

Renison University College, along with Conrad Grebel University College, St. Paul’s University College, and St. Jerome’s University, is located on the campus of the University of Waterloo. They are federated and affiliated university colleges with residences and academic programs that are fully integrated into and part of the academic life of the University of Waterloo. Each of the university colleges has church origins: St. Jeromes (Catholic), Renison (Anglican), St. Paul’s (United Church), and Conrad Grebel (Mennonite). Registration and conference sessions will be held at Renison, meals and lodging will be available at Conrad Grebel.

The twin cities of Kitchener and Waterloo are located one and a half hours west of Toronto, Ontario. International participants fly into Toronto’s Pearson International Airport. We will try to arrange some transportation from the airport to Waterloo. The regular Airways Transit is 75.00-80.00 per person. If you are flying into Toronto and inform us of your arrival time and there are several of you arriving within a window of a couple of hours, we will arrange pick-up and transport to Waterloo for c. 25.00 per person.

There will be a Conference fee of $100.00 which will cover the cost of a Conference Booklet containing all the papers submitted for the Conference and a closing banquet. (Payable upon arrival at Renison in 2014). The Conference papers will be distributed by e-mail to all those who register for ERH: Then & Now as they are received. When you arrive at the Conference you will then receive your copy of the Conference Book. (The cost of mailing these in advance by post is prohibitive but you will all have access to e-mail versions of the papers/reflections/memories in advance.)

It will also be possible to obtain lodging on campus (at Conrad Grebel College, a 4-minute walk from Renison) for three nights (Thursday, Friday, & Saturday) at the rate of $42.00 per night shared dorm room (breakfast included), or $71.00 p.n. (breakfast included) for a single room in the dorm, both options involved shared bathroom facilities. There are also a couple of apartments available that house 4 persons with four single beds and include a living room and shared kitchen facilities at 135.00 or 35.00 per person. (If you would like to be one person in a shared apartment, please let me know and I will let you know who else would be in that shared apartment.) Lodging at Conrad Grebel University College was made necessary by renovations that will be underway at Renison. The cost for three nights lodging would range from a low of c. $126.00 to a high of c. $210.00

There are also motels available within ten minutes of the University if you prefer to make your own arrangements. I would recommend either the Waterloo Inn (519-884-0220) or the Best Western St. Jacobs Country Inn (519-884-9295). Each is within ten minutes.

Meals will also be available at Conrad Grebel University College beginning with dinner on Thursday evening, June 19, 2014. All those meals will be in the College Dining Hall and shared with College students, though an area for Conference participants will be set aside. Meals would include (Thursday dinner, Friday breakfast, lunch, dinner, & Saturday breakfast, lunch). And a meal ticket for these meals would cost approximately $50.00 if you stay on campus, slightly higher if off-campus. You could also choose to eat off campus or at the restaurant at the Campus Centre (5-6 minute walk from Renison) or one of the other on-campus food centres.

We do NOT want anyone to stay away from this event due to costs. If these costs – from registration costs to food and lodging costs to travel costs – are too onerous please let me know (mdbryant@uwaterloo.ca) . We are expecting there will be some money available for those needing assistance. You can also e-mail me for any others questions you might have concerning the forthcoming Conference.

At this point I have heard from 10 or 12 people concerning their intention to participate in the Conference. My expectation is that we will have 25-30 Conference attendees. It will be helpful to me if you would inform me of your intentions earlier rather than later. It will facilitate the organization of the event and ensure that everything will be in place to welcome you to ERH: Then & Now on June 19th, 2014. I hope to see you in Waterloo in June 2014!

PROF. DARROL BRYANT, University of Waterloo (mdbryant@uwaterloo.ca)
