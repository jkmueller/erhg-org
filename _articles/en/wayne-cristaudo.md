---
title: "Wayne Cristaudo over Rosenstock-Huessy"
category: reception
order: 10
language: en
page-nr: 740
published: 2023-09-13
---
![Wayne Cristaudo]({{ 'assets/images/Wayne-Cristaudo.jpg' | relative_url }}){:.img-right.img-small}

Wayne Cristaudo has the Chair in Politics at Charles Darwin University Northern Territory after having taught European Studies at the University of Hong Kong for seven years.
His [page at CDU](https://www.cdu.edu.au/research-and-innovation/higher-degree-research/find-supervisor/policy/professor-wayne-cristaudo) with the list of publications

### Why Eugen Rosenstock-Huessy matters
Wayne Cristaudo wrote an essay and recorded a video message on [the 50th Anniversary](https://www.erhfund.org/wayne-cristaudo-on-the-50th-anniversary-of-rosenstock-huessys-death/) of Rosenstock-Huessy’s Death. An [extended version of the essay]({{ 'assets/downloads/cristaudo_wayne_why_Eugen_Rosenstock-Huessy_matters_230901.pdf' | relative_url }}) contains additionally an introduction section.


### Idolizing the Idea (2019)
Ever since Plato made the case for the primacy of ideas over names, philosophy has tended to elevate the primacy of its ideas over the more common understanding and insights that are circulated in the names drawn upon by the community. Commencing with a critique of Plato's original philosophical decision, Cristaudo takes up the argument put forward by Thomas Reid that modern philosophy has generally continued along the 'way of ideas' to its own detriment. His argument identifies the major paradigmatic developments in modern philosophy commencing from the new metaphysics pioneered by Descartes up until the analytic tradition and the anti-domination philosophies which now dominate social and political thought. Along the way he argues that the paradigmatic shifts and break-downs that have occurred in modern philosophy are due to being beholden to an inadequate sovereign idea, or small cluster of ideas, which contribute to the occlusion of important philosophical questions. In addition to chapters on Descartes, and the analytic tradition and anti-domination philosophies, his critical history of modern philosophy explores the core ideas of Locke, Berkeley, Malebranche, Locke, Hume, Reid, Kant, Fichte, Hegel, Schelling, Marx, Kierkegaard, Schopenhauer, Nietzsche, Husserl and Heidegger. The common thread uniting these disparate philosophies is what Cristaudo calls 'ideaism' (sic.). Rather than expanding our reasoning capacity, 'ideaism' contributes to philosophers imposing dictatorial principles or models that ultimately occlude and distort our understanding of our participative role within reality. Drawing upon thinkers such as Pascal, Vico, Hamann, Herder, Franz Rosenzweig, Martin Buber and Eugen Rosensock-Huessy Cristaudo advances his argument by drawing upon the importance of encounter, dialogue, and a more philosophical anthropological and open approach to philosophy.

**Wayne Cristaudo: Idolizing the Idea**  
*A Critical History of Modern Philosophy*<sup>[A](https://www.amazon.com/Idolizing-Idea-Critical-Philosophy-Political/dp/1793602352)</sup>  
Lexington Books (October 16, 2019) <sup>[R](https://rowman.com/ISBN/9781793602367/Idolizing-the-Idea-A-Critical-History-of-Modern-Philosophy)</sup>

To the memory of my teacher Eugen Rosenstock-Huessy and in friendship to Michael Gormann-Thelen

*Preface*  
*Acknowledgments*  
*Introduction*  
Chapter 1 *Ideas and Names—A Philosophical Crossroad*  
Chapter 2 *Mechanistic Metaphysics, the “Way of Ideas,” and the Understanding’s Rule of the Imagination*  
Chapter 3 *Metaphysical Quandaries along the “Way of Ideas”*  
Chapter 4 *The Return of the Idea to the Everyday World*  
Chapter 5 *Transcendental, Subjective, and Objective Idealisms: A Matter of Absolutes*  
Chapter 6 *Schelling on Thinking and Being: An Absolute to End All Absolutes*  
Chapter 7 *Post-Hegelianism—or the Idea in Our Action in Nineteenth-Century Philosophy*  
Chapter 8 *The Analytic Retreat to Reason and the Relative Splintering of the Idea*  
Chapter 9 *Husserl’s Idea of Phenomenology and Heidegger’s Being (an Idea in Spite of Itself)*  
Chapter 10 *The Chosen Path of the Idea-isms of the 1960s: Anti- domination, Limitless Freedom, and the Politics and Ethics of the Impossible*  
*Conclusion*  
*Bibliography*  
*Index*  
*About the Author*

A [review](https://voegelinview.com/idolizing-the-idea-a-critical-history-of-modern-philosophy/) of *Wayne Cristaudo: Idolizing the Idea* by Lee Trepanier

### Religion, Redemption, and Revolution (2012)

Religion, Redemption, and Revolution closely examines the intertwined intellectual development of one of the most important Jewish thinkers of the twentieth century, Franz Rosenzweig, and his friend and teacher, Christian sociologist Eugen Rosenstock-Huessy. The first major English work on Rosenstock-Huessy, it also provides a significant reinterpretation of Rosenzweig's writings based on the thinkers' shared insights — including their critique of modern Western philosophy, and their novel conception of speech.

This groundbreaking book provides a detailed examination of their ‘new speech thinking’ paradigm, a model grounded in the faith traditions of Judaism and Christianity. Wayne Cristaudo contrasts this paradigm against the radical liberalism that has dominated social theory for the last fifty years. Religion, Redemption, and Revolution provides powerful arguments for the continued relevance of Rosenzweig and Rosenstock-Huessy's work in navigating the religious, social, and political conflicts we now face.

**Wayne Cristaudo: Religion, Redemption, and Revolution**  
*The New Speech Thinking Revolution of Franz Rozenzweig and Eugen Rosenstock-Huessy* <sup>[A](https://www.amazon.com/Religion-Redemption-Revolution-Rozenzweig-Rosenstock-Huessy/dp/1442643013/)</sup>  
University of Toronto Press, Scholarly Publishing Division (April 28, 2012)<sup>[T](https://utorontopress.com/us/religion-redemption-and-revolution-3)</sup>

To (the memory of) Freya and the Huessys

*We are the internal foe; don't mix us up with the external one! Our enmity may have to be bitterer than the enmity of the external foe, but all the same - we and you are within the same frontier, in the same Kingdom.*  
Rosenzweig to Rosenstock-Huessy, 7 November 1916,  
Judaism Despite Christianity

*I have here a thick volume of theological biographies ... Franz Rosenzweig is included. So am I. But I'm not mentioned in his life and he isn't mentioned in mine. One can do that, I suppose. Life goes on more comfortly without the effort of the truth. But the senselessness of the volume is really astonishing. Franz and I overcame the division in faith. That, however, will only be conceded 50 years after my death.*  
Letter from Rosenstock-Huessy to Freya van Moltke, January 1970

*Acknowledgements*  
*Preface*  
*Introducing Rosenzweig and Rosenstock-Huessy and Their “Common Life’s Work”*  
Chapter 1: *Which Spirit to Serve? The Stirring of the Living Loving God*  
Chapter 2: *The Basis of New Speech Thinking*  
Chapter 3: *Grammatical Organons in Rosenstock-Huessy and Rosenzweig*  
Chapter 4: *On God as an Indissoluble Name and an Indispensible Pole of the Real*  
Chapter 5: *The Sundered and the Whole: Rosenzweig’s Distinction between the Pagans and the Elect*  
Chapter 6: *Rosenstock-Huessy’s Incarnatory Christianity*  
Chapter 7: *The Ages of the Church and Redemption through Revolution*  
Chapter 8: *The Modern Humanistic Turn of the French Revolution in Rosenstock-Huessy*  
Chapter 9: *Beyond the Idol of the Nation: Rosenstock-Huessy in the Aftermath of the Great War*  
Chapter 10: *Beyond the Idol of the State: Rosenzweig on Hegel*  
Chapter 11: *Beyond the Idol of Art Part 1: Rosenzweig and the Role of Art in Redemption*  
Chapter 12: *Beyond the Idol of Art Part 2: Rosenstock-Huessy and Art in Service to Revolution*  
Chapter 13: *Beyond the Prophets of Modernity: Rosenzweig and Rosenstock-Huessy on Nietzsche and Marx*  
Chapter 14: *Rosenzweig on Why Allah Is Not the Loving Revealing Redeeming God*  
Chapter 15: *Rosenstock-Huessy on Islam, Confucianism, Taoism and Buddhism*  
*Conclusion: Pagan, Jew, Christian - or Three Lives in one Love*  
*Postscript*

### Other books by Wayne

**Wayne Cristaudo: A Philosophical History of Love** <sup>[A](https://www.amazon.com/Philosophical-History-Love-Wayne-Cristaudo-dp-1412846269/dp/1412846269)</sup>  
Transaction Publishers, 2012T

A Philosophical History of Love explores the importance and development of love in the Western world. Wayne Cristaudo argues that love is a materializing force, a force consisting of various distinctive qualities or spirits. He argues that we cannot understand Western civilization unless we realize that, within its philosophical and religious heritage, there is a deep and profound recognition of love’s creative and redemptive power.

Cristaudo explores philosophical love (the love of wisdom) and the love of God and neighbor. The history of the West is equally a history of phantasmic versions of love and the thwarting of love. Thus, the history of our hells may be seen as the history of love’s distortions and the repeated pseudo-victories of our preferences for the phantasms of love. Cristaudo argues that the catastrophes from our phantasmic loves threaten to extinguish us, forcing us repeatedly to open ourselves to new possibilities of love, to new spirits.

Fusing philosophy, literature, theology, psychology, and anthropology, the volume reviews major thinkers in the field, from Plato and Freud, to Pierce, Shakespeare, and Flaubert. Cristaudo explores the major themes of love of the Church, romantic love and the return of the feminine, the conflict between familial and romantic love, love in a meaningless world and the love of evil, and the evolutionary idea of love. With Cristaudo, the reader embarks on a journey not just through time, but also through the different kinds, origins, and spirits of love.

Table of Content  
1: *Introduction*  
2: *Plato and the First Philosophy of Love*  
3: *The Love of Christ*  
4: *Religion plus Philosophy plus Politics*  
5: *The Medieval Return of Venus*  
6: *The Heavenly Romance*  
7: *Love in the Family and Its Dissolution*  
8: *De Sade and the Love of Evil*  
9: *Charles Sanders Peirce and Love as Evolutionary Principle*  
10: *Conclusion*  
Index

**Wayne Cristaudo: Power, Love and Evil: Contribution to a Philosophy of the Damaged** <sup>[A](https://www.amazon.com/Power-Love-Evil-Contribution-Philosophy/dp/9042023384)</sup>  
Amsterdam/New York, NY, 2008R

Love and evil are real – they are substances of force fields which contain us as constituent parts. Of all the powers of life they are the two most pregnant with meaning, hence the most generative of what is specifically human. Love and evil stand in the closest relationship to each other: evil is both what destroys love and what forces more love out of us; it is, as Augustine astutely grasped, privative (requiring something to negate) but it is also born out of misdirected love. Breaking with naïve realist and post-modern dogmas about the nature of the real, this book provides the basis for a philosophy of generative action as it draws upon examples from philosophy, literature, religion and popular culture. While this book has a sympathetic ear for ancient and traditional narratives about the meaning of life, it offers a philosophy appropriate for our times and our crises. It is particularly directed at readers who are seeking for new ways to think about our world and self-making, and who are as dissatisfied with post-Nietzschean and post-Marxian 20th century social theory as they are by more traditional philosophical and naturalistic accounts of human being.

Table of Contents:  
*Introduction*  
Chapter 1: *Catastrophe and the Necessity of Evil*  
Chapter 2: *Sacrifice: Love’s Ultimate Demand*  
Chapter 3: *Evil and the Phantasmic*  
Chapter 4: *Damage: A Logic of Evil*  
Chapter 5: *Denial and Elimination of Evil and Evil’s Elimination of the Subject in Denial*  
Chapter 6: *Truth and Faith, or Forms and Signs of Life’s Power*  
Chapter 7: *Love and the Limits of Justice*  
Chapter 8: *Alchemising Evil*  
Endnotes  
Index

### Books edited by Wayne

**Revolutions: Finished and Unfinished, From Primal to Final** <sup>[A](https://www.amazon.com/Revolutions-Finished-Unfinished-Primal-Final/dp/1443840394)</sup>  
Paul Caringella (Editor), Wayne Cristaudo (Editor), Glenn Hughes (Editor)  
Cambridge Scholars Publishing, September 1, 2012 C

Revolutions: Finished and Unfinished, from Primal to Final is an important philosophical contribution to the study of revolution. It not only makes new contributions to the study of particular revolutions, but to developing a philosophy of revolution itself. Many of the contributors have been inspired by the philosophical approaches of Eric Voegelin or Eugen Rosenstock-Huessy, and the tension between these two social philosophies adds to the philosophical uniqueness and richness of the work.

TABLE OF CONTENTS

*Preface by Wayne Cristaudo*  
*Introduction by Wayne Cristaudo*  
Chapter 1: *Revolution as a Political Concept by Eugen Rosenstock-Huessy*  
Chapter 2: *Revolutions: Progress or Decline? by Thomas J. McPartland*  
Chapter 3: *The Primal Revolution: Original and Unfinished by Louis Herman*  
Chapter 4: *Modernity as the Immanentization of the Eschaton: A Critical Re-evaluation of Eric Voegelin’s Gnosis-thesis by Manfed Riedl*  
Chapter 5: *A Disturbance in Being: The Idea of Revolution in History by Thomas A. Hollweck*  
Chapter 6: *On the Ruins of Civilizations: The Regimes of Terror by Manfred Henningsen*  
Chapter 7: *“England: A Parliamentary Church” and “The European Significance of the Glorious Revolution” – Selections from Out of Revolution by Eugen Rosenstock-Huessy*  
Chapter 8: *A “Half-Revolution” or a Revolution Finally Completed? Reformed Protestant Theology’s Fulfillment in the American Revolution by Glenn A. Moots*  
Chapter 9: *Arendt and Rosenstock-Huessy on The French Revolution by Wayne Cristaudo*  
Chapter 10: *Salvation Without Individuals: The Bolshevik Revolution by Glenn Hughes*  
Chapter 11: *Loot the Looters: Out of Revolutions With or Without Wealth, Health, Knowledge, Liberty, and Justice—Generalizations from the Russian Revolution and Applications of Generalities to Russia by Michael S. Bernstam*  
Chapter 12: *Development with Chinese Characteristics: Asia’s Sinic Revolutions in Global Historical Perspective by William Ratliff*  
Chapter 13: *Revolution or Redemption? The Middle East by Arie Amaya-Akkermans*  
Chapter 14: *The Worst Revolution of All? Managerialism and the “Body without Ears” by Christopher Hutton*  
Chapter 15: *Transhumanism: The Final Revolution by Klaus Vondung*  
Appendix  
Contributors  
Index

**Love in the Religions of the World** <sup>[A](https://www.amazon.com/Love-Religions-World-Wayne-Cristaudo/dp/1443835048)</sup>     
Editor: Wayne Cristaudo and Gregory Kaplan  
Cambridge Scholars Publishing, Jan 2012 C

The study of comparative religion is no longer a matter merely for those interested in religion – it is a matter of concern for everybody. For irrespective of whether one believes in God, religion is a major characteristic of identity. And in the post 9/11 world, every educated person is aware of how important it is to understand what others believe. This collection of essays by international scholars emerged from an intense and powerful dialogue at the University of Hong Kong about love in the major religions of the world. Eschewing the comforting, but ultimately erroneous and dangerous idea that all religions believe more or less the same thing, each essay examines the role and nature of love in a major religion of the world. It is an invaluable guide for students, teachers and the general reader wanting to cut through the morass of doctrinal differences and emphases in the world’s religions. It also makes an important contribution to the urgent issue of dialogue amongst faiths and cultures.

Preface  
Acknowledgments  
Introduction: *Love and Religion in the Secular World by Wayne Cristaudo*  
Chapter One: *Notions of Love in the Hindu Tradition by Hillary Rodrigues*  
Chapter Two: *Aspects of love in Yogic Philosophy by Janusz Sysak*  
Chapter Three: *Buddhism‘s View of Love by Weilin Fang and Guozhu Sun*  
Chapter Four: *Love and Fear in Judaism by David P. Goldman*  
Chapter Five: *Love in Some Jewish Philosophies by Gregory Kaplan*  
Chapter Six: *Flame of Yah: Divine Eros in the Song of Songs by Peter J. Leithart*  
Chapter Seven: *Love in the Christian Period by Matthew del Nevo*  
Chapter Eight: *Love in Islam: Dimensions, Manifestations and Guidance by Kaseh Abu Bakar*  
Chapter Nine: *Love meets Love: Christian-Muslim Dialogue on Love by Mega Hidayati*  
Chapter Ten: *Ethics of Loving Others as Found in Ruist ("Confucian") and Christian Traditions by Lauren Pfister*  
Chapter Eleven: *The Unity of Corporeality and Spirituality: Love in Daoism by Ellen Y. Zhang*  
Chapter Twelve: *Piety or Love: A Comparative Culture Approach on Ruth and Yong Quan Yue Li by Wenjuan Yin*  
Contributors

**From Faith in Reason to Reason in Faith: Transformations in Philosophical Theology from the Eighteenth to Twentieth Centuries** <sup>[A](https://www.amazon.com/Faith-Reason-Transformations-Philosophical-Eighteenth/dp/0761854908)</sup>    
Wayne Cristaudo (Editor), Heung-wah Wong (Editor)  
University Press of America, 2011 U

If the philosophers of the Enlightenment had hoped to establish, once and for all, that reason is the primary source of human orientation, twentieth century philosophy has demonstrated all too clearly that reason is far from having clear boundaries. In this respect, Immanuel Kant’s contemporaries and critics, Johann Georg Hamann and Friedrich Heinrich Jacobi, look surprisingly modern. Faith is now increasingly recognized as intrinsic to social identity and thus no more capable of taking a permanently subordinate role to reason—whatever that may be—than reason is capable of an existence free from social embodiment. This collection of thirteen essays focuses upon major philosophical and theological debates from the past three hundred years. Written by leading international scholars, this remarkable text takes the reader through major transitions in the modern understanding of faith and reason. It thus provides an invaluable guide to the history of modern philosophical theology whilst informing readers why the relationship between faith and reason remains an issue of major social and philosophical importance.

*Foreword by Heung-wah Wong*  
*Introduction by Wayne Cristaudo*  
1: *Faith and Reflexivity: Reflections on Language and the “Semiotic Turn” by Christopher Hutton*  
2: *The Destructive Potential of the God of Reason (Reimarus) by Englehard Weigl*  
3: *The Material God in Diderot’s D’Alembert’s Dream by Miran Bozovic*  
4: *Anthropologist of Enlightenment: Purity, Pollution, and Forbidden Mixtures in Hamann’s Metacriticism by Peter J. Leithart*  
5: *Hegel on Kant, Fichte, Jacobi: Being Reasonable about Faith and Knowledge by Wayne Cristaudo*  
6: *Beyond Paradox: Faith and Reason in the Thought of Søren Kierkegaard by Murray Rae*  
7: *Nature, Nurture and Nietzsche’s Faith in Life by Nalin Ranasinghe*
8: *Reason and Faith: A Comparison of Immanuel Kant and Albert Schweitzer by Predrag Cicovacki*  
9: *Faith and Reason: Shestov and Gilson by Mathew Del Nevo*  
10: *Karl Barth: Reason Beyond Autonomy? by Phillip Tolliday*  
11: *Consciousness and Transcendence: Voegelin and Lonergan on the Reasonableness of Faith by Glenn Hughes*  
12: *Reason and Violence in Girard’s Mimetic Theory: The Anthropology of the Cross by Robert Hamerton-Kelly*  
13: *The Spirit Has Reasons That Rationalists Cannot Fathom: The Emergence of Christian Dao-ology in Late Twentieth Century China by Lauren Pfister*  
Index  
Contributors

**Augustine: His Legacy and Revelance** <sup>[G](https://books.google.nl/books/about/St_Augustine.html?id=YgwsAQAAMAAJ)</sup>  
Wayne Cristaudo (Editor), Heung-Wah Wong (Editor)  
University of Hong Kong European Studies in Philosophical Theology  
ATF Press, 2010

If the defining feature of the Middle Ages is it churches, the defining architect of its mind, heart and soul- at least until Aquinas - is St Augustine. The Church was the spiritual army whose leaders were its fathers. And in that sense, Augustine's thought is closer in modern terms to a revolutionary like Lenin than it is to a philosopher's. A philosopher may well be part of a broad movment, but his appeal is usually to first principles rather than to a body of faith, even if once philsophies are entrenched, very questionable first principles easily become matters of faith, and the collection of a philosophers' members form a kind of church. This collection examines Augustine's core ideas and brings scholars from the USA, EU, Australia and Asia.

**The Cross and the Star: The Post-Nietzschean Christian and Jewish Thought of Eugen Rosenstock-Huessy and Franz Rosenzweig** <sup>[A](https://www.amazon.com/Cross-Star-Post-Nietzschean-Rosenstock-Huessy-Rosenzweig/dp/1443810118/)</sup>
Wayne Cristaudo (Author, Editor), Frances Huessy (Editor) \
Cambridge Scholars Publishing, 2009C

Eugen Rosenstock-Huessy, a Christian convert and a social philosophy scholar, had an intense conversation with the Jewish thinker Franz Rosenzweig in 1913. This “Leipzig Conversation” shattered Rosenzweig’s understanding of the meaning of religion, but it also propelled him to embrace his innate Jewish faith. Three years later, they engaged in a correspondence that has emerged as an historic, stunning dialogue on Jewish-Christian thinking. Rosenzweig went on to write The Star of Redemption, a classic work of modern Jewish philosophical theology and to become one of the most important and influential figures of twentieth-century German Jewry. Rosenstock-Huessy took a different path—writing his Sociology, which pointed the social sciences in a new direction based on speech-thinking, and an enormous, rich body of work covering grammar and society, revolutions, Church history, and industrial law; teaching generations of European and American university students; and putting his faith into action. This is the first major collection of essays on these two close friends’ “new thinking.” Their dialogue mirrored Nietzsche’s anti-transcendent reading of Judaism and Christianity, as well as his attack on idealism. But their dialogue also resurrected the redemptive cores of these faiths as sources for the rejuvenation of human society.

This book brings to publication three essays by Rosenstock-Huessy on Nietzsche, and a translation of a chapter from his Sociology, clarifying the post-Nietzschean approach of the “new thinking.” The Cross and the Star, a 50-year span of significant scholarship, vivifies the reasons for Rosenzweig’s and Rosenstock-Huessy’s influence on faith and society, and why their respective thought speaks directly and enduringly to the global human challenges of our time.

Preface  
Acknowledgements

Part I – *Rosenstock-Huessy’s Engagements with Nietzsche*  
Chapter One: *The End of the World or, When Theology Slept* (1941) by *Eugen Rosenstock-Huessy*  
Chapter Two: *Hölderlin and Nietzsche* (1941) by *Eugen Rosenstock-Huessy*  
Chapter Three: *Nietzsche’s Untimeliness* (ca 1942) by *Eugen Rosenstock-Huessy*  
Chapter Four: *The Perils of Intellectual Spaces* (1956) by *Eugen Rosenstock-Huessy*  

Part II – *Rosenzweig and Rosenstock-Huessy: The Star and the Cross—Affinities and Differences*
Chapter Five: *From the Star of Redemption to the Cross of Reality* (1959) by *Georg Müller*  
Chapter Six: *The Discovery of the New Thinking* (1987) by *Wolfgang Ullmann*  
Chapter Seven: *Franz Rosenzweig’s Letters to Margrit Rosenstock-Huessy, 1917-1922* (1989) by *Harold M Stahmer*  
Chapter Eight: *The Great Gift—The Impact of Franz Rosenzweig’s Jewishness on Eugen Rosenstock-Huessy* by *Wayne Cristaudo*  
Chapter Nine: *Orate Thinker Versus Literate Thinker* by *Michael Gormann-Thelen*  
Chapter Ten: *Sovereignty and Sacrifice in Writings by Eugen Rosenstock-Huessy and Franz Rosenzweig* by *Gregory Kaplan*  
Chapter Eleven: *'The Stubbornness of the Jews* by *Robert Erlewine*

Part III – *Liturgical Speech and Deed: Church, History, and Education*  
Chapter Twelve: *Speech Is the Body of the Spirit* (1987) by *Harold M Stahmer*  
Chapter Thirteen: *Grammar on the Cross* by *Peter J Leithart*  
Chapter Fourteen: *Goethe, the First Father of the Third Age of the Church* by *Matthew del Nevo*  
Chapter Fifteen: *Rosenstock-Huessy’s Anti-transcendent Critique of Karl Barth* by *Wayne Cristaudo*  
Chapter Sixteen: *Rosenstock-Huessy and Liturgical Thinking* by *Donald E Pease*  
Chapter Seventeen: *From Here to Eternity* by *Michael Ermarth*  
Chapter Eighteen: *Education in the Shadow of Camp William James* by *Claire Katz*  
Chapter Nineteen: *Christian, Muslim, Jew* (2007) by *Spengler*  
Notes  
Index

**Messianism, Apocalypse, Redemption: 20th Century German Philosophy** <sup>[A](https://www.amazon.com/Messianism-Apocalypse-Redemption-Century-Thought/dp/1920691405)</sup>   
edited by Wayne Cristaudo and Wendy Baker \
Australian Theological Forum, 2006

At the beginning of the twentieth century the tropes of messianism, apocalypse and redemption, which had been so central to the West's religious formation, seemed spent forces in Germany. Nietzsche had pronounced God as dead and theology seemed to be travelling the same secular route as philosophy. But World War I changed that. This book introduces some of Germany's key thinkers in theology, philosophy, literature and social and political thought through their engagement with these previously discarded concepts. They initiated a new and urgent dialogue between philosophy and theology. This imaginative and innovative collection brings together essays by established scholars on Messiamism, Redemption and Apocalypse in twentieth century German thought. Major theologians such as Barth, Buber, Bonhoeffer, Rahner, Pannenberg and Moltmann are discussed alongside leading intellectuals such as Adorno, Benjamin, Bloch, Heiddeger and Rosenzweig. Literary figures, such as Kafka and George, are also included. The interfaces imply a different way of reading theology and challenge the reader to think what the implications of immanence in a specific philosophical culture are for the theological project. Some of the essays introduce thinkers who are little known to English speaking readers. Others cast new light on more familiar figures. The collection as a whole contextualises German religious and philosophical thought on these crucial topics in very useful ways. The dialogue at work in these pages is a very important one and should be carried further.

Chapter: *Revolution and the Redeeming of the World: Eugen Rosenstock-Huessy's Messianic Reading of History* by *Wayne Cristaudo*  
Chapter: *Redemption and Messianism in Franz Rosenzweig’s "The Star of Redemption"* by *Wayne Cristaudo*

**From Great Ideas in the Western Literary Canon** <sup>[A](https://www.amazon.com/Great-Ideas-Western-Literary-Canon/dp/0761823964)</sup>    
by Wayne Cristaudo and Peter Poiana  
University Press of America, 2003 U

This book examines 'great ideas'- the term used generically to refer to the deep-seated anxieties that art, religion and philosophy all seek to address- in relation to a selection of great literary texts. The texts chosen are those that remain, often centuries after their appearance, beacons of illumination and wisdom. The twelve chapters of this book each deal with one great text and the central idea that propels it. The ideas are examined as events possessed of their own field of resonance, and it is by tracing them in their narrative, dramatic or lyrical development that one can appreciate how these great texts speak as powerfully as they do to generations of readers.

Chapter 1 *Preface*
Chapter 2 *Acknowledgments and Permissions*
Chapter 3 *The Tragic Affirmation of Rage in Homer's The Iliad*
Chapter 4 *The Religion of Fear in Sophocles' Oedipus the King*
Chapter 5 *The Power of Love in Dante's The Divine Comedy*
Chapter 6 *Rabelais' Vitalism OR Feasting, Flagons, Fornicating, Fighting, Fertility, Farting, Fun and Freedom from Fear and Fools in Gargantua and Pantagruel*  
Chapter 7 *Truth and Persuasion in Cervantes' Don Quixote*  
Chapter 8 *Wisdom and Mastery in Shakespeare's The Tempest*  
Chapter 9 *Will, Pride and Enslavement in Milton's Paradise Lost*  
Chapter 10 *Striving in Goethe's Faust*  
Chapter 11 *Ennui in Baudelaire's The Flowers of Evil*  
Chapter 12 *Parricide and Deicide in Dostoyevsky's The Brothers Karamazov*  
Chapter 13 *The Monument of Time in Proust's Swann's Way*  
Chapter 14 *The Anxiety of Origins and the Trials of Filiation in Joyce's Ulysses*  
Chapter 15 *Bibliography*  
Chapter 16 Index

**This Great Beast: Progress & the Modern State** <sup>[A](https://www.amazon.com/This-Great-Beast-Progress-Routledge-dp-1138365165/dp/1138365165)</sup>    
Robert Catley (Author), Wayne Cristaudo (Author)  
Ashgate Publishing, 1997

This text uses political science, historical and philosophical methods to explain why the 200 states that exist at the end of the 20th century assume the different characteristics they exhibit: "modern / civilized", wherein the organic theory of the state is revived; "developmental", using growth and policy criteria; "idealist", including Marxist and theocratic, wherein the origins and development of idealist thought is expounded; and "primitive", which comprises the least developed of states and explores the reasons for their lack of development. This book seeks to connect the development of political philosophy with the creation of different kinds of state. It concludes with an argument in defence of liberal democracy - which nonetheless presents the liberal democratic states - and a critique of the conventions of the various forms of "critical theory". The book was conceived in the realization that the end of the Cold War necessitates a thorough re-evaluation of idealist theories.

This Great Beast: Progress and the Modern State is a provocative and ambitious contribution to state theory. Taking issue with the radical democratic tradition which now dominates contemporary theorization of the state, Catley and Cristaudo argue that the mature civilized state is the most benign and most responsive political form that has hitherto evolved. In a reworking of the tradition of Montesquieu, Tocqueville, and Alexander Hamilton for the contemporary world, they argue that the potency of the modern mature civilized state stems from the different powers (often illiberal and undemocratic) that are intrinsic to its evolution. In making their argument they not only provide a typology of state forms, and an investigation of the major contributors to state theory, they also survey the trajectory of the state from its formation until the break down of social democracy and communism. In the area of international relations, they argue that the zone of mature civilized states allows for conduct in compliance with the rule of law, but outside of that zone more primitive energies prevail.
Other writings by Wayne

The Truth and Divinity of Sickness and Rage in the Karaoke of Despair article  
The Journal of Religion and Popular Culture, 2003

[Eugen Rosenstock-Huessy](https://plato.stanford.edu/entries/rosenstock-huessy/)    
The Stanford Encylopedia of Philosophy

["Love is as Strong as Death": The Triadic Love of Franz Rosenzweig, Eugen Rosenstock-Huessy and Gritli Rosenstock-Huessy]({{ 'assets/downloads/cristaudo_triadic_love_rosentsock_rosenzweig.pdf' | relative_url }})
