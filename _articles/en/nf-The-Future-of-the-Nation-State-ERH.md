---
title: "Norman Fiering: Future of the nation-state & ERH"
category: reception
language: en
page-nr: 312
---
I sent out last week, as an attachment, some thoughts about Rosenstock-Huessy and the future of the nation-state, in response to a question by Michael Wells. I should have mentioned that I know of two recent journal articles on this subject, in English, although I did not draw on them. The key word in both is "transnational."

* First is Kees van der Pijl, *A Theory of Transnational Revolution: Universal History according to Eugen Rosenstock-Huessy and Its Implications*, Review of International Political Economy, III, no. 2 (1996), pp. 287-318.
* The second, which takes issue with Van der Pijl, is: Knut Martin Stünkel, *Nations as Times: The National Construction of Political Space in the Planetary History of Eugen Rosenstock-Huessy*, in Mathias Albert, Gesa Blum, Jan Helmig, Andreas Leutzsch, et al., Transnational Political Spaces (Frankfurt/New York: Campus Verlag, 2009), pp. 297-317.

Prof. Leutzsch kindly sent me a copy of Stünkel's article some months ago.

At the annual meeting of the American Political Science Association, which will be held Sept. 2-5, 2010, in Washington, D. C., several papers, including one by Professor Leutzsch, will be presented on Rosenstock-Huessy's philosophy of history, at a session devoted to a comparison between Rosenstock-Huessy and Eric Voegelin.

ERH began his career as a conventional academic historian, and although he abandoned that path, i. e. the "conventional academic" part of it, all of his life he approached almost every subject from a historical point of view and had passionate convictions about the creation and shape of history and the proper role of the historian.

Another recent essay that addresses the future of the planet according to ERH is Michael Ermarth's scintillating *From Here to Eternity: The Philosophy of History of Eugen Rosenstock-Huessy as Eschatology on the Transmodern Installment Plan*, which was first presented at the conference on Rosenstock and Rosenzweig at Dartmouth College in July 2008, and is published in *The Cross and the Star: The Post-Nietzschean Christian and Jewish Thought of Eugen Rosenstock-Huessy and Franz Rosenzweig*, ed. Wayne Cristaudo and Frances Huessy (Newcastle upon Tyne, 2009).

Ermarth's opening description of ERH captures the man adeptly:  
*"As many others in his cohort of German and European emigré thinkers (including most anti-Nazi refugees in the United States), Eugen Rosenstock-Huessy strived to advance a whole new order in the world inaugurated by a radical shift of attitude or mindset among its inhabitants. He was an unabashed futurist, but of a very special poly-historical, pan-traditional bent. He remained a fervent if selective defender of many Western traditions; but he was also a candidly eschatological, rather than liberal progressive or secular utopian, thinker. He did not think small or merely mid-range; nor did he think mildly, moderately, or accommodatingly toward most modern suppositions and standpoints. In contrast to our current era, he did not favor scaled-down, surrogate micro-messianisms; comfort-zone niche numinosities; or tentative epiphanies. Nor did he think that that the current modality of Western modernity was bound to continue as the culmination or end of history. Viewed from an eschatological standpoint of absolute faith beyond reason, he believed history had other ends in store beyond what is called modernity. There would be no eternity for modernity, as it too was destined to be supplanted by its own end."*
