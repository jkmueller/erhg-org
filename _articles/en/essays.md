---
title: Overview of Essays
category: reception
language: en
page-nr: 330
---


Essays by Norman Fiering:
* [The Miracle of Speech: A Comment on ERH]({{ 'nf-the-miracle-of-speech' | relative_url }})
* [ERH on Universal History]({{ 'nf-erh-on-universal-history' | relative_url }})
* [Future of the nation-state & ERH]({{ 'nf-the-future-of-the-nation-state-erh' | relative_url }})
* [Anthropurgy]({{ 'nf-anthropurgy' | relative_url }})
* [Andragogy]({{ 'nf-andragogy' | relative_url }})
* [Revolution and the Advancement of Mankind in the Second and Third Millennia](http://www.erhsociety.org/wp-content/uploads/2018/12/revolution-and-the-advancement-of-mankind-in-the-second-and-third-millennia.pdf)
* [Marriage]({{ 'nf-marriage' | relative_url }})
* [Rosenstock-Huessy in the classroom]({{ 'nf-erh-in-the-classroom' | relative_url }})

and by Bill Cane:
* [IN THE SPIRIT OF EUGEN ROSENSTOCK-HUESSY]({{ 'bc-in-the-spirit-of-erh' | relative_url }})

and by [Peter Leithart](https://www.firstthings.com/featured-author/peter-j-leithart):
* [The Relevance of Eugen Rosenstock-Huessy](https://www.firstthings.com/web-exclusives/2007/06/the-relevance-of-eugen-rosenst)
* [Why study Rosenstock-Huessy](https://www.patheos.com/blogs/leithart/2007/01/why-study-rosenstock-huessy/)
* [Speech on the Cross](https://www.patheos.com/blogs/leithart/2017/10/speech-on-the-cross/)
* [The Politics of Emma's Hand](https://www.firstthings.com/article/1995/03/the-politics-of-emmas-hand)
* Tribalism
* [Jews and Gentiles](https://www.patheos.com/blogs/leithart/2007/01/jews-and-gentiles/)
* [Metabolism of Science](https://www.patheos.com/blogs/leithart/2007/01/metabolism-of-science/) and its addendum [World, Nature, Physis](https://theopolisinstitute.com/leithart_post/world-nature-physis/)
* [The Cross of Eugen Rosenstock-Huessy](https://theopolisinstitute.com/leithart_post/cross-eugen-rosenstock-huessy/)
* [The Soul of Eugen Rosenstock-Huessy](https://www.patheos.com/blogs/leithart/2015/10/the-soul-of-rosenstock-huessy/)
* [Fathers and Sons](https://www.firstthings.com/web-exclusives/2010/02/fathers-and-sons)

and by Wayne Cristaudo:  
* [Eugen Rosenstock-Huessy](https://plato.stanford.edu/entries/rosenstock-huessy/)

and by Wim Leenman and Bob O'Brian  
* [Founding Planetary Posts]({{ 'wl-bo-planetary-posts' | relative_url }})

and Otto Kroesen:
* Otto Kroesen: [Fourfold Responsibility in an Engineering Course on Ethics](http://www.erhfund.org/wp-content/uploads/2002-KROESEN-Fourfold-Responsibility-Engineering-Course-Et….pdf), in ‘Planetary Articulation: The Life, Thought, and Influence of Eugen Rosenstock-Huessy’, Conference Proceeedings, Millikin University, Decatur, Illinois USA, June 2002, pp. 81-90
* Otto Kroesen: [From thou to IT: information technology from the perspective of the language philosophy of Rosenzweig and Rosenstock-Huessy](https://www.researchgate.net/publication/262013775_From_thou_to_IT_information_technology_from_the_perspective_of_the_language_philosophy_of_Rosenzweig_and_Rosenstock-Huessy), in U Görman, WB Drees, & H Meisinger (Eds.), Creative creatures: values and ethical issues in theology, science and technology (Issues in science and theology) (pp. 53-63). London - New York: T & T Clark International
* Otto Kroesen: [From empire to globalization and oecumene](http://koed.hu/sw250/otto.pdf), Student World: Ecumenical Review World Student Christian Federation, 26-34, 2006
* Otto Kroesen & Wim Ravesteijn: [Priests of Positivism: Engineers and the Legacy of the Great European Revolutions in a Globalizing World, in Locating engineers: Education, Knowlegde , Desire](https://www.researchgate.net/publication/229037974_Priests_of_positivism_engineers_and_the_legacy_of_the_great_European_revolutions_in_a_globalizing_world) (pp. 1-14). Blacksburg: Virginiatech, 2006
* Otto Kroesen & Wim Ravesteijn: [Between Spengler and Rosenstock-Huessy: twofold or threefold thinking within a fourfold reference framework](https://www.researchgate.net/publication/270687020_Between_Spengler_and_Rosenstock-Huessy_twofold_or_threefold_thinking_within_a_fourfold_reference_framework), in G Bluhm (Ed.), The communicatioive construction of transnational political spaces and times (pp. 1-13), 2007

and others:
* Andrew Russ: Four Diseases of Social Speech in “Hamlet”, May 2010
* Mike McDuffee: [An Introduction to the Christian Thought of Eugen Rosenstock-Huessy: The Strange Catechism of the Christian Future](https://www.yumpu.com/en/document/read/18324990/an-introduction-to-the-christian-thought-of-eugen-rosenstock-huessy), Evangelical Theological Society, November 2004
