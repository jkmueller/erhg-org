---
title: The Rosenstock-Huessy Huis
category: reception
language: en
page-nr: 400
---
The [Rosenstock-Huessy Huis](https://www.rosenstock-huessy-huis.nl/geschiedenis/) in Haarlem was founded in 1972 and was  community till the 1990s.

Different articles have been published about it:
- [Feico Houweling: How does our life bear fruit?](https://www.feico-houweling.nl/how-does-our-life-bear-fruit-the-rosenstock-huessy-huis-in-haarlem/)
- [Interview with Bob O’Brien: “Today we have to look further and wider”](https://www.feico-houweling.nl/today-we-have-to-look-further-and-wider/)
