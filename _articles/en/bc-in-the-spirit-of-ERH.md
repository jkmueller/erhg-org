---
title: "Bill Cane: In the Spirit of Eugen Rosenstock-Huessy"
category: reception
language: en
page-nr: 317
---


Eugen Rosenstock-Huessy, the historian, philosopher and social thinker, had an incredible impact on his students. Rabbi Marshall Meyer, who lived under death threat for nine years in Argentina, claims that he became a rabbi because of Eugen's influence. (Eugen introduced him to Martin Buber). Page Smith, the celebrated American historian, pointed to Eugen as his intellectual mentor. Howard Berman, an international lawyer, was told by Eugen that by the time the lawyers get hold of things the life and spirit have already gone out of them. (Yet he set Berman on the path of his legal career). Frank Davidson of MIT , whose field is large-scale technology, acknowledged his debt to Eugen. Bob O'Brien, a former Vermont legislator, now spends all of his time coordinating efforts at volntary planetary service, because Eugen taught him that planetary service is the most important imperative for the third millenuum!

Many of Eugen's students were part of Camp William James, an experiment in which Dartmouth students went out to help farmers in New England. This camp, which got the attention of then President Franklin Delano Roosevelt, is often cited as the precursor of the Peace Corps.

In Amsterdam, there is a center named after Rosenstock-Huessy. In Germany, there is a Rosenstock-Huessy Society which sponsors an annual international conference. Rosenstock-Huessy firmly believed that education meant not simply instructing, but inspiring -- opening out new possibilities for people and givng them hope.

As a general malaise and sense of futility settle more and more over our own society, we felt it would be worth-while to listen to what Rosenstock-Huessy might have to say to us in our current difficulties. The answers to the following questions draw heavily from the writings of Rosenstock-Huessy; they are not, of course, his answers, but they are in his spirit and in the spirit of those of us whom he has influenced.

What do we do when the trends and the polls and the priorities of our society go against us?

Trends, projections, polls, and statistics never cause the major changes in history. Major changes come about because people passionately believe in something. Those who go against the trends and give their lives to what they passionately believe in are the ones who change history!

But the trends seem overwhelming. Smugness, a sense of complacency, and an unwillingness to face what is happening seem to have taken over.

It takes thirty years for any major social change to become established in a society. You can't work at something for a few years, and then say : it didn't work! Peace and justice are the work of generations, and in difficult times, what is important is to keep the spriit alive and pass it on to others.

Some people have the feeling that we are going through a period similar to what happened in pre-Nazi Germany. There is a similar reliance on propaganda, on slogans, on American superiority, on continuing buildups for "defense."

Even in Nazi Germany, there were those who resisted the propaganda, the militarization, the racial superiority. Those who stand up for justice and peace and dialogue represent the presence of God in history. That presence may at times seem fragile, but it is most important. Germany was able to rebuild a society because of the resistance. So people who work for justice and peace and who bear witness to the truth must not give up. Because if they do, the presence of God ceases to exist in a society, and the possibilities for rebuilding can be cut off forever.

The major institutions of our society -- the corporations, government, the universities -- seem to be intent on making money, improving their own situation. What can we do when the major forces in our society seem inert?

Institutions preserve the steps human beings have already taken. That is what they are for and that is what they are good at. But don't expect them to lead us into the future. Only conscious and conscienced and spirited human beings can do that. The great changes in history have come from writers, artists, students, workers, movements of people, communities of faith -- and sometimes even politicians!

But it's lonely trying to bring about change when the major institutions resist it.

Some people always have to come too early so that other people can arrive on time. There will always have to be people who begin something before its time has come; people who begin to make a story happen before that story has become history. It doesn't take faith to go along with what you see all about you. What takes faith is getting involved in a new story which hasn't happened yet, which has not yet become history!

Isn't there some way to get the world together, to work out peace and justice for all, some master program which will get around this constant chipping away at things?

Nothing happens everywhere all at once. Look at the Soviet Union, at Poland and the eastern bloc countries. Look at the movements of the poor majorities in Latin American and the thirst for democracy there. Look at the change in South Africa. Look at the Palestinians. These are all significant struggles, but such struggles keep recurring in history. The great revolutions come about because societies begin to rot and corrode. Revolution is very close to hell. But it is very close to heaven also, because it opens out the possibility of new life in societies that were already brain-dead and rotting with corruption. There is no movement that is going to solve everything once and for all. "Everything good has to be done over and over again forever!"

Although he was an exceptional scholar, a great and thorough German academician, Rosenstock spoke more from the depths of his own experience than from book learning. His experience as a German soldier in the First World War influenced his entire life and his attitude toward history. He recalled a Christmas holiday when there was a cease-fire, and the German and French soldiers left their trenches and began talking to each other. When the respite ended, the commanding officers had to threaten their own soldiers to get them shooting at each other once again. Huessy remained convinced throughout his life that talking to each other is of absolute importance, and that war begins when dialogue ends.

He recalled how mortified he was when the German soldiers pillaged a little drug store in a French village and smashed practically everything in the shop. He left his personal apology for the shopkeeper to find, and when the war was ended, one of the first letters he received was from that French shopkeeper.

The experience of the Great War was the end of a world for Rosenstock. He often said that we must believe in the end of the world, because it will come at least once during our lifetime, and many times in the course of history. We must believe in the end of the world and we must also believe in the world to come. Those of us who have had a world end because of failure or alcoholism or divorce or death or tragedy can well understand what the end of the world means, and the difficulty of taking steps into a new and different world. Historically, Rosenstock saw the end of the world come. But he also saw people who had faith and hope and love and imagination which enabled them to envision a new and different world on the other side of the world they saw coming to an end. And he credited them with saving us all from ruin.

When he returned to Germany after the war, Rosenstock experienced what he later saw as the turning point of his life. Within the space of two weeks, he was offered three prestigious positions: a chair at the university, a position in one of the Ministries of the government, and a job as head of a major religious publishing house. He agonized over the decision he had to make, and then unexpectedly, he turned down all three offers! It was years before Hitler would come to power, yet Rosenstock already recognized the smell of death in the society. He could not become part of government or church or academia. He had to reject their "dead works" in order to "serve the living God".

Turning his back on the major institutions of the society was for him the beginning of a new life in the spirit. "No social space or field exists outside the powers that be and the existing institutions are all that there is at the moment of one's 'metanoia' (change of mind and heart), of one's giving up their dead works." One is called to enter a new existence which does not yet exist!

"The words make no sense", he later wrote, "the atmosphere is stifled. One chokes. One has no choice but to leave. But one does not know what is going to happen, one has no blueprint for action. The 'decision' literally means... being cut off from one's own routines in a paid and honored position. And the trust that this sub- zero situation is bound to create new ways of life is our faith."

"I probably did not advance much in personal virtue by this about-face toward the future, away from any visible institution. I did not become a saint. All I received was life. From then on, I did not have to way anything which did not originate in my heart."

Eugen's personal revolution against the deadliness of pre-Hitler Germany helped him understand historical revolutions. The great revolutions, he believed, came when societies had reached a dead-end, when they were crushing the spirit and life of the people. Hence, revolution, which is very close to hell, is "very close to heaven also", because it opens out new life and new possibilities in an atmosphere of death and decay.

Some people sense the death and decay early on. They are the ones who "come too early " so that others will be able to arrive "on time". They carry within themselves a different future for all of us. They are the people who can imagine "the world to come" and who begin to pursue a new vision, begin to live out a new story even before that story becomes history.

Rosenstock had little use for trends, polls, statistics or economic projections. Real history, he believed, happens when a people go against the trends and begin to live out what they passionately believe in. "Our passions give life to the world", he wrote, "and our collective passions constitute the history of mankind.." That is why he called his major work in English "Out of Revolution". He fully realized that death comes to historical institutions, and when that happens, the only hope is the end of the old and the beginning of the new.

The great revolutions are in our blood stream. The papal revolution, which brought the emporer to his knees before the pope, dramatized that there are higher laws than those of empire; that spiritual values have power in our world. Rosenstock traced the birth of the western idea of freedom to the beginnings of church-state tension. People were no longer stuck with one authority only; they were able to play the pope against the emperor and so escape the tyranny of one absolute ruler!

The Protestant revolution opened up freedom of conscience and religion. The French revolution the value of the individual; the scientific revolution freedom of thought and of scientific progress; the Russian revolution the rights of the worker. Indeed, there would have been no labor movement in the United States had not the Russian revolution occurred.

We tend to deny our revolutions even though they run through our blood; we claim to be Catholics or Protestants or freethinkers or conservatives or liberals or Russians or Americans. Yet we are all the product of a common heritage of revolutions. And until we realize that we have a common past, we can never build a common future.

The flags that are being waved at us today cannot command our allegiance. Rosenstock saw this thirty-five years ago. In Planetary Service, one of his last works, he mused that we may well have to hoist our own flags in this present era, when the flags of nations and ideologies and religions do not express hope for a common future and a just and peaceful world. We may have to become pirates, he conjectured, hoisting our own flags in dangerous and hostile waters. But of one thing he was sure, the future belonged to planetary service. We must, as individuals, cross the boundaries that divide us and experience in simple work together the bonds that unite us.

He saw in his own native Germany the terror of Hitler, and the apparent weakness of the resistance. Yet this fragile resistance gave Germany a basis on which to build a future after Hitler, so it was precious and to be nurtured and believed in. The prophets of Israel taught him this-- that no matter how few there are who speak out for justice and humanity, they are always precious, because they are passing on the spirit that is humanity's hope.

Bill Cane,  
Jun 30 2011
