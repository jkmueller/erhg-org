---
title: Uncentered World
category: online-text
language: en
page-nr: 700
---
![Uncentered World]({{ 'assets/images/bipolar_world.jpg' | relative_url }}){:.img-right.img-xlarge}
![Uncentered World]({{ 'assets/images/bipolar_world2.jpg' | relative_url }}){:.img-left.img-xlarge}

Back Endpaper Map, The World Adjudicated to Nobody: No Nation or Continent Is in the Centre. The map is given twice. A bipolar, transverse, elliptical, equal-area map. Like a Mercator projection, this map distorts forms; unlike a Mercator map, it rep-resents areas exactly, and shows all the connections possible across the poles. The first map of this type was drawn, perhaps in an un-impressive technique, by Sir C. F. Close in Great Britain's Ord-nance Survey, Professional Papers, New Series, volume II, 1927. This seems to be omitted in the otherwise exhaustive study by C. H. Deetz and Oscar S. Adams, Elements of Map Projection, U. S. Department of Commerce, Coast and Geodetic Survey, Spe-cial Publication No. 68 (Fourth edition revised April 2, 1934). See also C. H. Deetz, Cartography (Special Publication No. 205), pp. 50 ff., Washington, 1936, and this book, pp. 465, 604 f., 630, 715, 717, 729.

Eugen Rosenstock-Huessy: Out of Revolution, 1938
