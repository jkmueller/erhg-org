---
title: Disserations
category: reception
language: en
page-nr: 350
---
* Bruce O. Boston: *I Respond Although I Will Be Changed: The Life and Historical Thought of Eugen Rosenstock-Huessy*, (Dissertation Princeton Theological Seminary) Princeton, New Jersey 1973
* William Thomas Duncanson: *Never one thing : philosophical anthropology in the speech thought of Eugen Rosenstock-Huessy*, University of Iowa, 2001
