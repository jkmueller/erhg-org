---
title: "The Harvard Crimson News about ERH"
category: insight
order: 10
language: en
page-nr: 730
---


**[Today in Washington](https://www.thecrimson.com/article/1933/12/18/today-in-washington-pa-series-of/)**  
*Visiting Professor Rosenstock To Give 12 Bi-Weekly Lectures*  
Published On 12/18/1933 12:00:00 AM  
*NO WRITER ATTRIBUTED*

A series of bi-weekly lectures on "The Revolutions in Western Civilization" will be given, starting January 9, by Eugene Rosenstock, visiting professor of legal history and government from the University of Breslau. These lectures will attempt to simplify the ideas presented in Professor Rosenstock's book. "Die Europuisihe Revolutionen", and to give a short history of Europe as a unit, not as a combination of isolated nationalities. They will be twelve in number and will be presented on every Tuesday and Thursday at 4 o'clock in Emerson D.

+++++++++++++++++

**[High Birth Rate Factor in Social Development Leading to Hitler's Success, Says Rosenstock](https://www.thecrimson.com/article/1934/1/8/high-birth-rate-factor-in-social/)**  
*Revolutions Of Future To Be On Economic Developments, Not National Causes*  
Published On 1/8/1934 12:00:00 AM  
*NO WRITER ATTRIBUTED*

"I believe the success of the Hitler government was made inevitable by the Treaty of Versailles and, amongst other things, by the biological and social development of Germany during the years 1902 to 1914," said Eugen Rosenstock-Hussy, visiting professor of legal history and government from the University of Breslau, when interviewed by the CRIMSON yesterday. Professor Rosenstock is giving a series of twelve lectures on "The Revolutions in Western Civilization" beginning Tuesday, in Emerson F. The lectures will be given every Tuesday and Thursday and will be based on Professor Rosenstock's book "Dle Europaischen Revolutionen."

"By the social and biological development of Germany, I mean that during the years mentioned the birth rate was exceptionally high. The men and women born in this period naturally were not participants in the war, but severely felt the effects of it. They were the famous starving children of Germany. They felt their hardships more acutely than did the hardened soldier, and achieved maturity with an inborn hatred of the government that caused their troubles, and of the treaty that it signed. It is only natural that they were ready to accept a new government such as Hitler's, that offered them better conditions in every way. In present day Germany there are 16 voters that never went to war, but felt its results, to every nine that actually saw action.

"The present Fascist developments, and other movements of that kind are not real revolutions. They are fruitless and sterile. The average man of today is too flat; has not the 'springs' with which to drive himself into the future. He talks always of his ancestors; to get anywhere he should change inheritance to heritage, he should become an ancestor himself. The people of America seem to be rising out of this flatness better than those of any other nation they are coming to realize that the present day is equally, if not more, thrilling and interesting than the past.

"I do not want to become too much of a predictor, but I do believe that the next revolution will not be concerns with national causes, but will be base on economic developments."


++++++++++++++++

**[Eliot House Lecture](https://www.thecrimson.com/article/1934/3/28/eliot-house-lecture-pprofessor-rosenstock-hussy-will/)**  
Published On 3/28/1934 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Professor Rosenstock-Hussy will speak at Eliot House this evening in the Junior Common Room. He has chosen as the subject of his talk, "A Precedent for Modern Dictatorship." All members of the House are invited to attend.

++++++++++++++

**[VISITING LECTURER ASSIGNED TO CHAIR OF ART, CULTURE](https://www.thecrimson.com/article/1776/1/22/visiting-lecturer-assigned-to-chair-of/)**  
*Paul Kluckhohn, Tubingen Professor, Occupies Chair at Midyear Until End Of Semester*  
Published On 1/22/1776 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Dr. Eugen Rosenstock-Hussy, Professor of the History of Law and of German Law at the University of Breslau, Germany, and now a visiting lecturer on government at Harvard University, has been appointed Kuno Francke Professor of German art and Culture at Harvard for the first half of 1934-35, it was learned yesterday at University Hall.

Professor Hussy, who obtained his LL.D, in 1909 and was given his Ph.D. in 1922, Both at the University of Heidelberg, started his teaching career at the University of Leipzig in 1912. He served through the Great War, and afterwards founded the Academy of Labour at the University of Frankfurt in 1921. In 1923 he was appointed Professor at the University of Breslau, and began his foreign career as a lecturer at Oxford University in 1926.

During the second half year the chair will be held by Dr. Paul Kluckhohn, present professor of German Language and Literature, at the University of Tubingen.

He received his Doctorate from the University of Gottingon in 1909, and was appointed an instructor at the University of Muster in 1913. After serving in the War, "he returned to Munster and was promoted to an Associate Professorship in 1920. Five years later he was called to Danzig as a full professor of German Language and Literature, and in this capacity he also spent the years 1927 to 1931 at Vienna, since then he has been a member of the faculty at Tubingen.


+++++++++++++++

**[HARVARD HOST TO TWELVE SCHOLARS IN VARIED FIELDS](https://www.thecrimson.com/article/1934/9/20/harvard-host-to-twelve-scholars-in/)**  
*Salvemini, Ulrich, Rosenstock-Hussy Return--Dennes to be Lecturer on Philosophy*  
Published On 9/20/1934 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Harvard will be host for the coming academic year to a large group of visiting lecturers and professors. Of the twelve visitors eight will have the title of Lecturer and four will carry the title of Professor. All four professors and half of the lecturers will come from foreign countries.

Samuel Flagg Bemis, professor of history at George Washington University since 1924, will come to Cambridge as lecturer for the first half-year. Professor Bemis won the Pulitzer prize in history in 1926 for a book on Pinckney's treaty, and the Knights of Columbus prize for the best historical book of the year by an American college professor in 1923, for a study of Jay's treaty.

The William James Lecturer on Philosophy and Psychology during the first half-year will be Wolfgang Koehler, professor of philosophy and director of the Psychological Institute at the University of Berlin.

Two other appointments for the first half-year are those of Niles Carpenter of the University of Buffalo, who will come to Harvard as lecturer on Sociology; and Walter Rice Sharp of the University of Wisconsin, who will come as lecturer on government and tutor in the division of history, government and economics.

Dr. Jules Blache, Professor of Physical Geography at the University of Grenoble, will come to Harvard as Exchange Professor from France for the second half-year. Dr. Leopold von Wiese, director of the Research Institute in Social Science at Cologne, Germany, will be Lecturer on Sociology for the second half-year. William Ray Dennes, Associate Professor of Philosophy at the University of California, will come as Lecturer on Philosophy for the second half-year.

In addition to the new appointments, a former visiting lecturer is also returning to Harvard this year on a regular university appointment. He is William Koehler, former Director of the Weimar State Art Collection, and will come this year as Professor of Fine Arts. Professor Koehler has been Kuno Francke Professor of German Art and Culture at Harvard since September 1932.

Continuing this year as Lauro de Bosis Lecturer on History of Italian Civilization is Gaetano Salvemini, who has been a visiting professor at several universities in England and America for the past few years, and was at Harvard during the second half of the past year. Professor Salvemini served as Professor of History in the Universities of Messina, Pisa, and Florence from 1901 to 1925. Also continuing as Lecturer on Comparative Education is Robert Ulich who was until recently Professor of Philosophy at the Higher Technical School of Dresden. Dr. Ulich was in Cambridge during the second half of the past academic year and gave several public lectures.

Dr. Eugen Rosenstock-Hussy will return to Harvard this year as Kuno Francke Professor of German Art and Culture for the first half-year and will be a visiting lecturer during the second half-year. Dr. Rosenstock-Hussy, professor of the History of Law and of German Law at the University of Breslau, Germany, has been visiting lecturer on Government during the past year. Dr. Paul Kluckhohn, professor of German Language and Literature at the University of Tubingen, will come as Kuno Francke Professor for the second half-year

+++++++++++++++

**[GREENE REVIEWS WORK OF ROSENSTOCK-HUSSY](https://www.thecrimson.com/article/1934/9/26/greene-reviews-work-of-rosenstock-hussy-pstudents/)**  
*WIDE EXPERIENCE PROVIDES A BACKGROUND FOR COURSE*  
Published On 9/26/1934 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Students who care for adventure combined with solid scholarship have the chance of a life time, this year. A new man is at Harvard whose vitality in thinking and in teaching has seldom been equaled since the days of William James. Professor Rosenstock-Hussy's field is very different. William James, of course, dealt with psychology and philosophy, Rosenstock-Hussy explores history (including sociology) and the philosophy of history. But his mind can best be compared to William James, because, like James, Rosenstock-Hussy is as alive and as spirited as a race-horse winning a race.

This quality is inborn, but nourished by a unique experience. Rosenstock-Hussy is very widely and very individually a man of the world. At twenty, he took his doctor's degree at Heidelberg. He was promptly appointed professor at Leipzig. He served four and a half years as an officer in the war. At the front he wrote a challenging book, "War and Revolution". It was a call to make the world war end all war. It was a protest too against a threatened hardening of class lines into a conflict between classes. After the war Rosenstock-Hussy devoted himself to the cause of education among factory workers. This brought him into the founding of the Academy of Labor at the University of Frankfurt.

***Organized Work Camps***

In 1912, he originated democratic Work Camps as a new form of education, and took a leading part in the Camps, for students, workmen and farmers together, which were finally established in Silesia. From those camps he was called to the Chair of History and Sociology at the University of Breslau. There he published a great work on "Revolutions in European History".

His interest in adult education led him into active connection with work in this field in England. In 1927, he lectured at Oxford, on the invitation of L. P. Jacks, on post-war conditions in labor and education in Germany. He has always been interested in social changes, particularly revolutionary changes, and he came to this country largely because of his interest in the American Revolution. As Visiting Lecturer on Government at Harvard, last winter, he gave a series of public lectures on the Revolutions in Western Civilization. In these he revealed the effect of revolutions on national characteristics and national thought.

As Kuno Francke Professor during this half year, Professor Rosenstock-Hussy is giving two courses open to undergraduates, History 73 and 74 on German Constitutional History from Otto the Great to Charles V and on German Constitutional Documents in their Cultural Setting; also a Seminary, Philosophy 20, in which he will work out a comparison between Hegel's ideas of the philosophy of history and Goothe's. Undergraduate students of history are lucky in their chance to work with a man of such experience of life, such democratic ideals, such a stirring intellectual temperament. Graduate students of history and of philosophy will find in Philosophy 20 a seminar which will grow through their active participation under an intellectual leader whose vitally stimulating, very spirited teaching may well be compared with the great teaching of William James.

+++++++++++++++

**[Lowell House Dance Patrons And Patronesses Announced](https://www.thecrimson.com/article/1934/10/22/lowell-house-dance-patrons-and-patronesses/)**  
*Doc Peyton's Orchestra Will Play For Dartmouth Game Celebration*  
Published On 10/22/1934 12:00:00 AM  
*NO WRITER ATTRIBUTED*

The Dartmouth game and Doc Peyton's orchestra are ready to furnish entertainment for Lowell men and others in the University who are so inclined this weekend. Dancing will be from 6.30 o'clock until midnight in the Lowell House Dining Hall. The patrons and patronesses will be headed by Professor and Mrs. Julian Coolidge and will include: Mr. and Mrs. B. J. Jones, Mr. and Mrs. G. P. Baker, Mr. P. P. Chase, Mr. and Mrs. E. Mims, Mr. and Mrs. James B. Munn, Mr. and Mrs. Edward Melius, Mr. and Mrs. E. Rosenstock-Hussy, Mr. and Mrs. Walter Sharp, and Mr. and Mrs. Charles E. Laurist, Jr.

+++++++++++++++

**[DR. ROSENSTOCK - HUSSY WILL LECTURE TODAY](https://www.thecrimson.com/article/1935/1/4/dr-rosenstock-hussy-will-lecture/)**  
*INDUSTRIALIZED WORLD WILL BE SUBJECT OF EIGHT TALKS*  
Published On 1/4/1935 12:00:00 AM  
*NO WRITER ATTRIBUTED*

"The Twenty-Four Hour Day of Machinery and Capital," will be the subject of the first speech by Eugen Rosenstock-Hussy, professor of Law and Sociology, in a series of annual public lectures sponsored by the Lowell Institute under the direction of President-emeritus Lowell. The lecture will be given at 5 o'clock today in Huntington Hall.

The series includes eight lectures, to be given on successive Tuesdays and Fridays. In the first four, Professor Rosenstock-Hussy will consider the "world of modern industry as a challenge to all our standards in family, education, government, art, and church." The latter four will deal with "possible answers and solutions in order to restore society in the New World."

Professor Rosenstock-Hussy has been appointed Kuno Francke Professor of German Art and Culture for 1934-35. He is also head of the Academy of Labor in Frankfurt and was vice-chairman of the World Association for Adult Education from 1929 to 1933. Since last October he has been conducting historical seminars in the University.


+++++++++++++++

**[FACULTY MEMBERS TO SPEAK AT RADCLIFFE](https://www.thecrimson.com/article/1935/1/9/faculty-members-to-speak-at-radcliffe/)**
*Foreign Affairs Sessions Are to Be Conducted in Agassiz House for Three Days*  
Published On 1/9/1935 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Several members of the Faculty are included in a group of distinguished European and American speakers, all specialists in their various fields, who have been drawn from different universities to take part in the program of the fourteenth annual Foreign Affairs School sponsored by Radcliffe College and the Massachusetts League of Women Voters.

The first day's speakers include Dr. Oliver M. W. Sprague '94, Edmund Cogs well Converse Professor of Banking and Finance, who will speak on "Can the United States Withdraw from the International Scene?" Dr. Sprague was formerly Economic Adviser to the Bank of England and was recently associated with the United States Treasury Department.

Speaking on "Russia Today," Dr. Merle Fainsod, instructor in Government, will open the morning session on Wednesday. In the evening a symposium on "Changes in the European Equilibrium in 1984" will be presented under the leadership of Elton Maye, professor of Industrial Research. It will take the form of a diplomatic round table in which each nation involved in European complications is to be represented. Among those taking part are Dr. Eugen Rosenstock-Hussy. Kuno Francke professor of German Art and Culture; William Y. Elliott, professor of Government; and Dr. Olgerd P. M. Sherbowitz-Wetzor, assistant in Slavic languages.

Payson S. Wild, Jr., instructor in Government will lead a discussion Thursday morning on "Disarmament and Munitions." In the afternoon, Bruce C. Hopper '24, assistant professor of Government, will speak on "The Clash of Nations in Manchuria," following which he will direct a study group on Pan-Pacific problems


+++++++++++++++

**[HOCKING TO REPLACE PERRY AS CHAIRMAN OF DEPT. OF PHILOSOPHY](https://www.thecrimson.com/article/1935/1/9/hocking-to-replace-perry-as-chairman/)**  
*H. A. KORFF NAMED TO SUCCEED ROSENSTOCK-HUSSY*  
Published On 1/9/1935 12:00:00 AM  
*NO WRITER ATTRIBUTED*

In addition to the announcement of five new Faculty appointments by the Corporation yesterday, it was revealed that William E. Hocking '01, Alford Professor of Natural Religion, Moral Philosophy, and Civil Polity, will succeed Ralph, B. Berry, Edgar Pierce Professor of Philosophy, as Chairman of the Division of Philosophy and Psychology and of the Department of Philosophy, on September 1, 1935.

A second departmental appointment announced by the Council of the Faculty of Arts and Sciences was that of Louis C. Graton, professor of Mining Geology, who will be chairman of the Division of Geological Sciences and of the Department of Geology during the second half of this year in the absence of Donald H. McLaughlin, professor of Mining Engineering.

The Corporation has accepted the resignation of William G. Howard '91, professor of German, who will henceforth be Professor Emeritus. Professor Howard was an instructor in German at Princeton from 1893 to 1895 and has been a member of the Harvard faculty since that time.

Herman A. Korff, professor at the University of Leipzig has been appointed Kuno Francke Professor of German Art and Culture for the second half of this year. He succeeds Professor Eugen Rosenstock-Hussy, who has held this position since September.

Other appointments announced yesterday by the Corporation are those of Friedrich E. Machlup-Wolf, who will be Lecturer on Economics for the second half of the year; Edward Welbourne, Lecturer on Government and tutor in the Division of History, Government and Economics for the second half of the year; Leonard Carmichael, Lecturer on Psychology for the first half of 1935-36; and Harrison S. Dimmitt, assistant secretary of the Law School for one year, from January 1, 1935.

Leaves of absence for the first half of 1935-36 were granted, for purposes of research to Bartholomeus J. Bok, assistant professor of Astronomy; Talcott Parsons, instructor in Sociology; and Hassler Whitney, instructor in Mathematics.


++++++++++++++++

**[Rosenstock-Hussy Talks on Industry This Afternoon](https://www.thecrimson.com/article/1935/1/11/rosenstock-hussy-talks-on-industry-this-afternoon/)**  
*To Give Second of Eight Lectures for Lowell Institute*  
Published On 1/11/1935 12:00:00 AM  
*NO WRITER ATTRIBUTED*

"Modern Industry as a Restoration of Nature" will be the subject of the second speech by Dr. Eugen Rosenstock-Hussy, professor of Law and Sociology, in the annual series of lectures now being sponsored by the Lowell Institute under the direction of President-emeritus Lowell. The lecture will be given at 5 o'clock today in Huntington Hall.

The series includes eight lectures, to be given on successive Tuesdays and Fridays. In the first four, Dr. Rosenstock-Hussy is discussing "the world of modern industry as a challenge to all our standards in family, education, government, art, and church." The latter four will deal with "possible answers and solutions in order to restore society in the New World."

For the second half of the year, Dr. Rosenstock-Hussy is to be succeded by Professor Herman A. Korff

+++++++++++++++++

**[Sprague's Speech Tops List At League of Women Voters](https://www.thecrimson.com/article/1935/1/21/spragues-speech-tops-list-at-league/)**  
*"School of Politics" to Include Eight Other Harvard Men*  
Published On 1/21/1935 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Starting its talks on foreign affairs with a speech by Oliver M. W. Sprague '94, Edmund Cogswell Converse Professor of Banking and Finance, the Massachusetts League of Women Voters, in cooperation with Radcliffe and the Cambridge League of Women Voters, will open tomorrow a special "School of Politics," to be held at Agassiz House.

Included on the school's curriculum, in addition to Professor Sprague's lecture, will be speeches by eight other Harvard professors on subjects ranging from "Social Equilibrium" to "The Clash of Nations in Manchuria." Other Harvard men will be: Bruce C. Hopper '24, professor of Government, William Y. Elliott, professor of Government, Eugen Rosenstock-Hussy, retiring Kuno Francke Professor of German Art and Culture, Elton B. Mayo, professor of Industrial Management in the Business School.

++++++++++++++

**[BOOKSHELF](https://www.thecrimson.com/article/1940/11/19/bookshelf-pbfbrank-davidson-39-founder-of/)**  
*AMERICAN YOUTH: AN ENFORCED RECONNAISSANCE. Edited by Thacher Winslow and Frank P. Davidson. Harvard University Press. 210 pp.*  
Published On 11/19/1940 12:00:00 AM  
*NO WRITER ATTRIBUTED*

FRANK DAVIDSON '39, founder of the Guardian, who edited "Foresight in Foreign Affairs" two years ago, has taken time off from his work with the C.C.C. educational program to engineer a new book. This time he has teamed up with Thacher Winslow '29 administrative assistant in the N.Y.A., and collected thirteen essays dealing with what Mrs. Roosevelt calls in her foreword "one of the most vital problems of our society"--the problem of meeting the threat to democracy growing out of a jobless, drifting, and disillusioned generation of young Americans.

The lines along which the contributors to "American Youth" believe this problem must be attacked are suggested by George S. Pettee in his penetrating chapter on "The Appeal of Totalitarianism," where he says, "There is one critical point in the field of this battle (against social disintegration and revolution) and one only: if American youth can continue to feel that the American order is adequate in opportunity and hope, that it is worth working in and for, that its purposes are stimulating and rewarding, we are in no danger from any side." He goes on to make two suggestions: "First we must never stop teaching the possibilities of modern human life. Second, we must find ways to give every young American the sense of pride in fruitful work."

The nature of the educational problem which Mr. Pettee hints at is enlarged on by Harvard's Professor Robert Ulich in a chapter on "Constructive Education" in which he calls for a school system which will "develop in its pupils initiative and ethical character." Dartmouth's Professor Rosenstock-Huessy vaguely believes that "we must provide for the future adult an education that makes him experience 'emerging inspiration,' emerging authority." The more prosaic task of increasing the facilities for vocational guidance and training is treated by Aubrey Williams in his chapter on "The Role of the Schools."

But character building and job training will not provide for America's four million unemployed young people "the sense of pride in fruitful work." Consequently the majority of the contributors to "American Youth" see the necessity for some form of voluntary work camp programe--possibly an enlarged C.C.C. with "a reputation as a man factory, not as a mere warden of sub marginal youth."

Such a proposal may seem irrelevant today when America is beginning to find a place for its jobless--in the army and in defense industries. But this currently popular remedy is at best a palliative, and in the long run aggravates the disease. For as William James pointed out in 1910 in his brilliant essay on "The Moral Equivalent of War" which is reprinted in this volume, there is a direct and fundamental connection between a frustrated generation with no stake in society, and the devastation of recurring warfare.


+++++++++++++++++

**[GRADUATES HELP IN PLAN TO BUILD BETTER YOUTH CAMPS](https://www.thecrimson.com/article/1940/11/26/graduates-help-in-plan-to-build/)**  
*William James Philosophy Is Basis For Vermont Project*  
Published On 11/26/1940 12:00:00 AM  
*NO WRITER ATTRIBUTED*

Partially because of the work of several Harvard graduates, the United States may be introduced to a new kind of work-service camp, designed to overcome the inadequacy of the C.C.C.

Built on the principle that "the best training for democratic youth lies in the practice of democracy, fellowship, and hard work," the plan calls for the opening of a William James Camp in Sharon, Vermont, on the site of an abandoned C.C.C. camp. Named after the great American philosopher, the establishment would accommodate about 40 youths from all walks of life who would work on jobs selected by local citizens and supervised by local men.

***All Kinds of Work***

Work undertaken would include the rehabilitation of farms, repair of buildings, and installation of improvements. The camp would also provide a flexible labor supply of hands to help in chores whenever extra help was required on nearby farms. A work superintendant, elected by the men, would have general charge of the camp.

The William James Camp will exemplify the philosophy of the man after whom it is named, following his famous idea that the necessity in modern civilization is to provide young men with a "moral equivalent for war." This philosophical purpose is constantly kept before the group by Eugene Rosenstock-Heussy, professor of social philosophy at Dartmouth, who is directing the campaign, aided by the Harvard men and several Dartmouth graduates.

Originally only Dartmouth was participating in the program, but Harvard came into the picture in 1939, when three graduates who had come to the conclusion that the United States faced a dangerous social strain unless something drastic and constructive were done for the young people of the nation, were put in touch with Professor Rosen-stock-Hussy's project by a mutual Cambridge friend, Mrs. Henry Copley Greene.

They were Frank P. Davidson '39, founder and first president of the Harvard Guardian, Richard T. Davis '38, summa cum laude graduate in government, and Robert E. Lane '39, former president of the Student Union. Other graduates working with them are Philip Bugby '39, Enno R. Hobbing '40, and George W. Phillips '39.

***Give Up Careers***

All the men gave up careers or scholarships for hard farm work everyday from sun-up to sun-down, gaining another kind of education from that to which they have been accustomed, and collecting evidence that their plan for the salvation of youth would work.

Having gained the backing of Mrs. Roosevelt, Dorothy Thompson, and Dorothy Canfield Fisher, all of whom have done much to help the group, they submitted a petition to the government asking "for cooperation among rural communities, college men, and city youth." Paul V. McNutt, Commissioner of the Federal Securities Board, appointed a committee to consider the petition, one of whose members was Dean James M. Landis of the Law School. The plan was approved by by the National Defense Council, and is now before the Department of Agriculture.

Although many of the residents of the farm community, to whom this activity would be an economic stimulant, suspect the group of Communist or Nazi leanings, their feelings were countered by Nathan Dodge, chairman of the local group backing the plan, who jokingly suggested that it would be better to have the boys together in one camp than free to roam as they would
