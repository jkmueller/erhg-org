---
title: Who is Eugen Rosenstock-Huessy
category: lebensbild
order: 1
language: en
page-nr: 100
landingpage: front
order-lp: 1
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_jung.jpg' | relative_url }}){:.img-right.img-small}
Born in Berlin on July 6, 1888, died on February 24, 1973 in Four Wells, Norwich, Vt. USA.
<!--more-->
Eugen Rosenstock-Huessy could have said that he had many lives:

* as child of emancipated parents of Jewish origin, grown up with six sisters,
* as student of law,
* as the youngest professor in 1914,
* as combatant in the First World War (officer at Verdun),
* as first editor-in-chief of the Daimler-Werkzeitung, a company newspaper,
* as first director of the Akademie der Arbeit (academy of labour), Frankfurt,
* as professor of law in Breslau,
* as initiator and inspirator of voluntary work camps for factory workers, farmers and
  students in Silesia,
* as historian, theologian, sociologist before the Hitler period,
* as emigrant in 1933,
* as immigrant to the United States of America,
* as professor in Harvard and at Dartmouth College, Hanover, New Hampshire,
* as initiator of Camp William James,
* as founder of a new direction for adult education after the First World War in Germany
  and  similarly after the Second World War in Germany and the United States,
* as author of a sociology which systematically treats the present time and universal history.

![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_5.jpg' | relative_url }}){:.img-left.img-small}

Rosenstock-Huessy is characterized by his fine intuition for events of historic importance. With surprising speed he could place those events into larger historic contexts. He lived by the belief that after the foundational experience of the collapse of the First World War a new social order was required. In this belief he was mainly joined by the authors of the journal "Die Kreatur", which was published between 1926 and 1930. [^1] The encounter with [Franz Rosenzweig](http://www.ersterweltkrieg.eu/rosenzweig/rosenzweigichbleibealsojude.html) showed him that in spite of Christianity Judaism was still necessary.

The voluntary work camps for factory worker, farmers and students in Silesia which were initiated by Eugen Rosenstock-Huessy became the cornerstone for the resistance movement around Helmut James Graf von Moltke. Walter Hammer (1888-1966), therefore, called him the the progenitor of the [Kreisauer Circle](https://en.wikipedia.org/wiki/Kreisau_Circle).

Immediately after the takeover of power by Hitler at the 31st of January 10933, he recognized how persons of Jewish origin in Germany would be treated and therefore left Germany during the same year. As professor in the United states of America he had a lasting impact on many people. In Germany, where he could speak for the first time after the Second World War in 1950 at the University of Gottingen, he would only receive a warm reception by those who were willing to be inspired by him.[^2] His closest European followers after 1945 were probably in the Netherlands.
![Eugen Rosenstock-Huessy]({{ 'assets/images/110kk.jpg' | relative_url }}){:.img-right.img-small}

Eckart Wilkens, Köln, April 2007  
translated by Jürgen Müller


[^1]: Walter Benjamin, Nikolaj Berdjajew, Hugo Bergmann, Martin Buber, Edgar Dacqué, Hans Ehrenberg, Rudolf Ehrenberg, Marie Luise Enckendorff, M Gerschenson und W. Iwanow, Eberhard Grisebach, Willy Haas, Hermann Herrigel, Edith Klatt, Fritz Klatt, Georg Koch, Ernst Loewenthal, Ernst Michel, Wilhelm Michel, Albert Mirgeler, Karl Nötzel, Alfons Paquet, Werner Picht, Florens Christian Rang, Eugen Rosenstock-Huessy, Franz Rosenzweig, Heinrich Sachs, Leo Schestow, Justus Schwarz, Ernst Simon, Dolf Sternberger, Eduard Strauss, Ludwig Strauss, Hans Trüb, Viktor von Weizsäcker, Joseph Wittig.

[^2]: His argument, that Germany's spiritual and mental existence was depending on the way martyrs under Hitler where treated influence during the times of Adenauer in West-Germany and Ulbricht in East-Germany. Those ideas were extremely unpopular in those days.
