---
title: "Conference 2010 - Proceedings"
category: reception
language: en
page-nr: 422
---




### The Moral Equivalent of War - From William James to Camp William James and Beyond:
**Eugen Rosenstock-Huessy and the Social Representation of Truth**  
Conference at Dartmouth College - November, 12-13 2010

A bound set of the papers prepared for the conference on  
*William James's Moral Equivalent of War and ERH's Social Representation of Truth* (Nov. 12-13)  
can be had for the asking (plus a small payment of $20.00/for overseas, $25).

The contents are:
* Paul Lee, "A Moral Equivalent of War";
* Justin Reynolds,"'The Star of My Americanization': ERH's Discovery of William James and the Fading of an American Dream";
* Mike McDuffee, "A Re-Reading of ERH's Christian Philosophy of History in the Era of God's Return";
* Clinton Gardner, "Camp William James and ERH's Vision of Chrisitianity in the Third Millennium";
* Svein Loeng, "ERH and Andragogy";
* William Young, "ERH, Rosenzweig, and the Work of Education";
* Andreas Leutzsch, "Rethinking Planetary Service";
* Feico Houweling, "How Does Our Life Bear Fruit?: The Unfinished Story of Rosenstock-Huessy Huis in Haarlem, the Netherlands";
* Otto Kroesen, "Planetary Internships and Cultural Diversity";
* Norman Fiering, "ERH on the Structure of Significant Lives".

Total number of pages is 215.

The price is the exact cost of copying, binding, packaging, and postage.  
Those who registered for the conference will be receiving a set in the mail next week; other contributors are getting a complimentary copy.
