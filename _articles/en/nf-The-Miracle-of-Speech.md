---
title: "Norman Fiering: The Miracle of Speech: A Comment on ERH"
category: reception
language: en
page-nr: 310
---
Human speech is able to serve as a lever by which the commitments of astonishingly large numbers of people may be lifted and shifted. I refer here not to the obvious power of oratory to move crowds. Consider instead the muted inauguration of the new president of the United States or of any peaceful transfer of political power. In January 2009, the allegiance of 300 million people was instantly but gently moved from one point to another by the oath of office taken by the new president.

This large country, possessed with enormous power in many forms, tip-toes at the moment of transfer with the delicacy of a ballet dancer, lest any suggestion creep in of coercion, as opposed to consent or free will. A mammoth pivoting on a pinhead by the simple enunciations: “Do you solemnly swear to faithfully execute. . . .?” “I do.” The deed is done merely by the miracle of articulated speech, that is, the meshing of speech and response. An order is issued in the form of a question, and the response is, I will do it.

This is, of course, the wonder of democracy, its boisterous, hard-edged contention in the end subdued by a vote, that is to say, by essentially spoken “yes”’s and “no”’s, which are translated by the ballot box or a computerized version thereof, into some mechanical action. The “voice” of the people is no metaphor, since we all do say “yea” or “nay,” in effect.

Moreover, the presidential transfer in the U. S. goes back, as well, to a meeting in Philadelphia in 1787 where also what were originally spoken words were embodied in the Constitution, which we reverence and to which we defer, but which we also allow to carefully metamorphose under the command of the speech of a group of justices in a court room, or sometimes by more voting in the amendment process. Spoken words determine both individual lives and the fate of millions.

Thus we are bound together, one might even say spiritually unified, not by “words,” as is sometimes said, not by “language,” as is also asserted, and certainly not by mere “talk,” but by living speech, which is language backed up by conviction and commitment, that is, by full accountability, and backed up even by putting one’s life or one’s freedom on the line: “This I have said. I stand by it. You may quote me. I am responsible for what I have said.” Sometimes even we hear, at moments of the greatest crisis, “I know I may die for what I have said.”

Thus is speech, which strangely we take for granted as though it was part of the natural order of things. But real speech is decidedly unnatural, or perhaps “supernatural” is the more apt word if freed from the connotation of superstition.

We are regularly reminded by those that miss the point that even bees, whales, and chimpanzees have language. Indeed, chimps have language, but they don’t take oaths that bind 300 million other creatures, or utter promises that last a lifetime, such as “I do” in the marriage ceremony, or pronounce a verdict in a courtroom that determines life or death, or issue an order that is still in the process of fulfillment after two millennia: “And he said to them, Go you into all the world, and preach the gospel to every creature.” (Mark 16:15). All religion hinges on ancient spoken injunctions. Speech creates vast corridors through time and links the generations.

The foremost analyst, or better to say, prophet, of man as the speech-making animal, Eugen Rosenstock-Huessy, died only in 1973, and is still not widely known. Everything stated above is directly derived from his writing and speaking. He made discovery after discovery about this strange power, little of which has been absorbed by the learned world, despite his thousands of pages of published writings. His friends and those he inspired, such as Franz Rosenzweig and Martin Buber, or W H. Auden and Walter Ong, are far better known.

If speech is so evidentally fundamental to human society, Rosenstock-Huessy said, let us probe it to its depths and discover how it performs its magic. This exploration of the power of speech, it turns out, reveals God, man, and the world anew. Indeed, “God is the power that makes us speak,” Rosenstock held. In a supposedly disenchanted world, lacking in mystery and miracles, the irreducible magic of speech remains as potent an elixir as ever, although it is often degraded and in need of revitalization.

Great spoken words echo through the centuries, calling us forth. While at the other extreme in the tight intimacy of the family, the infant is called into life by the direct address of his parents and others: “Jimmy, we love you, grow and be.” Indeed, the magical and transforming reassurance, from cradle to grave, of directly hearing those words, “I love you,” has not suffered in the slightest from the reductionist investigations of neuroscience, which sometimes purport, absurdly, to “explain” love.

Grade and rank your knowledge, Rosenstock frequently urged. The product of the laboratory deserves no privilege over what you know from direct, personal experience. The filter of "education" can destroy our ability to receive our experience as primary evidence.

In Rosenstock-Huessy’s “grammatical method,” even the vast enterprise of natural science in the past six centuries may be classified as a particular exercise of speech, one of only four necessary forms of speech–– that form which properly relates to the "dead" and objectified elements in the world, the eternally repetitious, ahistorical, external world of space. After love has happened to a person, as a unique event, the psychologist, or whomever, comes along and studies its generalized form, but as Rosenstock says, there is no “I,” “you,” and “we” in it. In fact, “nature” and science all have to do with the world that specifically excludes speech. Science is an essential, invaluable part of the grammar of mankind, but it always comes after the real action is spent

Rosenstock-Huessy’s “grammatical method,” which cannot be explored here at length, and his thought taken comprehensively, restores a social balance that has long been terribly distorted. In his work, all human endeavour finds its particular rightful place: art and literature, sports and play, law and politics, science and philosophy, technology and industry. Nothing is lost or expunged or rejected; they are all profoundly and freshly understood. But all of these modes of human experience are at last put within the perspective of a whole view of who man is. Once the lesser gods in society––art and science, for example–– are recognized as such, they can no longer be objects of worship and reverence as they typically are in our time. Grammar, Rosenstock said, “will ascend beyond the grammar school, and become from a dry-as-dust textbook-obsession, the open sesame to the hidden treasures of society.”[^1]

Such an assertion is hard to believe, but a fundamental re- consideration, for example, of the huge bias towards space as the primary dimension in human affairs (we ludicrously regard time as merely the fourth dimension of space) has revolutionary implications, as does the unseating of our adherence to the standard subject/object distinction, and both of these matters are addressed by a new understanding of speech and grammar.

The grammatical method is no academic exercise, no series of metaphysical abstractions. Rosenstock’s writing is always extraordinarily grounded. He has no recourse to transcendence or to retreat from the world. He had lived through World War I as an officer in the German army and witnessed the suicidal catastrophe of European culture, an internecine slaughter beyond comprehension.

From the time of an epiphany he had in 1918 until his death in 1973, Rosenstock-Huessy devoted his life to the study of where and how Western Civilization went off track, with World War II and the Shoah only a confirmation of his worst fears.

He challenged academe, he challenged the nation-state, he challenged the Christian churches. At the time of the crisis of World War I, he wrote, the prevailing idols in Germany––the universities, the churches, and the state––all had “piteously failed. They had not been anointed with one drop of the oil of prophecy, which God requires from our governors, from our teachers, and from our churches. . . . Not one of them had any inkling of the doom or any vision for the future beyond mere national sovereignty.”[^2]

The author of hundreds of published articles and books, as recorded in Lise van der Molen’s Guide to the Works of Eugen Rosenstock- Huessy: Chronological Bibliography (1997), Prof. Rosenstock-Huessy remained always at heart a philosophical historian, although he acquired the voice, as well, of a Judaeo-Christian prophet.

[^1]: “In Defense of the Grammatical Method,” in Speech and Reality [1970], 9.

[^2]: “Metanoia: To Think Anew,” in I Am an Impure Thinker (1970). 
