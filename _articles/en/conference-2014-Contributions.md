---
title: "Conference 2014 - Contributions"
category: reception
language: en
page-nr: 426
---

### Eugen Rosenstock-Huessy: Then & Now
Conference at Renison University College - June 19-22, 2014

The following contributions are planned:

* Weissman, Norman: Letter
* Cane, Bill: A Reflection on ERH & the Future
* Cristaudo, Wayne: An Introduction to ERH’s Sociology
* Bade, David: Respondeo Etsi Mutabor: ERH & Linguistic Theory
* Utsunomiya, Hidekazu: R-H & the Trinity
* Hussey, Dana: A Reflection on the Influence of ERH
* van der Molen, Lise: A Frame Story in which we all Participate
  * What is the Meaning of New Thinking?
  * New Thinking in FR
  * The Good Samaritan
  * Translating the Gospel
  * Bibliography
* Roy, Christian: Revolution, Work, Resistance
* Fiering, Norman: “The Uni-versity of Logic, Language & Liturgy”
* Houweling, Feico: ERH & the Dutch
* Gardner, Clinton C.: Speech as our Matrix
* Bryant, M. Darrol: Remembering ERH
* Leenman, Willem: The Influence of ERH on Bas
* Ruiter, Nick: What Can I Say?
* Pollard, Bob: R-H, the Galileo of the Human, Social Sciences
* Leithart, Peter J.: Stasis & Eruption: Apocalyptic in ERH
* Myers, Paul: “From Studying Eugen to Standing in Eugen’s Study: The Grammatical Method for Group Leadership”
* Gorman-Thelen, Michael: Some Necessary but Preliminary Remarks
* Huessy, Raymond: An Introduction in Four Acts & an Epilogue
* Huessy, Ray: Eugen & Margrit R-H vs. the Academy
* Fiering, Norman: Rosenstock-Huessy in the Classroom
* Mas Diaz, Sergio: Speech Thinking, Zeitdenken, & Personalism
* Moore, Michael: Religion & History: RH, FR, & B in Die Kreatur
* McDuffee, Mike: God Gives us the Fruit of Praise
* Bryant, M. Darrol: A Record & Promise of Life : ERH & WR
* Jordan, James B.: ERH: From Then to Now, Reflection
