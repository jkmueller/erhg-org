---
title: "Wim Leenman & Bob O'Brien: Founding Planetary Posts"
category: reception
language: en
page-nr: 320
---
The idea of a new social institution, „Planetary Posts", as a progression of voluntary service, has arisen from 55 years of voluntary workcamp experiences and 25 years of community life.
Such a new institution is historically comparable with monastic history, which started with one monastery in the sixth century, and grew to thousands all over Europe by the Middle Ages.
Today social stability is threatened by the increasing fragmentation of urbanized, industrialized, technologically-orientated society, which has led to widespread loneliness, alienation, resignation from citizen responsibilities and a sense of powerlessness in the advanced economies and which is beginning to affect the developing nations with possible catastrophic results.  
The founding of a new social institution of Voluntary Service, alongside such other large institutions as universities, trade unions, churches, the Law and similar other sociological „inventions“, would offer a constructive response to our predicament.

Pierre CérésoIe, who is generally regarded as the founder of voluntary service in 1920, was inspired by the essay, " A Moral Equivalent of War", published by William James in 1910, after many years of studying the psychology of war.  
James wrote: *"...when whole nations are the armies, and the science of destruction vies in intellectual refinement with the science of production, I see, that war becomes absurd and impossible from its own monstrosity"*.

There are two important exponents of our need and suffering: war and fragmentation.
Is Voluntary Service an answer to these items?

As Voluntary Service has grown and now can be found in private and governmental programs all over the globe, we see, that it offers even more than "A Moral Equivalent of War". It can revitalize spiritual life and lead to a revaluation of the values of present day society, not only by providing constructive work as an outlet for the energy, imagination and idealism of youth, but by doing this in a social framework that brings together all classes of society, all generations and the special talents of each individual, in a process that offers possibilities for friendship and understanding among all members of the human family.

When meeting each other in alleviating the sufferings of Mankind, these volunteers for Planetary Service will soon need some common spiritual ground beyond their conflicting backgrounds. To deal with this vacuum, biographies of men and women, in retrospect recognizable as seeds of the past for a universal future, should be part of the orientation of these politically and spiritually heterogenous groups of volunteers. Only those human seeds that have been sown in the field of aIl Mankind, can have the power to provide us with a spirit that can reconcile a plurality of spirits.

Our proposal is an attempt to invoke the younger generation in human history and to rekindle faith and hope in a common destiny. We propose that a small scale experiment be undertaken in an existing community, whose members have been prepared for and are willing to accept "a garrison" of volunteers, who would commit themselves to voluntary service for at least a year.

The community would be a permanent base for succeeding groups of volunteers, thus providing continuity, establishing contacts with the surrounding area, and being a source of social, political and economic expertise, which the inexperienced volunteers could draw upon.
It will be a practical experience and a spiritual one.

We can only suggest the essential elements that will comprise „ A Planetary Post“. Each Planetary Post will develop its own identity, just as each University has his own identity, although its basic ingredients, values and features express and contribute to a common purpose.
Society is like a home with many rooms. Throughout history, when a great need has arisen, a new room has been added, which has changed the entire pattern of life which had existed previously.
We believe that a critical need calls for a new institution within our social structure, which will strengthen unity without destroying diversity.  
The existing programs of voluntary service themselves need the creation of "Planetary Posts“ if they are to consolidate the achievements which have been made in history. We know that fragility always exists, that gains can never be considered as permanent, that, when the time is ripe for a new step forward, the moment must be seized.  
We propose that an experiment to form "A Planetary Post" be initiated as a Flagship Project in the „European Voluntary Service for Young People“ program.

Haarlem, september 1996.

Written by:

Wim Ph.Leenman & Bob 0'Brien  
„Rosenstock-Huessy-Huis“  
Hagestraat 10  
NL 2011 CV Haarlem
