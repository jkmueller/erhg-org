---
title: Eugen Rosenstock-Huessy kim
category: lebensbild
order: 1
language: tr
page-nr: 100
landingpage: front
order-lp: 1
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_jung.jpg' | relative_url }}){:.img-right.img-small}
Doğumu 6. Temmuz 1888 Berlin, vefatı 24. Subat 1973 Four Wells, Norwich, Vt Amerika.
<!--more-->
Kendinin çok yaşamı olduğunu diyebilirdi Eugen Rosenstock-Huessy:

* Yahudi asıllı, eşit görüşlü bir ailenin çocuğu olarak 6 kız kardeşiyle büyümüş,
* Hukuk öğrencisi ve 1914 de en genç ilk özel doçent,
* Savaş katılımcısı (Verdun savaşında subay),
* Daimler firma gazetesinin ilk yayıncısı,
* Çalışma akademisinin ilk yöneticisi,
* Breslau şehrinde hukuk profesörü,
* İşçi, çiftçi ve öğrenciler için gönüllü çalışma kamplarının teşvikçisi,
* Hitler döneminden önce tarihçi, theolog ve sosyolog,
* 1933 de göçmen,
* Amerika Birleşik Milletlerine göçmen,
* Harvard, Dartmouth Koleji, Hanover ve New Hampshire de yüksek okul öğretim üyesi,
* Camp William James'in teşvikcisi,
* Birinci dünya savaşından sonra ve aynı yönde ikinci dünya savaşından sonra da Almanya da yetişkin eğitiminin yeni yönlendiricisi,
* Evrensel tarihi ve şimdiki zamanı kapsayan bir sosyoloji nin yazarı.

![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_5.jpg' | relative_url }}){:.img-left.img-small}

Rosenstock-Huessy'nin tarihi gelişmelere karşı büyük bir hassaslığı vardı. Bunları yaşayan olarak şaşırtıcı bir hızla daha büyük bağlantılara koyuyordu. Oluşan temellerin birinci dünya savaşında yıkılma tecrübesinden sonra kendini dünya savaşlarından sonra sosyal bir düzen arayan kişilerden birisi olarak anladı. Bu grubun içinde 1926 dan 1930'a kadar basılan 'Die Kreatur' dergisinin yazarları da vardı. [^1] [Franz Rosenzweig](http://www.ersterweltkrieg.eu/rosenzweig/rosenzweigichbleibealsojude.html) ile olan karşılaşması onu hıristiyanlığın olmasına rağmen yanında yahudiliğin de olması gerektiği kanatına getirdi. İşçilere, çiftçilere ve öğrencilere yönelik Eugen Rosenstock-Huessy tarafından teşvik edilen çalışma kampları Helmut James Graf von Moltke etrafında oluşan direnme hareketinin temelini attı. Walter Hammer (1888-1966) onu Kreisau grubunun esas babası olarak adlandırdı.

The voluntary work camps for factory worker, farmers and students in Silesia which were initiated by Eugen Rosenstock-Huessy became the cornerstone for the resistance movement around Helmut James Graf von Moltke. Walter Hammer (1888-1966), therefore, called him the the progenitor of the [Kreisauer Circle](https://en.wikipedia.org/wiki/Kreisau_Circle).


31. Ocak 1933, Hitler iktidara geçtikten hemen sonra, Almanya da yahudi insanların durumunu anladı ve aynı yıl da ülkeyi terk etti. Amerika da yüksek okul öğretim üyesi olarak çok insanı etkiledi. İlk sefer yine 1950 de Almanya Göttingen üniversitesinde sesini duyurduğunda esin verdiklerine yakın olabildi.[^2] 1945 yılından sonra herhalde en sadık avrupalı dinleyicilerini Hollanda da bulmak mümkün.

Eckart Wilkens, Köln, Nisan 2007  
Übersetzung: Müzeyyen Dreessen, Gladbeck 2010

![Eugen Rosenstock-Huessy]({{ 'assets/images/110kk.jpg' | relative_url }}){:.img-right.img-small}

[^1]: Walter Benjamin, Nikolaj Berdjajew, Hugo Bergmann, Martin Buber, Edgar Dacqué, Hans Ehrenberg, Rudolf Ehrenberg, Marie Luise Enckendorff, M Gerschenson und W. Iwanow, Eberhard Grisebach, Willy Haas, Hermann Herrigel, Edith Klatt, Fritz Klatt, Georg Koch, Ernst Loewenthal, Ernst Michel, Wilhelm Michel, Albert Mirgeler, Karl Nötzel, Alfons Paquet, Werner Picht, Florens Christian Rang, Eugen Rosenstock-Huessy, Franz Rosenzweig, Heinrich Sachs, Leo Schestow, Justus Schwarz, Ernst Simon, Dolf Sternberger, Eduard Strauss, Ludwig Strauss, Hans Trüb, Viktor von Weizsäcker, Joseph Wittig.

[^2]: Almanyanın akli ve ruhi varlığının Hitler döneminde şehitlerine karşı nasıl değer verdiğine bağlı olduğu argümani Adenauer ve Ulbricht döneminde çalışmalarının önüne engel olmuş olmalı. Çeşitli nedenlerden dolayı o dönemlerde bu tür düşünceler istenilmiyodu ve popüler değildi.
