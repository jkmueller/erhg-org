For adding a new language:
- make a copy of the infrapages
- create _articles/<language>/eugen-rosenstock-huessy-<language>.md with
    page-nr: 100
- potentially more articles in _articles
- potentially lists for further page organization in _lists
