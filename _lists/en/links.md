---
title: Links
language: en
page-nr: 1070
column: 1
row: 7
used-categories: links
---

## Other Sites about Eugen Rosenstock-Huessy

* [***Eugen Rosenstock-Huessy Gesellschaft***](https://www.rosenstock-huessy.com)\
  Literary Society about Eugen Rosenstock-Huessy

* [***Respondeo***](https://www.respondeo.nl)\
  Dutch association, about the legacy of Eugen Rosenstock-Huessy.

* [***Eugen Rosenstock-Huessy Fund***](https://erhfund.org)\
  The Fund works in the USA for preserving and distributing the legacy of ERH.

* [***Eugen Rosenstock-Huessy Society of North America***](https://erhsociety.org)\
   Literary Society about Eugen Rosenstock-Huessy

## Personal Websites

* [***Feico Houweling's Homepage***](https://www.feico-houweling.nl)\
  Personal storytelling about history based on insights of Eugen Rosenstock-Huessy.

* [***Otto Kroesen's Homepage***](https://temporavitae.nl)\
  Inspired by Rosenstock-Huessy he presents his works as a sociologist,
  philosopher of technology and coach.

* [***Fritz Herrenbrück's Homepage***](http://www.fritz.herrenbruck.de)\
  Contains a review of the 2009 Edition of Rosenstock-Huessy's Sociology

* [***Norman Fiering's Homepage***](http://www.normanfiering.net/)

* [***Peter Leithart***](https://www.patheos.com/blogs/leithart/)

* [***Wayne Cristaudo @ cdu***](https://www.cdu.edu.au/node/3329)
