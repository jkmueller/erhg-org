---
title: Texts online
language: en
page-nr: 1040
column: 1
row: 3
used-categories: online-text
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_5.jpg' | relative_url }}){:.img-right.img-large}

[Indivisibillimus](https://www.rosenstock-huessy.com/text-erh-indivisibillismus-1950/)

[APE: Animism](https://www.rosenstock-huessy.com/text-erh-animism-1962/)

[APE: Aristotle](https://www.rosenstock-huessy.com/text-erh-aristotle-1962/)

[APE: Concordance](https://www.rosenstock-huessy.com/text-erh-concordance-1962/)

[APE: Existentialism](https://www.rosenstock-huessy.com/text-erh-existentialism-1962/)

[APE: Pierre Abélard](https://www.rosenstock-huessy.com/text-erh-abelard-pierre-1962/)

[I am an impure Thinker](http://erhfund.org/wp-content/uploads/I-am-an-Impure-Thinker.pdf)

[The Multiformity of Man](http://erhfund.org/wp-content/uploads/The-Multiformity-of-Man.pdf)

[The Listener's Tract](http://www.erhsociety.org/wp-content/uploads/2018/12/the-listeners-tract.pdf)

[Soul, Body, Spirit](http://www.erhsociety.org/wp-content/uploads/2018/12/erh-soul-body-spirit.pdf)

Some [texts](https://www.rosenstock-huessy.com/zz-english-texts/) are on the site of the ERH Gesellschaft

Most of [Writings and Manuscripts of Eugen Rosenstock-Huessy](https://www.erhfund.org/search-the-works/) are available as Scan in PDF-Format.
